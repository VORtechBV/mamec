! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module CalcMatrices_FD
! The discrete operators used in the FD schemes (DIV, GRAD, DIVr, GRADr) consist
! of multiple simpler calculations (interpolations, discrete derivatives in
! computational grid direction).
!
! These simple building blocks are stored in the form of banded sparse matrices,
! so they can be applied very easily in the subroutines of FDApplyOperators.
! These banded sparse matrices are allocated and filled in this module.
  private
  public:: CalcMatrices

contains

  subroutine CalcMatrices()
  use Inputs,           only: do_correction, scaleCurv
  use FDApplyOperators, only: AllocateFD
  implicit none
    call AllocateFD()
    call setIntegration()
    call CalcDifferenceMatrices()

    ! The interpolations have to be exact for certain fields.
    ! In rectilinear grids, this is always the case, but in curvilinear grids, we
    ! need a low-rank correction term in the interpolation matrix.
    if (do_correction .and. abs(scaleCurv)>0) call InterpolateExactFunctions()
  end subroutine CalcMatrices

  ! Symmetry-preservation is all about scalar products, given by
  !     <x,y> =  x^T * dVc * y                        for scalar fields
  !     <v,w> = vx^T * dVe * wx + vy^T * dVn * vy     for vector fields
  subroutine setIntegration()
  use Util,       only: error_exit
  use Settings,   only: myprec
  use Inputs,     only: m,n, GridFunctions
  use GridValues, only: dVe, dVn, dVc
  use omp_lib
  implicit none

     integer :: ierr
     integer :: ix, iy

     real(myprec) :: dxdxi, dxdeta, dydxi, dydeta, rix, riy, x, y
     real(myprec) :: sumQ, sumQX, sumQY
     integer :: irow

     allocate( dVc(0:m*n-1), dVe(0:m*n-1), dVn(0:m*n-1), stat=ierr)
     if (ierr/=0) call error_exit('Allocation error')
     if (ierr/=0) stop

! $ OMP PARALLEL DO PRIVATE (ix,iy,dxdxi,dxdeta,dydxi,dydeta,x,y, rix, riy)
     do irow = 0,n*m-1
        ix = mod(irow,m)
        iy = irow/m
        ! Calculate the integration coefficients
        rix = real(ix,myprec)
        riy = real(iy,myprec)

        call GridFunctions(rix,riy, x,y, dxdxi, dydxi, dxdeta, dydeta)
        dVc(irow) = (dxdxi * dydeta - dxdeta * dydxi)

        rix = real(ix,myprec)
        riy = real(iy,myprec)+0.5
        call GridFunctions(rix,riy, x,y, dxdxi, dydxi, dxdeta, dydeta)
        dVn(irow)  = ( dxdxi * dydeta - dxdeta * dydxi)

        rix = real(ix,myprec)+0.5
        riy = real(iy,myprec)
        call GridFunctions(rix,riy, x,y, dxdxi, dydxi, dxdeta, dydeta)
        dVe(irow) =( dxdxi * dydeta - dxdeta * dydxi)
     end do
! $ OMP END PARALLEL DO

     do iy = 0,n-1
        sumQ = 0
        sumQX = 0
        do ix = 0,m-1
           sumQ = sumQ + dVc(ix+m*iy)
           sumQX = sumQX + dVe(ix+m*iy)
        end do
        do ix = 0,m-1
           dVe(ix+m*iy) = dVe(ix+m*iy) * sumQ/sumQX
        end do
     end do
     do ix = 0,m-1
        sumQ = 0
        sumQY = 0
        do iy = 0,n-1
           sumQ = sumQ + dVc(ix+m*iy)
           sumQY = sumQY + dVn(ix+m*iy)
        end do
        do iy = 0,n-1
           dVn(ix+m*iy) = dVn(ix+m*iy) * sumQ/sumQY
        end do
     end do
  end subroutine setIntegration

  subroutine FixItp(Fin, Fout, dx)
  ! Calculate a low-rank correction:
  !
  !    Fout_after * diag(dx) * Fin_after^T * Fin_before = Fout_before
  use Settings, only: myprec
  implicit none
     real(myprec) :: Fin(0:,:), Fout(0:,:)
     real(myprec), intent(in) :: dx(0:)

     real(myprec) :: scaling
     integer :: j, k
     do j = 1,size(Fin,2)
        scaling = sqrt(sum(dx*Fin(:,j)**2))
        Fin(:,j)  = Fin(:,j)  / scaling
        Fout(:,j) = Fout(:,j) / scaling
        do k = j+1,size(Fin,2)
           scaling = sum(Fin(:,j) * dx * Fin(:,k))
           Fin(:,k)  = Fin(:,k)  - Fin(:,j)  * scaling
           Fout(:,k) = Fout(:,k) - Fout(:,j) * scaling
        end do
     end do
  end subroutine FixItp

  subroutine InterpolateExactFunctions()
  ! Calulate the low-rank correction matrices that make the interpolations exact
  use Util,             only: error_exit
  use Settings,         only: myprec
  use Inputs,           only: m,n
  use FDApplyOperators, only: ExactInE2C,  ExactInN2C,  ExactInC2E,  ExactInC2N
  use FDApplyOperators, only: ExactOutE2C, ExactOutN2C, ExactOutC2E, ExactOutC2N, InterpV2C, InterpC2V
  use GridValues,       only: dVe, dVn, dVc
  use GridValues,       only: GridOrientationYatE,  GridOrientationYatN, GridOrientationXatE, GridOrientationXatN, GridOrientationXatC, GridOrientationYatC
  use omp_lib
  implicit none

     integer :: ierr
     integer :: ix, iy, j
     real(myprec), allocatable :: Fin(:,:), Fout(:,:), dx(:)
     real(myprec), allocatable :: testX(:), testY(:), testP(:), itpTestX(:), itpTestY(:)
     real(myprec), pointer :: Gx(:,:), Gy(:,:)
     real(myprec), pointer :: Fx(:,:), Fy(:,:)
     real(myprec), pointer :: Hx(:,:), Hy(:,:)

     integer :: irow
     logical, parameter :: verbose=.false.

     allocate( ExactInE2C(0:n*m-1,5),  ExactInN2C(0:n*m-1,5),  ExactInC2E(0:n*m-1,5),  ExactInC2N(0:n*m-1,5),  &
               ExactOutE2C(0:n*m-1,5), ExactOutN2C(0:n*m-1,5), ExactOutC2E(0:n*m-1,5), ExactOutC2N(0:n*m-1,5),  &
               stat=ierr)
     if (ierr /= 0) call error_exit('allocation error')
     if (ierr/=0) stop

     ! Calculate corrected interpolation formulas
     !    from y-points to p-points
     do ix = 0,m-1
        do iy = 0,n-1
           irow = ix + m * iy

           ExactInE2C(irow,1) = 1
           ExactInE2C(irow,2) = GridOrientationXatE(ix,iy)
           ExactInE2C(irow,3) = GridOrientationYatE(ix,iy)
           ExactInE2C(irow,4) = GridOrientationYatE(ix,iy)**2
           ExactInE2C(irow,5) = GridOrientationYatE(ix,iy)*GridOrientationXatE(ix,iy)

           ExactInN2C(irow,1) = 1
           ExactInN2C(irow,2) = GridOrientationXatN(ix,iy)
           ExactInN2C(irow,3) = GridOrientationYatN(ix,iy)
           ExactInN2C(irow,4) = GridOrientationYatN(ix,iy)**2
           ExactInN2C(irow,5) = GridOrientationYatN(ix,iy)*GridOrientationXatN(ix,iy)

           ExactInC2E(irow,1) = 1
           ExactInC2E(irow,2) = GridOrientationXatC(ix,iy)
           ExactInC2E(irow,3) = GridOrientationYatC(ix,iy)
           ExactInC2E(irow,4) = GridOrientationYatC(ix,iy)**2
           ExactInC2E(irow,5) = GridOrientationYatC(ix,iy)*GridOrientationXatC(ix,iy)

           ExactInC2N(irow,1) = 1
           ExactInC2N(irow,2) = GridOrientationXatC(ix,iy)
           ExactInC2N(irow,3) = GridOrientationYatC(ix,iy)
           ExactInC2N(irow,4) = GridOrientationYatC(ix,iy)**2
           ExactInC2N(irow,5) = GridOrientationYatC(ix,iy)*GridOrientationXatC(ix,iy)
        end do
     end do

     do j = 1,5
        call InterpV2C(ExactInE2C(:,j),ExactInN2C(:,j),ExactOutE2C(:,j),ExactOutN2C(:,j),set_zero=.true.)
        call InterpC2V(ExactInC2E(:,j),ExactInC2N(:,j),ExactOutC2E(:,j),ExactOutC2N(:,j),set_zero=.true.)
     end do

     do ix = 0,m-1
        do iy = 0,n-1
           irow = ix + m * iy

           ExactOutE2C(irow,1) = 1                                                         - ExactOutE2C(irow,1)
           ExactOutE2C(irow,2) = GridOrientationXatC(ix,iy)                                - ExactOutE2C(irow,2)
           ExactOutE2C(irow,3) = GridOrientationYatC(ix,iy)                                - ExactOutE2C(irow,3)
           ExactOutE2C(irow,4) = GridOrientationYatC(ix,iy)**2                             - ExactOutE2C(irow,4)
           ExactOutE2C(irow,5) = GridOrientationYatC(ix,iy)*GridOrientationXatC(ix,iy)     - ExactOutE2C(irow,5)

           ExactOutN2C(irow,1) = 1                                                         - ExactOutN2C(irow,1)
           ExactOutN2C(irow,2) = GridOrientationXatC(ix,iy)                                - ExactOutN2C(irow,2)
           ExactOutN2C(irow,3) = GridOrientationYatC(ix,iy)                                - ExactOutN2C(irow,3)
           ExactOutN2C(irow,4) = GridOrientationYatC(ix,iy)**2                             - ExactOutN2C(irow,4)
           ExactOutN2C(irow,5) = GridOrientationYatC(ix,iy)*GridOrientationXatC(ix,iy)     - ExactOutN2C(irow,5)

           ExactOutC2E(irow,1) = 1                                                         - ExactOutC2E(irow,1)
           ExactOutC2E(irow,2) = GridOrientationXatE(ix,iy)                                - ExactOutC2E(irow,2)
           ExactOutC2E(irow,3) = GridOrientationYatE(ix,iy)                                - ExactOutC2E(irow,3)
           ExactOutC2E(irow,4) = GridOrientationYatE(ix,iy)**2                             - ExactOutC2E(irow,4)
           ExactOutC2E(irow,5) = GridOrientationYatE(ix,iy)*GridOrientationXatE(ix,iy)     - ExactOutC2E(irow,5)

           ExactOutC2N(irow,1) = 1                                                         - ExactOutC2N(irow,1)
           ExactOutC2N(irow,2) = GridOrientationXatN(ix,iy)                                - ExactOutC2N(irow,2)
           ExactOutC2N(irow,3) = GridOrientationYatN(ix,iy)                                - ExactOutC2N(irow,3)
           ExactOutC2N(irow,4) = GridOrientationYatN(ix,iy)**2                             - ExactOutC2N(irow,4)
           ExactOutC2N(irow,5) = GridOrientationYatN(ix,iy)*GridOrientationXatN(ix,iy)     - ExactOutC2N(irow,5)
        end do
     end do

     if (verbose) then
        print *
        print *
        print *,'For E2C-interpolation:'
        do j = 1,5
           print *,'Error in exact interpolation of function ',j,':', &
               sqrt(sum( dVc*ExactOutE2C(:,j)**2))
        end do
        print *
        print *,'For N2C-interpolation:'
        do j = 1,5
           print *,'Error in exact interpolation of function ',j,':', &
               sqrt(sum( dVc*ExactOutN2C(:,j)**2))
        end do
        print *
        print *,'For C2E-interpolation:'
        do j = 1,5
           print *,'Error in exact interpolation of function ',j,':', &
               sqrt(sum( dVe*ExactOutC2E(:,j)**2))
        end do
        print *
        print *,'For C2N-interpolation:'
        do j = 1,5
           print *,'Error in exact interpolation of function ',j,':', &
               sqrt(sum( dVn*ExactOutC2N(:,j)**2))
        end do
     end if

     allocate(Fin(0:m-1,5), Fout(0:m-1,5), dx(0:m-1), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')
     if (ierr/=0) stop
     do iy = 0,n-1
        do ix = 0,m-1
           irow = ix + m * iy
           dx(ix) = dVe(irow)
           do j = 1,5
              Fin(ix,j)  = ExactInE2C(irow,j)
              Fout(ix,j) = ExactOutE2C(irow,j)
           end do
        end do

        call FixItp(Fin,Fout,dx)

        do ix = 0,m-1
           irow = ix + m * iy
           do j = 1,5
              ExactInE2C(irow,j) = Fin(ix,j)
              ExactOutE2C(irow,j)= Fout(ix,j)
           end do
        end do

        do ix = 0,m-1
           irow = ix + m * iy
           dx(ix) = dVc(irow)
           do j = 1,5
              Fin(ix,j)  = ExactInC2E(irow,j)
              Fout(ix,j) = ExactOutC2E(irow,j)
           end do
        end do

        call FixItp(Fin,Fout,dx)

        do ix = 0,m-1
           irow = ix + m * iy
           do j = 1,5
              ExactInC2E(irow,j) = Fin(ix,j)
              ExactOutC2E(irow,j)= Fout(ix,j)
           end do
        end do
     end do
     deallocate(Fin, Fout, dx, stat=ierr)
     if (ierr/=0) call error_exit('deallocation error')

     allocate(Fin(0:n-1,5), Fout(0:n-1,5), dx(0:n-1), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')
     if (ierr/=0) stop

     do ix = 0,m-1
        do iy = 0,n-1
           irow = ix + m * iy
           dx(iy) = dVn(irow)
           do j = 1,5
              Fin(iy,j)  = ExactInN2C(irow,j)
              Fout(iy,j) = ExactOutN2C(irow,j)
           end do
        end do

        call FixItp(Fin,Fout,dx)

        do iy = 0,n-1
           irow = ix + m * iy
           do j = 1,5
              ExactInN2C(irow,j) = Fin(iy,j)
              ExactOutN2C(irow,j)= Fout(iy,j)
           end do
        end do

        do iy = 0,n-1
           irow = ix + m * iy
           dx(iy) = dVc(irow)
           do j = 1,5
              Fin(iy,j)  = ExactInC2N(irow,j)
              Fout(iy,j) = ExactOutC2N(irow,j)
           end do
        end do

        call FixItp(Fin,Fout,dx)

        do iy = 0,n-1
           irow = ix + m * iy
           do j = 1,5
              ExactInC2N(irow,j) = Fin(iy,j)
              ExactOutC2N(irow,j)= Fout(iy,j)
           end do
        end do
     end do

     deallocate(Fin, Fout, dx, stat=ierr)
     if (ierr/=0) call error_exit('deallocation error')

     if (verbose) then
        print *
        print *
        print *,'For E2C-interpolation:'
        do j = 1,5
           print *,'Correction for exact interpolation of function ',j,':', &
               sqrt(sum( dVc*ExactOutE2C(:,j)**2))
        end do
        print *
        print *,'For N2C-interpolation:'
        do j = 1,5
           print *,'Correction for exact interpolation of function ',j,':', &
               sqrt(sum( dVc*ExactOutN2C(:,j)**2))
        end do
        print *
        print *,'For C2E-interpolation:'
        do j = 1,5
           print *,'Correction for exact interpolation of function ',j,':', &
               sqrt(sum( dVe*ExactOutC2E(:,j)**2))
        end do
        print *
        print *,'For C2N-interpolation:'
        do j = 1,5
           print *,'Correction for exact interpolation of function ',j,':', &
               sqrt(sum( dVn*ExactOutC2N(:,j)**2))
        end do

        allocate( testX(0:n*m-1), testY(0:n*m-1), testP(0:n*m-1), ItpTestX(0:n*m-1), ItpTestY(0:n*m-1), stat=ierr)
        if (ierr /= 0) call error_exit('allocation error test-arrays')
        if (ierr/=0) stop

        Gx => GridOrientationXatC
        Gy => GridOrientationYatC
        Fx => GridOrientationXatE
        Fy => GridOrientationYatE
        Hx => GridOrientationXatN
        Hy => GridOrientationYatN
        do j = 1,5
          print *
          if (j==1) then
             print *,'Interpolating f=1'
          else if (j==2) then
             print *,'Interpolating f=cos(alpha)'
          else if (j==3) then
             print *,'Interpolating f=sin(alpha)'
          else if (j==4) then
             print *,'Interpolating f=sin(alpha)**2'
          else if (j==5) then
             print *,'Interpolating f=sin(alpha)*cos(alpha)'
          end if
           do iy = 0,n-1
              do ix = 0,m-1
                 irow = ix + m * iy
                 if (j==1) then
                    testP(irow) = 1
                    testX(irow) = 1
                    testY(irow) = 1
                 else if (j==2) then
                    testP(irow) = Gx(ix,iy)
                    testX(irow) = Fx(ix,iy)
                    testY(irow) = Hx(ix,iy)
                 else if (j==3) then
                    testP(irow) = Gy(ix,iy)
                    testX(irow) = Fy(ix,iy)
                    testY(irow) = Hy(ix,iy)
                 else if (j==4) then
                    testP(irow) = Gy(ix,iy)**2
                    testX(irow) = Fy(ix,iy)**2
                    testY(irow) = Hy(ix,iy)**2
                 else if (j==5) then
                    testP(irow) = Gy(ix,iy)*Gx(ix,iy)
                    testX(irow) = Fy(ix,iy)*Fx(ix,iy)
                    testY(irow) = Hy(ix,iy)*Hx(ix,iy)
                 end if
              end do
           end do
           call InterpC2V(testP, testP, itpTestX, itpTestY, set_zero = .true., exactAtV=.true.)
           print *,'   max(testX-itpX*testP)=',maxval(abs(testX-itpTestX))
           print *,'   max(testY-itpY*testP)=',maxval(abs(testY-itpTestY))

           call InterpV2C(testX, testY, itpTestX, itpTestY, set_zero = .true., exactAtC=.true.)
           print *,'   max(testP-itpX*testX)=',maxval(abs(testP-itpTestX))
           print *,'   max(testP-itpY*testX)=',maxval(abs(testP-itpTestY))
        end do

        deallocate(testX, testY, testP, ItpTestY, ItpTestX, stat=ierr)
        if (ierr/=0) call error_exit('deallocation error')
        if (ierr/=0) stop
     end if
  end subroutine InterpolateExactFunctions

  ! Calulate the difference matrices needed to evaluate the DIV and GRAD operators
  subroutine CalcDifferenceMatrices()
  use Util,             only: error_exit
  use Settings,         only: myprec
  use Inputs,           only: m,n, deriv, GridFunctions
  use FDApplyOperators, only: DivXiAtE, DivEtaAtE, DivXiAtN, DivEtaAtN
  use FDApplyOperators, only: GradXiAtE, GradEtaAtE, GradXiAtN, GradEtaAtN
  use GridValues,       only: dVe, dVn, dVc
  use GridValues,       only: GridOrientationYatE,  GridOrientationYatN, GridOrientationXatE, GridOrientationXatN
  implicit none

     integer      :: nDeriv
     integer      :: ix, iy
     real(myprec) :: perc
     real(myprec) :: xstart, ystart, xend, yend
     integer      :: irow, di, icol

     nDeriv     = abs(lbound(deriv,1))

     ! Calculate divergence operator
     perc   = 5
     do iy = 0,n-1
        do ix = 0,m-1
           if (100*(ix + m*iy) > m*n * perc) then
              write(*,'(i0,a,$)') nint(perc),'% '
              perc = perc + 5
           end if

           irow = ix + m * iy

           ! from x-points to p-points
           do di = -nderiv, nderiv-1
              icol = modulo(ix+di,m) + m * iy
              call GridFunctions(real(ix+di+0.5,myprec),real(iy+(di+0.5),myprec), x=xend,  y=yend)
              call GridFunctions(real(ix+di+0.5,myprec),real(iy-(di+0.5),myprec), x=xstart,y=ystart)

              ! Cell face goes from (xtart,ystart) to (xend,yend).
              ! flux is  (yend-ystart, xstart-xend) * vector
              ! Available components are
              !    vxi  =  Ax*vx + Ay*vy
              !    veta = -Ay*vx + Ax*vy
              !    vx   =  Ax*vxi  - Ay*veta
              !    vy   =  Ay*vxi  + Ax*veta
              ! flux is  (yend-ystart, xstart-xend) * (Ax*vxi - Ay*veta, Ay*vxi+Ax*veta)
              !   =      ( (yend-ystart)*Ax + Ay*(xstart-xend) ) * vxi
              !    +      ((ystart-yend)*Ay + Ax*(xstart-xend) ) * veta
              DivXiAtE(irow,di) =  deriv(di) / (2*di+1) / dVc(irow) *  &
                     (   (yend-ystart)*GridOrientationXatE(modulo(ix+di,m),iy) &
                       + (xstart-xend)*GridOrientationYatE(modulo(ix+di,m),iy) )
              DivEtaAtE(irow,di) =  deriv(di)  / (2*di+1) / dVc(irow) *  &
                     (   (ystart-yend)*GridOrientationYatE(modulo(ix+di,m),iy) &
                       + (xstart-xend)*GridOrientationXatE(modulo(ix+di,m),iy) )
              GradXiAtE(icol,-di)  =  -DivXiAtE(irow,di)  * dVc(irow) / dVe(icol)
              GradEtaAtE(icol,-di) =  -DivEtaAtE(irow,di) * dVc(irow) / dVe(icol)
           end do

           ! from y-points to p-points
           do di = -nderiv, nderiv-1
              icol = ix + m * modulo(iy+di,n)
              call GridFunctions(real(ix+(di+0.5),myprec),real(iy+di+0.5,myprec), x=xend,  y=yend)
              call GridFunctions(real(ix-(di+0.5),myprec),real(iy+di+0.5,myprec), x=xstart,y=ystart)

              ! Cell face goes from (xtart,ystart) to (xend,yend).
              ! flux is  (ystart-yend, xend-xstart) * vector
              ! Available components are
              !    vxi  =  Ax*vx + Ay*vy
              !    veta = -Ay*vx + Ax*vy
              !    vx   =  Ax*vxi  - Ay*veta
              !    vy   =  Ay*vxi  + Ax*veta
              ! flux is  (ystart-yend, xend-xstart) * (Ax*vxi - Ay*veta, Ay*vxi+Ax*veta)
              !   =      ( (ystart-yend)*Ax + Ay*(xend-xstart) ) * vxi
              !    +      ((yend-ystart)*Ay + Ax*(xend-xstart) ) * veta
              DivXiAtN(irow,di) =  deriv(di)  / (2*di+1) / dVc(irow) *  &
                     (   (ystart-yend)*GridOrientationXatN(ix,modulo(iy+di,n)) &
                       + (xend-xstart)*GridOrientationYatN(ix,modulo(iy+di,n)) )
              DivEtaAtN(irow,di) =  deriv(di)  / (2*di+1) /dVc(irow) *  &
                     (   (yend-ystart)*GridOrientationYatN(ix,modulo(iy+di,n)) &
                       + (xend-xstart)*GridOrientationXatN(ix,modulo(iy+di,n)) )
              GradXiAtN(icol,-di)  =  -DivXiAtN(irow,di)  * dVc(irow) / dVn(icol)
              GradEtaAtN(icol,-di) =  -DivEtaAtN(irow,di) * dVc(irow) / dVn(icol)
           end do
        end do
     end do

     print *

  end subroutine CalcDifferenceMatrices

end module CalcMatrices_FD

! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module CalcMatrices_LinearGalerkin
! The discrete operators used in the Galerkin-type scheme for the linear wave equation
! (DIV and GRAD) are calculated in this module.
contains

  ! Apply Richardson-interpolation on a sequence of approximations (with 2nd order convergence)
  ! to calculate a more accurate approximation: version for 2-dim arrays (1-dim times accuracy dimension)
  subroutine RichardsonInterpolation2(integrals)
  use Inputs,   only: P
  use Settings, only: myprec
  implicit none

     real(myprec), intent(inout) :: integrals(:,0:)
     integer                     :: r, q, dix, i_rowcol, n_rowcol

     n_rowcol = size(integrals,1)
!$OMP PARALLEL DO PRIVATE(dix, q, r)
     do i_rowcol = 1,n_rowcol
        do q = 1,P
           integrals(i_rowcol,q) = integrals(i_rowcol,q) + integrals(i_rowcol,q-1)
        end do

        dix = 1
        do q = 0,P
           integrals(i_rowcol,q) = integrals(i_rowcol,q) / (dix*dix)
           dix = dix * 2
        end do

        do q = 1,P
           do r = P,q,-1
              integrals(i_rowcol,r) = ( (4**q)*integrals(i_rowcol,r)-integrals(i_rowcol,r-1))/(4**q-1)
           end do
        end do
     end do
!$OMP END PARALLEL DO

  end subroutine RichardsonInterpolation2

  subroutine RichardsonInterpolation3(integrals)
  ! Apply Richardson-interpolation on a sequence of approximations (with 2nd order convergence)
  ! to calculate a more accurate approximation: version for 3-dim arrays (2-dim times accuracy dimension)
  use Inputs,   only: P
  use Settings, only: myprec
  implicit none

     real(myprec), intent(inout) :: integrals(:,:,0:)
     integer                     :: r, q, dix, i_rowcol, n_row, n_rowcol, i_row, j_col

     n_row    = size(integrals,1)
     n_rowcol = n_row * size(integrals,2)
!$OMP PARALLEL DO PRIVATE(dix, q, r, i_row, j_col)
     do i_rowcol = 1,n_rowcol

        i_row = 1+mod(i_rowcol - 1,n_row)
        j_col = 1+(i_rowcol - 1)/n_row

        do q = 1,P
           integrals(i_row,j_col,q) = integrals(i_row,j_col,q) + integrals(i_row,j_col,q-1)
        end do

        dix = 1
        do q = 0,P
           integrals(i_row,j_col,q) = integrals(i_row,j_col,q) / (dix*dix)
           dix = dix * 2
        end do

        do q = 1,P
           do r = P,q,-1
              integrals(i_row,j_col,r) = ( (4**q)*integrals(i_row,j_col,r)-integrals(i_row,j_col,r-1))/(4**q-1)
           end do
        end do
     end do
!$OMP END PARALLEL DO

  end subroutine RichardsonInterpolation3

  subroutine CorrectInterpolation(Ax, Ay, wav, Ori, scal)
  ! Solve a LS system to make the interpolation correct for the given function:
  !      Ori * wav_out = [Ax;Ay], with
  !     |scal * (wav_out-wav_in)| as small as possible.
  use Settings, only: myprec
  implicit none
     real(myprec), intent(in)    :: Ax, Ay
     real(myprec), intent(inout) :: wav(:)
     real(myprec), intent(in)    :: scal(:), Ori(:,:)

     real(myprec)       :: M1, M2, DetMM, dwav
     real(myprec)       :: rhs(2), Mrhs(2), MM(2,2), invMM(2,2)
     integer            :: n_row, i_row
     logical, parameter :: verbose=.false.

     n_row = size(wav,1)

     rhs(1) = Ax
     rhs(2) = Ay
     MM(1:2,1:2) = 0
     do i_row = 1, n_row
        rhs(1) = rhs(1) - wav(i_row) * Ori(1,i_row)
        rhs(2) = rhs(2) - wav(i_row) * Ori(2,i_row)

        M1 = Ori(1,i_row) * scal(i_row)
        M2 = Ori(2,i_row) * scal(i_row)

        MM(1,1) = MM(1,1) + M1 * M1
        MM(1,2) = MM(1,2) + M1 * M2
        MM(2,2) = MM(2,2) + M2 * M2
     end do

     MM(2,1) = MM(1,2)
     DetMM = MM(1,1) * MM(2,2) - MM(1,2)*MM(1,2)
     invMM(1,1) = MM(2,2)/DetMM
     invMM(2,2) = MM(1,1)/DetMM
     invMM(1,2) = -MM(1,2)/DetMM
     invMM(2,1) = -MM(2,1)/DetMM

     Mrhs(1) = invMM(1,1) * rhs(1) + invMM(1,2) * rhs(2)
     Mrhs(2) = invMM(2,1) * rhs(1) + invMM(2,2) * rhs(2)

     if (verbose) then
        rhs(1) = 0
        rhs(2) = 0
        print '(10(a,f0.8))', ' (',Ax,',',Ay,') != 0+'
        do i_row = 1,n_row
           print '(10(a,f0.8))', &
               '    + (',wav(i_row),') * (',Ori(1,i_row),',',Ori(2,i_row),')'
           rhs(1) = rhs(1) + wav(i_row) * Ori(1,i_row)
           rhs(2) = rhs(2) + wav(i_row) * Ori(2,i_row)
        end do
        print '(10(a,f0.8))', ' = (',rhs(1),',',rhs(2),')'

        rhs(1) = 0
        rhs(2) = 0
        print '(10(a,f11.8))', ' (',Ax,',',Ay,') = 0+'
        do i_row = 1,n_row
           M1 = Ori(1,i_row) * scal(i_row)
           M2 = Ori(2,i_row) * scal(i_row)
           dwav = (M1 * Mrhs(1) + M2 * Mrhs(2) ) * scal(i_row)
           rhs(1) = rhs(1) + (wav(i_row)+dwav) *Ori(1,i_row)
           rhs(2) = rhs(2) + (wav(i_row)+dwav) *Ori(2,i_row)
           print '(10(a,f11.8))', &
               '    + (',wav(i_row),'+',dwav/max(1e-8,scal(i_row)),'*',max(1e-8,scal(i_row)),') * (',Ori(1,i_row),',',Ori(2,i_row),')'
        end do
        print '(10(a,f11.8))', ' =(',rhs(1),',',rhs(2),')'
     end if

     do i_row = 1,n_row
        M1 = Ori(1,i_row) * scal(i_row)
        M2 = Ori(2,i_row) * scal(i_row)
        wav(i_row) = wav(i_row) + (M1 * Mrhs(1) + M2 * Mrhs(2) ) * scal(i_row)
     end do
  end subroutine CorrectInterpolation

  subroutine Integrate(ij, perc, ijEnd, idxP, wavelet, dwavelet, wavescale, dwavPAx, dwavPAy, wavx, scalx, wavy, scaly, &
                       IntegralsQ, IntegralsQX, integralsQY, integralsX, integralsY, OriAtE, OriAtN)
  use Util,       only: error_exit
  use GridValues, only: GridOrientationYatE,  GridOrientationYatN, GridOrientationXatE, GridOrientationXatN
  use GridValues, only: dVe, dVn, dVc
  use Settings,   only: myprec
  use Inputs,     only: m,n, P, scaleCurv, GridFunctions, nSpan
  use GridTrafo , only: GridOrientation
  use LinearWaveEqGalerkin, only: ValGradX, ValGradY, ValDivX, ValDivY
  use omp_lib
  implicit none
     integer,                   intent(in)    :: ij
     integer,                   intent(in)    :: ijEnd
     real(myprec),              intent(inout) :: perc
     real(myprec), allocatable, intent(in)    :: wavelet(:), dwavelet(:), wavescale(:)
     integer,                   intent(in)    :: idxP(0:)
     real(myprec),              intent(inout) :: dwavPAx(:), dwavPAy(:)
     real(myprec),              intent(inout) :: wavx(:), scalx(:), wavy(:), scaly(:)
     real(myprec),              intent(inout) :: IntegralsQ(:,0:), IntegralsQX(:,0:), integralsQY(:,0:)
     real(myprec),              intent(inout) :: integralsX(:,:,0:), integralsY(:,:,0:)
     real(myprec),              intent(inout) :: OriAtE(:,:), OriAtN(:,:)

     real(myprec) :: wavP, dwavPxi, dwavPeta, dwavePx, dwavePy
     real(myprec) :: xi, eta, x, y
     real(myprec) :: dxdxi, dxdeta, dydxi, dydeta, dV, Ax, Ay
     integer      :: nNeigh, nNeighP
     integer      :: i_col, xi_col, eta_col, ij_row, ij_col
     integer      :: it
     integer      :: ix, iy, jy, jx, vy, vx, twoP, q
     integer      :: i_row, xi_row, eta_row
     integer      :: ia
     integer      :: kx, ky

     it = 1+omp_get_thread_num()
     twoP    = 2**P
     nNeigh  = (2*nSpan+1)*(2*nSpan)
     nNeighP = (2*nSpan)*(2*nSpan)

     iy = ij/m
     ix = mod(ij,m)
     if (it==1 .and. 100*ij > ijEnd * perc) then
        write(*,'(i0,a,$)') nint(perc),'% '
        perc = perc + 5
     end if

     ! All integrations in (xi,eta) = (ix:ix+1,iy:iy+1)
     integralsQ(:,:)   = 0
     integralsQX(:,:)  = 0
     integralsQY(:,:)  = 0
     integralsX(:,:,:) = 0
     integralsY(:,:,:) = 0

     do i_row = 1,nNeigh
        xi_row  = mod(i_row-1,2*nSpan+1)-nSpan
        eta_row = (i_row-1)/(2*nSpan+1)+1-nSpan
        xi_row  = modulo(ix+xi_row,m)
        eta_row = modulo(iy+eta_row,n)
        OriAtE(1,i_row) = GridOrientationXatE(xi_row,eta_row)
        OriAtE(2,i_row) = GridOrientationYatE(xi_row,eta_row)

        eta_row = mod(i_row-1,2*nSpan+1)-nSpan
        xi_row  = (i_row-1)/(2*nSpan+1)+1-nSpan
        xi_row  = modulo(ix+xi_row,m)
        eta_row = modulo(iy+eta_row,n)

        OriAtN(1,i_row) = GridOrientationXatN(xi_row,eta_row)
        OriAtN(2,i_row) = GridOrientationYatN(xi_row,eta_row)
     end do

     do jy = 0, twoP
        vy = 2
        if (jy==0 .or. jy==twoP) vy=1
        eta = iy + dble(jy)/twoP
        do jx = 0, twoP
           vx = 2
           if (jx==0 .or. jx==twoP) vx = 1
           q = max(idxP(mod(jx,twoP)),idxP(mod(jy,twoP)))
           xi = ix + dble(jx)/twoP

           call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
           dV = dxdxi * dydeta - dxdeta * dydxi
           call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)

           do i_col = 1,nNeighP
              xi_col  = mod(i_col-1,2*nSpan)+1-nSpan
              eta_col = (i_col-1)/(2*nSpan)+1-nSpan
              wavP     =  wavelet(jx-xi_col*(twoP+1)) *  wavelet(jy-eta_col*(twoP+1))
              dwavPxi  = dwavelet(jx-xi_col*(twoP+1)) *  wavelet(jy-eta_col*(twoP+1))
              dwavPeta =  wavelet(jx-xi_col*(twoP+1)) * dwavelet(jy-eta_col*(twoP+1))

              dwavePx  = ( dwavPxi*dydeta - dwavPeta*dydxi )/dV
              dwavePy  = (-dwavPxi*dxdeta + dwavPeta*dxdxi )/dV
              dwavPAx(i_col) =  Ax * dwavePx + Ay *  dwavePy
              dwavPAy(i_col) = -Ay * dwavePx + Ax *  dwavePy

              IntegralsQ(i_col,q) = IntegralsQ(i_col,q) + vx * vy * wavP * dV/4
           end do

           ! Calculate the interpolation functions at the grid point (ix + jx/nSpan, iy+jy/nSpan)
           do i_row = 1,nNeigh
              xi_row = mod(i_row-1,2*nSpan+1)-nSpan
              eta_row = (i_row-1)/(2*nSpan+1)+1-nSpan

              if (jx>=twoP/2) then
                 kx = jx-twoP/2 -     xi_row*(twoP+1)
              else
                 kx = jx+twoP/2 - (xi_row+1)*(twoP+1)
              end if
              ky = jy-eta_row*(twoP+1)

              if (-nSpan*(twoP+1) <= kx .and. kx < nSpan*(twoP+1)) then
                 wavx(i_row)  = wavelet(kx)   *  wavelet(ky)
                 scalx(i_row) = wavescale(kx) *  wavescale(ky)
              else
                 wavx(i_row)  = 0
                 scalx(i_row) = 0
              end if

              eta_row = mod(i_row-1,2*nSpan+1)-nSpan
              xi_row = (i_row-1)/(2*nSpan+1)+1-nSpan
              kx = jx-xi_row*(twoP+1)
              if (jy>=twoP/2) then
                 ky = jy-twoP/2 - eta_row*(twoP+1)
              else
                 ky = jy+twoP/2 - (eta_row+1)*(twoP+1)
              end if
              if (-nSpan*(twoP+1) <= ky .and. ky < nSpan*(twoP+1)) then
                 wavy(i_row)  = wavelet(kx)   * wavelet(ky)
                 scaly(i_row) = wavescale(kx) * wavescale(ky)
              else
                 wavy(i_row)  = 0
                 scaly(i_row) = 0
              end if
           end do

           if (abs(ScaleCurv)>0) then
              call CorrectInterpolation(Ax, Ay, wavx, OriAtE, scalx)
              call CorrectInterpolation(Ax, Ay, wavy, OriAtN, scaly)
           end if
           do i_col = 1,nNeighP
              xi_col  = mod(i_col-1,2*nSpan)+1-nSpan
              eta_col = (i_col-1)/(2*nSpan)+1-nSpan

              do i_row = 1,nNeigh
                 xi_row = mod(i_row-1,2*nSpan+1)-nSpan
                 eta_row = (i_row-1)/(2*nSpan+1)+1-nSpan
                 IntegralsX(i_row,i_col,q) = IntegralsX(i_row,i_col,q) + &
                        vx * vy * wavx(i_row) * dwavPAx(i_col) * dV / 4
              end do
              do i_row = 1,nNeigh
                 eta_row = mod(i_row-1,2*nSpan+1)-nSpan
                 xi_row = (i_row-1)/(2*nSpan+1)+1-nSpan
                 IntegralsY(i_row,i_col,q) = IntegralsY(i_row,i_col,q) + &
                        vx * vy * wavy(i_row) * dwavPAy(i_col) * dV / 4
              end do
           end do

           do i_row = 1,nNeigh
              IntegralsQX(i_row,q) = IntegralsQX(i_row,q) + vx * vy * wavx(i_row) * dV/4
              IntegralsQY(i_row,q) = IntegralsQY(i_row,q) + vx * vy * wavy(i_row) * dV/4
           end do
        end do
     end do

     call RichardsonInterpolation2(integralsQ)
     call RichardsonInterpolation2(integralsQX)
     call RichardsonInterpolation2(integralsQY)
     call RichardsonInterpolation3(integralsX)
     call RichardsonInterpolation3(integralsY)

     ! Construct matrices
!$OMP CRITICAL
     do i_col = 1,nNeighP
        xi_col  = ix + mod(i_col-1,2*nSpan)+1-nSpan
        eta_col = iy + (i_col-1)/(2*nSpan)+1-nSpan

        do i_row = 1,nNeigh
           xi_row  = ix + mod(i_row-1,2*nSpan+1)-nSpan
           eta_row = iy + (i_row-1)/(2*nSpan+1)+1-nSpan
           ia  = (xi_col - xi_row + 2*nSpan-1) + 4*nSpan * (eta_col - eta_row + 2*nSpan-1)
           ij_row  = modulo(xi_row,m) + m * modulo(eta_row,n)
           ValGradX(ij_row,ia) = ValGradX(ij_row,ia) + IntegralsX(i_row,i_col,P)
           ij_row  = modulo(xi_col,m) + m * modulo(eta_col,n)
           ValDivX(ij_row,ia)  = ValDivX(ij_row,ia)  + IntegralsX(i_row,i_col,P)

           eta_row = iy + mod(i_row-1,2*nSpan+1)-nSpan
           xi_row  = ix + (i_row-1)/(2*nSpan+1)+1-nSpan
           ia  = (eta_col - eta_row + 2*nSpan-1) + 4*nSpan * (xi_col - xi_row + 2*nSpan-1)
           ij_row  = modulo(xi_row,m) + m * modulo(eta_row,n)
           ValGradY(ij_row,ia) = ValGradY(ij_row,ia) + IntegralsY(i_row,i_col,P)
           ij_row  = modulo(xi_col,m) + m * modulo(eta_col,n)
           ValDivY(ij_row,ia)  = ValDivY(ij_row,ia)  + IntegralsY(i_row,i_col,P)
        end do
     end do

     do i_row = 1,nNeigh
        xi_row  = mod(i_row-1,2*nSpan+1)-nSpan
        eta_row = (i_row-1)/(2*nSpan+1)+1-nSpan
        ij_row  = modulo(ix+xi_row,m) + m * modulo(iy+eta_row,n)
        dVe(ij_row) = dVe(ij_row) + IntegralsQX(i_row,P)

        eta_row = mod(i_row-1,2*nSpan+1)-nSpan
        xi_row = (i_row-1)/(2*nSpan+1)+1-nSpan
        ij_row  = modulo(ix+xi_row,m) + m * modulo(iy+eta_row,n)
        dVn(ij_row) = dVn(ij_row) + IntegralsQY(i_row,P)
     end do

     do i_col = 1,nNeighP
        xi_col  = mod(i_col-1,2*nSpan)+1-nSpan
        eta_col = (i_col-1)/(2*nSpan)+1-nSpan
        ij_col  = modulo(ix+xi_col,m) + m * modulo(iy+eta_col,n)
        dVc(ij_col) = dVc(ij_col) + IntegralsQ(i_col,P)
     end do
!$OMP END CRITICAL
  end subroutine Integrate

  subroutine CalcMatrices()
  use GridTrafo,  only: GridOrientation
  use GridValues, only: dVe, dVn, dVc
  use Inputs,     only: GridFunctions, u_input, u_scale, nSpan, m,n, P
  use LinearWaveEqGalerkin, only: ValGradX, ValGradY, ValDivX, ValDivY, WriteMatrices
  use Settings,   only: myprec
  use Util,       only: error_exit
  use GalerkinBaseFunctions, only: EvalPiecewisePolynomial
  use omp_lib
  implicit none

     real(myprec), allocatable :: wavelet(:), dwavelet(:), wavescale(:), wavtmp(:)
     real(myprec), allocatable :: integralsX(:,:,:,:), integralsY(:,:,:,:)
     real(myprec), allocatable :: integralsQX(:,:,:), integralsQY(:,:,:), integralsQ(:,:,:)
     integer, allocatable      :: idxP(:)
     integer                   :: ierr
     integer                   :: ix, twoP, q, dix
     real(myprec), allocatable :: OriAtE(:,:,:), OriAtN(:,:,:)
     real(myprec), allocatable :: wavx(:,:), scalx(:,:), wavy(:,:), scaly(:,:)
     real(myprec)              :: perc
     real(myprec), allocatable ::  dwavPAx(:,:), dwavPAy(:,:)
     integer                   :: nNeigh, nNeighP
     integer                   :: ij, ij_row, ijStart, ijEnd, i,j0,ni,nj
     real(myprec)              :: dj
     integer                   :: ia, iaStart, iaEnd
     integer                   :: nt, it
     logical, parameter        :: save_matrices = .false.

     twoP   = 2**P
     ni     = nSpan*(twoP+1)
     nj     = size(u_scale,2)*(twoP+1)

     call EvalPiecewisePolynomial(u_scale,   wavtmp, twoP)
     call EvalPiecewisePolynomial(u_input,  wavelet, twoP)
     call EvalPiecewisePolynomial(u_input, dwavelet, twoP, derivOrder=1)
     allocate(wavescale(-ni:ni-1),stat=ierr)
     if (ierr/=0) call error_exit('Allocation error')
     do i = -ni,ni-1
        j0 = -nj +     (i+ni       )/(2*ni)
        dj = -nj + real(i+ni,myprec)/(2*ni) - j0
        if (j0==nj-1) then
           wavescale(i) = wavtmp(j0)
        else
           wavescale(i) = wavtmp(j0) + dj * (wavtmp(j0+1)  -  wavtmp(j0))
        end if
     end do

     deallocate(wavtmp,stat=ierr)
     if (ierr/=0) call error_exit('Deallocation error')

     perc   = 5
     nt     = omp_get_max_threads()
     nNeigh  = (2*nSpan+1)*(2*nSpan)
     nNeighP = (2*nSpan)*(2*nSpan)

     iaStart = 0
     iaEnd   = (4*nSpan)*(4*nSpan-1)-1

     allocate(OriAtE(2,(2*nSpan+1)*(2*nSpan),nt), &
              OriAtN(2,(2*nSpan+1)*(2*nSpan),nt), &
              wavx((2*nSpan+1)*(2*nSpan),nt),     &
              wavy((2*nSpan+1)*(2*nSpan),nt),     &
              scalx((2*nSpan+1)*(2*nSpan),nt),    &
              scaly((2*nSpan+1)*(2*nSpan),nt),    &
              IntegralsQ(nNeighP,0:P,nt),         &
              IntegralsQX(nNeigh,0:P,nt),         &
              IntegralsQY(nNeigh,0:P,nt),         &
              IntegralsX(nNeigh,nNeighP,0:P,nt),  &
              IntegralsY(nNeigh,nNeighP,0:P,nt),  &
              dwavPAx(nNeighP,nt),                &
              dwavPAy(nNeighP,nt),                &
              ValDivX( 0:m*n-1,iaStart:iaEnd),    &
              ValDivY( 0:m*n-1,iaStart:iaEnd),    &
              ValGradX(0:m*n-1,iaStart:iaEnd),    &
              ValGradY(0:m*n-1,iaStart:iaEnd),    &
              dVc(0:m*n-1),                       &
              dVe(0:m*n-1),                       &
              dVn(0:m*n-1),             stat=ierr)
     if (ierr/=0) call error_exit('Allocation error')

     ValDivX  = 0
     ValGradX = 0
     ValDivY  = 0
     ValGradY = 0

     allocate(idxP(0:twoP-1), stat=ierr)
     if (ierr/=0) call error_exit('Allocation error')

     dix = 1
     do q = P,0,-1
        do ix = 0,twoP-1,dix
           idxP(ix) = q
        end do
        dix = dix * 2
     end do

!$OMP PARALLEL PRIVATE(ij,ijStart,ijEnd,it)
     it = 1+omp_get_thread_num()
     ijStart = nint(real(it-1)*m*n/nt)
     ijEnd   = nint(real(it  )*m*n/nt)-1
     do ij = ijStart,ijEnd
        call Integrate(ij, perc, ijEnd, idxP, wavelet, dwavelet, wavescale, &
                       dwavPAx(:,it), dwavPAy(:,it), wavx(:,it), scalx(:,it), wavy(:,it), scaly(:,it), &
                       IntegralsQ(:,:,it), IntegralsQX(:,:,it), integralsQY(:,:,it), integralsX(:,:,:,it), integralsY(:,:,:,it), &
                       OriAtE(:,:,it), OriAtN(:,:,it) )
     end do
!$OMP END PARALLEL

!$OMP PARALLEL DO PRIVATE(ia)
     do ij_row = 0,m*n-1
        do ia = iaStart,iaEnd
           valDivX( ij_row,ia) = -valDiVX( ij_row,ia) / dVc( ij_row)
           valDivY( ij_row,ia) = -valDivY( ij_row,ia) / dVc( ij_row)
           ValGradX(ij_row,ia) = ValGradX(ij_row,ia) / dVe(ij_row)
           ValGradY(ij_row,ia) = ValGradY(ij_row,ia) / dVn(ij_row)
        end do
     end do
!$OMP END PARALLEL DO

     print *

     if (save_matrices) call WriteMatrices()

  end subroutine CalcMatrices
end module CalcMatrices_LinearGalerkin

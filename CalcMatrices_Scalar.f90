! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module CalcMatrices_Scalar
use Settings, only: myprec
implicit none

   real(myprec), pointer :: LaplaceMatrix(:,:)

contains

  subroutine CalcMatrices()
  use GridValues, only: dVc
  use Settings,   only: myprec
  use OutFile,    only: WriteMatrix, WriteDiagMatrix
  use Inputs,     only: u_input, P
  use GalerkinBaseFunctions, only: EvalPiecewisePolynomial
  implicit none
     real(myprec), allocatable :: wavelet(:), dwavelet(:)

     integer               :: twoP
     logical, parameter    :: save_matrices = .false.
     real(myprec), pointer :: ValQ(:,:)

     twoP   = 2**P
     call EvalPiecewisePolynomial(u_input,  wavelet, twoP)
     call EvalPiecewisePolynomial(u_input, dwavelet, twoP, derivOrder=1)

     call CalcLaplacian(wavelet,dwavelet,ValQ)

     if (save_matrices) then
        call WriteMatrix(LaplaceMatrix,'matA')
     end if
     call ValQ2dVp(ValQ)
     call ScaleLaplacian()
     if (save_matrices) then
        call WriteMatrix(LaplaceMatrix,'matAscaled')
        call WriteDiagMatrix(dVc,'vecQ')
     end if
  end subroutine CalcMatrices

   subroutine ScaleLaplacian()
   use GridValues, only: dVc
   use Inputs,     only: m,n
   implicit none
      integer irow, ia, nSpan
      nSpan = nint((sqrt(real(size(LaplaceMatrix,2))+1))/4)

      do irow = 1,m*n
         do ia = 1,(4*nSpan-1) * (4*nSpan-1)
            LaplaceMatrix(irow,ia) = -LaplaceMatrix(irow,ia) / dVc(irow)
         end do
      end do
   end subroutine ScaleLaplacian

   subroutine ValQ2dVp(ValQ)
   use GridValues, only: dVc
   use Settings,   only: myprec
   use Inputs,     only: m,n
   use Util,       only: error_exit
   implicit none
      real(myprec), pointer :: ValQ(:,:)
      integer               :: irow, ierr, ia, nSpan

      nSpan = nint((sqrt(real(size(LaplaceMatrix,2))+1))/4)
      allocate(dVc(m*n),stat=ierr)
      if (ierr/=0) call error_exit('allocation error')
      dVc = 0
      do irow = 1,m*n
         dVc(irow) = 0
         do ia = 1,(4*nSpan-1) * (4*nSpan-1)
           dVc(irow) = dVc(irow) + ValQ(irow,ia)
         end do
      end do
      deallocate(ValQ, stat=ierr)
      if (ierr/=0) call error_exit('deallocation error')
   end subroutine ValQ2dVp

  subroutine ProcessIntegrals(istart, iend, ix, iy, nSpan, &
              integralsA, integralsQ, ValQ)
  use Inputs,   only: m,n, P
  use Settings, only: myprec
  implicit none
     integer, intent(in)                      :: istart, iend
     integer, intent(in)                      :: ix, iy, nSpan
     real(myprec), allocatable, intent(inout) :: integralsA(:,:), integralsQ(:,:)
     real(myprec), pointer,     intent(inout) :: ValQ(:,:)

     integer            :: ix_row, xi_row, eta_row, ix_col, xi_col, eta_col
     integer            :: irow, icol, djcol, djrow
     integer            :: r, q, dix, i_rowcol
     logical, parameter :: verbose=.false.

     ix_row = 0
     xi_row  = -1
     eta_row = -1
     do i_rowcol = istart,iend
        if (i_rowcol>=ix_row*(ix_row+1)/2) then
           do while (i_rowcol > (ix_row*(ix_row+1))/2)
              ix_row = ix_row + 1
           end do
           xi_row  = 1-nSpan + mod(ix_row-1,2*nSpan)
           eta_row = 1-nSpan + (ix_row-1)/(2*nSpan)
        end if
        ix_col  = i_rowcol -  (ix_row*(ix_row-1))/2
        xi_col  = 1-nSpan + mod(ix_col-1,2*nSpan)
        eta_col = 1-nSpan + (ix_col-1)/(2*nSpan)

        irow  = n*modulo(ix+xi_row,m) + modulo(iy+eta_row,n) + 1
        icol  = n*modulo(ix+xi_col,m) + modulo(iy+eta_col,n) + 1

        if (verbose) then
           if (irow==1 .and. icol==1) print *,'i_rowcol=',i_rowcol
           if (irow==1 .and. icol==1) &
              print '(2(a,i0),a,1p,10(e14.6))', &
                'integralsA(',irow,',',irow,')=', &
                integralsA(i_rowcol,:)
           if (irow==1 .and. icol==1) stop 'finished'
        end if

        ! Apply Richardson-interpolation on a sequence of approximations (with 2nd order convergence)
        ! to calculate a more accurate approximation: version for integralsQ and integralsA
        do q = 1,P
           integralsQ(i_rowcol,q) = integralsQ(i_rowcol,q) + integralsQ(i_rowcol,q-1)
           integralsA(i_rowcol,q) = integralsA(i_rowcol,q) + integralsA(i_rowcol,q-1)
        end do
        dix = 1
        do q = 0,P
           integralsQ(i_rowcol,q) = integralsQ(i_rowcol,q) / (dix*dix)
           integralsA(i_rowcol,q) = integralsA(i_rowcol,q) / (dix*dix)
           dix = dix * 2
        end do

        do q = 1,P
           do r = P,q,-1
              integralsQ(i_rowcol,r) = ( (4**q)*integralsQ(i_rowcol,r)-integralsQ(i_rowcol,r-1))/(4**q-1)
              integralsA(i_rowcol,r) = ( (4**q)*integralsA(i_rowcol,r)-integralsA(i_rowcol,r-1))/(4**q-1)
           end do
        end do

        ! xi_col = 1-nSpan : nSpan
        ! xi_row = 1-nSpan : nSpan
        !-------------------------
        ! xi_col-xi_row = 1-2*nSpan : 2*nSpan-1       ! 2*nSpan-1-(1-2*nSpan)+1 = 4*nSpan-1

        djcol = (xi_col-xi_row+2*nSpan-1) + (4*nSpan-1) * (eta_col-eta_row+2*nSpan-1) + 1
        djrow = (xi_row-xi_col+2*nSpan-1) + (4*nSpan-1) * (eta_row-eta_col+2*nSpan-1) + 1

        if (verbose) then
           if (irow==1 .and. icol==1) &
              print '(2(a,i0),1p,10(a,e14.6))', &
                'LaplaceMatrix(',irow,',',irow,')=', &
                LaplaceMatrix(irow,djcol),' + ',integralsA(i_rowcol,P),'=', &
                LaplaceMatrix(irow,djcol) + integralsA(i_rowcol,P)
        end if
        LaplaceMatrix(irow,djcol) = LaplaceMatrix(irow,djcol) + integralsA(i_rowcol,P)
        ValQ(irow,djcol) = ValQ(irow,djcol) + integralsQ(i_rowcol,P)
        if (irow/=icol) then
           LaplaceMatrix(icol,djrow) = LaplaceMatrix(icol,djrow) + integralsA(i_rowcol,P)
           ValQ(icol,djrow) = ValQ(icol,djrow)  + integralsQ(i_rowcol,P)
        end if
     end do
  end subroutine ProcessIntegrals

  subroutine ProcessGridPoint(q,istart,iend, &
             nSpan, twoP, jx, jy, vx, vy, dV, &
             dxdxi, dydxi, dxdeta, dydeta, wavelet, dwavelet, &
             integralsA, integralsQ)
  use Settings, only: myprec
  implicit none
     integer,                   intent(in)    :: q
     integer,                   intent(in)    :: istart, iend
     integer,                   intent(in)    :: nSpan, twoP, jx, jy, vx, vy
     real(myprec),              intent(in)    :: dxdxi, dydxi, dxdeta, dydeta, dV
     real(myprec), allocatable, intent(in)    :: wavelet(:), dwavelet(:)
     real(myprec),              intent(inout) :: integralsA(:), integralsQ(:)

     integer            :: ix_row, i_rowcol, ix_col
     integer            :: xi_row, eta_row
     integer            :: xi_col, eta_col
     real(myprec)       :: wave_row, dwavexi_row, dwaveeta_row, dwavex_row, dwavey_row
     real(myprec)       :: wave_col, dwavexi_col, dwaveeta_col, dwavex_col, dwavey_col
     logical, parameter :: verbose=.false.

     ix_row     = 0
     eta_row    = -100
     xi_row     = -100
     wave_row   = -1
     dwavey_row = -1
     dwavex_row = -1
     do i_rowcol = istart,iend
        if (i_rowcol>=ix_row*(ix_row+1)/2) then
           do while (i_rowcol > (ix_row*(ix_row+1))/2)
              ix_row = ix_row + 1
           end do
           xi_row  = 1-nSpan + mod(ix_row-1,2*nSpan)
           eta_row = 1-nSpan + (ix_row-1)/(2*nSpan)
           wave_row     =  wavelet(jx-xi_row*(twoP+1)) *  wavelet(jy-eta_row*(twoP+1))
           dwavexi_row  = dwavelet(jx-xi_row*(twoP+1)) *  wavelet(jy-eta_row*(twoP+1))
           dwaveeta_row =  wavelet(jx-xi_row*(twoP+1)) * dwavelet(jy-eta_row*(twoP+1))
           dwavex_row   = ( dwavexi_row*dydeta - dwaveeta_row*dydxi )/dV
           dwavey_row   = (-dwavexi_row*dxdeta + dwaveeta_row*dxdxi )/dV
        end if
        ix_col  = i_rowcol -  (ix_row*(ix_row-1))/2
        xi_col  = 1-nSpan + mod(ix_col-1,2*nSpan)
        eta_col = 1-nSpan + (ix_col-1)/(2*nSpan)

        wave_col     =  wavelet(jx-xi_col*(twoP+1)) *  wavelet(jy-eta_col*(twoP+1))
        dwavexi_col  = dwavelet(jx-xi_col*(twoP+1)) *  wavelet(jy-eta_col*(twoP+1))
        dwaveeta_col =  wavelet(jx-xi_col*(twoP+1)) * dwavelet(jy-eta_col*(twoP+1))
        dwavex_col   = ( dwavexi_col*dydeta - dwaveeta_col*dydxi )/dV
        dwavey_col   = (-dwavexi_col*dxdeta + dwaveeta_col*dxdxi )/dV

        integralsQ(i_rowcol) = integralsQ(i_rowcol) + &
            vx * vy * dV * ( wave_row * wave_col)/4
        if (i_rowcol==120 .and. q==0 .and. verbose) then
           print '(a,i0,1p,10(a,e14.6))','integralsA(',i_rowcol,') = ',integralsA(i_rowcol),' + ', &
            vx * vy * dV * ( dwavex_row * dwavex_col + dwavey_row * dwavey_col ) /4, ' = ', &
            integralsA(i_rowcol) + vx * vy * dV * ( dwavex_row * dwavex_col + dwavey_row * dwavey_col ) /4
           print *,'dwavelet(',jx-xi_col*(twoP+1),')=',dwavelet(jx-xi_col*(twoP+1))
           print *,'dwavexi_row=',dwavexi_row
        end if
        integralsA(i_rowcol) = integralsA(i_rowcol) + &
            vx * vy * dV * ( dwavex_row * dwavex_col + dwavey_row * dwavey_col ) /4
     end do
  end subroutine ProcessGridPoint

  subroutine CalcLaplacian(wavelet,dwavelet,ValQ)
  use Util,     only: error_exit
  use Settings, only: myprec
  use Inputs,   only: m,n, P, nSpan, GridFunctions
  use omp_lib
  implicit none
     real(myprec), allocatable, intent(in) :: wavelet(:), dwavelet(:)
     real(myprec), pointer                 :: ValQ(:,:)

     real(myprec)              :: dxdxi, dxdeta, dydxi, dydeta, dV
     real(myprec), allocatable :: integralsA(:,:), integralsQ(:,:)
     integer, allocatable      :: idxP(:)
     integer                   :: ierr
     real(myprec)              :: xi, eta
     integer                   :: ix, iy, twoP, q, dix
     integer                   :: vx, vy
     real(myprec)              :: x,y
     real(myprec)              :: perc
     integer                   :: jx, jy, nNeigh
     integer                   :: it, nt, iend, istart

     perc   = 5
     twoP   = 2**P
     nNeigh = 2*nSpan * 2*nSpan
     nt     = omp_get_max_threads()

     allocate(LaplaceMatrix(m*n,(4*nSpan-1)*(4*nSpan-1)), &
              ValQ(m*n,(4*nSpan-1)*(4*nSpan-1)), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')
     LaplaceMatrix = 0
     ValQ = 0

     allocate(integralsA((nNeigh * (nNeigh+1))/2,0:P), &
              integralsQ((nNeigh * (nNeigh+1))/2,0:P), stat=ierr)
     if (ierr/=0) call error_exit('Allocation error')


     allocate(idxP(0:twoP-1), stat=ierr)
     if (ierr/=0) call error_exit('Allocation error')

     dix = 1
     do q = P,0,-1
        do ix = 0,twoP-1,dix
           idxP(ix) = q
        end do
        dix = dix * 2
     end do

     do iy = 0,n-1
        do ix = 0,m-1
           if (100*(ix + m*iy) > m*n * perc) then
              write(*,'(i0,a,$)') nint(perc),'% '
              perc = perc + 5
           end if
           ! All integrations in (xi,eta) = (ix:ix+1,iy:iy+1)
           integralsA = 0
           integralsQ = 0

           do jy = 0, twoP
              vy = 2
              if (jy==0 .or. jy==twoP) vy=1
              eta = iy + dble(jy)/twoP
              do jx = 0, twoP
                 vx = 2
                 if (jx==0 .or. jx==twoP) vx = 1
                 q = max(idxP(mod(jx,twoP)),idxP(mod(jy,twoP)))
                 xi = ix + dble(jx)/twoP

                 call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
                 dV = dxdxi * dydeta - dxdeta * dydxi

!$OMP            PARALLEL PRIVATE(it, istart, iend)
                 it = omp_get_thread_num() + 1
                 istart = 1 + ((it-1)*nNeigh * (nNeigh+1))/(2*nt)
                 iend   =     (    it*nNeigh * (nNeigh+1))/(2*nt)
                 call ProcessGridPoint(q,istart,iend, &
                          nSpan, twoP, jx, jy, vx, vy, dV, &
                          dxdxi, dydxi, dxdeta, dydeta, wavelet, dwavelet, &
                          integralsA(:,q), integralsQ(:,q))
!$OMP            END PARALLEL
              end do
           end do

!$OMP      PARALLEL PRIVATE(it, istart, iend)
           it = omp_get_thread_num() + 1
           istart = 1 + ((it-1)*nNeigh * (nNeigh+1))/(2*nt)
           iend   =     (    it*nNeigh * (nNeigh+1))/(2*nt)
           call ProcessIntegrals(istart, iend, ix, iy, nSpan, &
                                 integralsA, integralsQ, ValQ)
!$OMP      END PARALLEL

        end do
     end do
     write(*,*)
  end subroutine CalcLaplacian

end module CalcMatrices_Scalar


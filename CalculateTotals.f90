! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module CalculateTotals
! Calculate total mass, momentum and energy for each of the supported models.
contains
   function CalcTotalsFromMomentum(yin) result(Totals)
   use FDApplyOperators, only: InterpC2V, rhoAtE, rhoAtN, vx, vy
   use GridValues,       only: GridOrientationYatE,  GridOrientationYatN, GridOrientationXatE, GridOrientationXatN
   use GridValues,       only: dVc, dVe, dVn
   use Inputs,           only: m,n
   use Settings,         only: myprec, tnTOTALS, tMASS, tINTERNAL, tMOMENTUM_U, tMOMENTUM_V, tKINETIC, tENERGY
   use StateEq,          only: e_int
   implicit none
      real(myprec), target, intent(in) :: yin(0:)
      real(myprec)                     :: Totals(tnTOTALS)

      real(myprec), pointer :: rho(:), rvx(:), rvy(:)
      integer               :: xi,eta
      integer               :: irow

      rho(0:m*n-1) => yin(    0:  m*n-1)
      rvx(0:m*n-1) => yin(  m*n:2*m*n-1)
      rvy(0:m*n-1) => yin(2*m*n:3*m*n-1)

      call InterpC2V(rho, rho, rhoAtE, rhoAtN, set_zero=.true., exactAtC=.true.)

!$OMP PARALLEL DO
      do irow = 0,m*n-1
         vx(irow)  = rvx(irow) / rhoAtE(irow)
         vy(irow)  = rvy(irow) / rhoAtN(irow)
      end do
!$OMP END PARALLEL DO

      Totals = 0
      do irow = 0,m*n-1
         eta = irow/m
         xi  = mod(irow,m)

         Totals(tMASS)      = Totals(tMASS)      + dVc(irow) * rho(irow)
         Totals(tINTERNAL)  = Totals(tINTERNAL)  + dVc(irow) * e_int(rho(irow))

         Totals(tMOMENTUM_U) = Totals(tMOMENTUM_U) + dVe(irow) * rvx(irow) * GridOrientationXatE(xi,eta)
         Totals(tMOMENTUM_V) = Totals(tMOMENTUM_V) + dVe(irow) * rvx(irow) * GridOrientationYatE(xi,eta)
         Totals(tKINETIC)    = Totals(tKINETIC)    + dVe(irow) * vx(irow)* rvx(irow) / 2

         Totals(tMOMENTUM_U) = Totals(tMOMENTUM_U) - dVn(irow) * rvy(irow) * GridOrientationYatN(xi,eta)
         Totals(tMOMENTUM_V) = Totals(tMOMENTUM_V) + dVn(irow) * rvy(irow) * GridOrientationXatN(xi,eta)
         Totals(tKINETIC)    = Totals(tKINETIC)    + dVn(irow) * vy(irow)* rvy(irow) / 2
      end do

      if (size(Totals) >= tENERGY) Totals(tENERGY) = Totals(tINTERNAL) + Totals(tKINETIC)
   end function CalcTotalsFromMomentum

   function CalcTotalsFromDpDt(yin) result(Totals)
   use CalcMatrices_Scalar, only: LaplaceMatrix
   use GridValues,          only: dVc
   use Settings,            only: myprec, tnTOTALS, tMASS, tINTERNAL, tKINETIC, tENERGY
   use Inputs,              only: GridFunctions, m, n
   implicit none
      real(myprec), target, intent(in) :: yin(:)
      real(myprec)                     :: Totals(tnTOTALS)

      real(myprec)          :: xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta
      real(myprec)          :: Mass, EnerP, EnerK, Ay
      real(myprec), pointer :: dydt(:)
      integer               :: irow, icol, ia, eta_col, xi_col, eta_row, xi_row, nSpan

      nSpan = nint((sqrt(real(size(LaplaceMatrix,2))+1))/4)
      dydt(1:n*m) => yin(n*m+1:2*n*m)

      Mass  = 0.0
      EnerP = 0.0
      EnerK = 0.0

      do irow = 1,m*n
         xi_row  = mod(irow-1,m)
         eta_row = (irow-1)/m

         xi  = (irow-1)/m
         eta = mod(irow-1,m)
         call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)

         Ay = 0
         do ia = 1,(4*nSpan-1) * (4*nSpan-1)
            eta_col  = modulo(eta_row + mod(ia-1,4*nSpan-1)+1-2*nSpan,m)
            xi_col   = modulo(xi_row + (ia-1)/(4*nSpan-1)+1-2*nSpan,n)
            icol     = xi_col + m * eta_col + 1
            Ay       = Ay + LaplaceMatrix(irow,ia) * yin(icol)
         end do

         Mass  = Mass  +       dVc(irow) * yin(irow)
         EnerP = EnerP + 0.5 * dVc(irow) * dydt(irow) * dydt(irow)
         EnerK = EnerK - 0.5 * dVc(irow) * yin(irow) * Ay
      end do

      Totals = 0
      Totals(tMASS)       = Mass
      Totals(tINTERNAL)   = EnerP
      Totals(tKINETIC)    = EnerK
      if (size(Totals) >= tENERGY) Totals(tENERGY) = Totals(tINTERNAL) + Totals(tKINETIC)

   end function CalcTotalsFromDpDt

   function CalcTotalsFromVel(yin) result(Totals)
   use GridValues, only: dVc, dVe, dVn
   use GridValues, only: GridOrientationYatE,  GridOrientationYatN, GridOrientationXatE, GridOrientationXatN
   use Settings,   only: myprec, tnTOTALS, tMASS, tINTERNAL, tKINETIC, tENERGY, tMOMENTUM_U, tMOMENTUM_V
   use StateEq,    only: e_int
   use Inputs,     only: m, n
   implicit none
      real(myprec), target, intent(in) :: yin(0:)
      real(myprec)                     :: Totals(tnTOTALS)

      real(myprec), pointer :: p(:), vx(:), vy(:)
      integer               :: xi,eta
      integer               :: irow

      p( 0:m*n-1)  => yin(    0:  m*n-1)
      vx(0:m*n-1)  => yin(  m*n:2*m*n-1)
      vy(0:m*n-1)  => yin(2*m*n:3*m*n-1)

      Totals = 0
      do irow = 0,m*n-1
         eta = irow/m
         xi  = mod(irow,m)

         Totals(tMASS)       = Totals(tMASS)       + dVc(irow) * p(irow)
         Totals(tINTERNAL)   = Totals(tINTERNAL)   + dVc(irow) * e_int(p(irow))

         Totals(tMOMENTUM_U) = Totals(tMOMENTUM_U) + dVe(irow) * vx(irow) * GridOrientationXatE(xi,eta)
         Totals(tMOMENTUM_V) = Totals(tMOMENTUM_V) + dVe(irow) * vx(irow) * GridOrientationYatE(xi,eta)
         Totals(tKINETIC)    = Totals(tKINETIC)    + dVe(irow) * vx(irow)**2

         Totals(tMOMENTUM_U) = Totals(tMOMENTUM_U) - dVn(irow) * vy(irow) * GridOrientationYatN(xi,eta)
         Totals(tMOMENTUM_V) = Totals(tMOMENTUM_V) + dVn(irow) * vy(irow) * GridOrientationXatN(xi,eta)
         Totals(tKINETIC)    = Totals(tKINETIC)    + dVn(irow) * vy(irow)**2
      end do

      if (size(Totals) >= tENERGY) Totals(tENERGY) = Totals(tINTERNAL) + Totals(tKINETIC)

   end function CalcTotalsFromVel

end module CalculateTotals


! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module Checks
! Supportive functions for unit testing
contains
   subroutine ScalarProduct(f, g, dV, Area_, normF_, normG_, ScalProd_)
   use Settings, only: myprec
   use Inputs,   only: m,n
   implicit none

     real(myprec), intent(in)            :: f(0:), g(0:), dV(0:)
     real(myprec), intent(out), optional :: Area_, normF_, normG_, ScalProd_

     real(myprec) :: Area, normF, normG, ScalProd
     integer      :: irow

     Area     = 0
     normF    = 0
     normG    = 0
     ScalProd = 0
     do irow = 0,n*m-1
        Area     = Area     + dV(irow)
        normF    = normF    + dV(irow) * f(irow)**2
        normG    = normG    + dV(irow) * g(irow)**2
        ScalProd = ScalProd + dV(irow) * f(irow) * g(irow)
     end do
     normF = sqrt(normF)
     normG = sqrt(normG)

     if (present(Area_))     Area_     = Area
     if (present(normF_))    normF_    = normF
     if (present(normG_))    normG_    = normG
     if (present(ScalProd_)) ScalProd_ = ScalProd

   end subroutine ScalarProduct

   subroutine ScalarProductVectorFieldsAtC(fx,fy,gx,gy, Area_, normF_, normG_,ScalProd_)
   use Settings, only: myprec
   use GridValues, only: dVc
   implicit none
     real(myprec), pointer, intent(in)   :: fx(:), fy(:), gx(:), gy(:)
     real(myprec), intent(out), optional :: Area_, normF_, normG_, ScalProd_

     real(myprec) :: Area, normFx, normGx, FxGx
     real(myprec) :: normFy, normGy, FyGy

     call ScalarProduct(fx, gx, dVc, Area, normFx, normGx, FxGx)
     call ScalarProduct(fy, gy, dVc, Area, normFy, normGy, FyGy)

     if (present(Area_))     Area_     = Area
     if (present(normF_))    normF_    = sqrt(normFx**2 + normFy**2)
     if (present(normG_))    normG_    = sqrt(normGx**2 + normGy**2)
     if (present(ScalProd_)) ScalProd_ = FxGx + FyGy
   end subroutine ScalarProductVectorFieldsAtC

   subroutine ScalarProductVectorFields(fx,fy,gx,gy, normF_, normG_,ScalProd_)
   use Settings,   only: myprec
   use GridValues, only: dVe, dVn
   implicit none

     real(myprec), intent(in)            :: fx(0:), fy(0:), gx(0:), gy(0:)
     real(myprec), intent(out), optional :: normF_, normG_, ScalProd_

     real(myprec) :: AreaX, normFx, normGx, FxGx
     real(myprec) :: AreaY, normFy, normGy, FyGy

     call ScalarProduct(fx, gx, dVe, AreaX, normFx, normGx, FxGx)
     call ScalarProduct(fy, gy, dVn, AreaY, normFy, normGy, FyGy)

     if (present(normF_))    normF_    = sqrt(normFx**2 + normFy**2)
     if (present(normG_))    normG_    = sqrt(normGx**2 + normGy**2)
     if (present(ScalProd_)) ScalProd_ = FxGx + FyGy

   end subroutine ScalarProductVectorFields

   subroutine CheckMomentumFree(Fx,Fy,tol,nameF)
   use Settings,      only: myprec
   use Inputs,        only: m,n
   use Util,          only: error_exit
   use TestFunctions, only: TestValues
   implicit none
     real(myprec), intent(in)     :: Fx(0:), Fy(0:)
     real(myprec), intent(in)     :: tol
     character(len=*), intent(in) :: nameF

     real(myprec), pointer :: vx10(:), vy10(:), vx01(:), vy01(:)
     real(myprec)          :: normF, norm10, norm01, f10, f01, relerr01, relerr10
     integer               :: ierr

     allocate( vx10(0:n*m), vy10(0:n*m), vx01(0:n*m), vy01(0:n*m), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')

     call TestValues(vx10=vx10, vy10=vy10, vx01=vx01, vy01=vy01)
     call ScalarProductVectorFields(Fx,Fy, vx10,vy10, normF_=normF, normG_=norm10, ScalProd_=f10)
     call ScalarProductVectorFields(Fx,Fy, vx01,vy01, normF_=normF, normG_=norm01, ScalProd_=f01)
     relerr10 = abs(f10)/sqrt(normF*norm10)
     relerr01 = abs(f01)/sqrt(normF*norm01)

     print '(a,a20,a,1p,2(e10.2,a))','    relative momentum in ',nameF,'= (',relerr10,',',relerr01,')'
     if (.not. relerr01+relerr10<=tol) call error_exit('Error too large', fatal=.false.)

     deallocate( vx10, vy10, vx01, vy01, stat=ierr)
     if (ierr/=0) call error_exit('allocation error')

   end subroutine CheckMomentumFree

   subroutine CheckSame(F,G,tol,nameF,nameG)
   use Settings,      only: myprec
   use Inputs,        only: m,n
   use Util,          only: error_exit
   use TestFunctions, only: TestValues
   implicit none
     real(myprec), intent(in)     :: F(0:), G(0:)
     real(myprec), intent(in)     :: tol
     character(len=*), intent(in) :: nameF, nameG

     real(myprec) :: normF, normG, Error, relerr
     integer      :: irow

     normF=0
     normG=0
     Error=0
     do irow = 0,n*m-1
        normF = normF + f(irow)**2
        normG = normG + g(irow)**2
        Error = Error + (f(irow)-g(irow))**2
     end do
     Error = sqrt(Error/(n*m))
     normF = sqrt(normF/(n*m))
     normG = sqrt(normG/(n*m))

     relerr = Error/max(1e-10,sqrt(normF*normG))
     print '(a,a,1p,e10.2)','    relErr('//nameF//'='//nameG//')','=',relerr
     if (.not. relerr<=tol) call error_exit('Error too large', fatal=.false.)
   end subroutine CheckSame

   subroutine CheckZero(F,tol,nameF)
   use Settings,      only: myprec
   use Inputs,        only: m,n
   use Util,          only: error_exit
   use TestFunctions, only: TestValues
   implicit none
     real(myprec),     intent(in) :: F(0:)
     real(myprec),     intent(in) :: tol
     character(len=*), intent(in) :: nameF

     real(myprec) :: Error
     integer      :: irow

     Error=0
     do irow = 0,n*m-1
        Error = Error + f(irow)**2
     end do
     Error = sqrt(Error/(n*m))

     print '(a,a,1p,e10.2)','    relErr('//nameF//'=Zero)','=',Error
     if (.not. Error<=tol) call error_exit('Error too large', fatal=.false.)
   end subroutine CheckZero

   subroutine CheckAdjointness(f, g, Af, ATg, dVf, dVg, tol, nameA, nameAt)
   use Settings, only: myprec
   use Util,     only: error_exit
   implicit none
     real(myprec),     intent(in) :: f(0:), g(0:), Af(0:), ATg(0:), dVf(0:), dVg(0:)
     real(myprec),     intent(in) :: tol
     character(len=*), intent(in) :: nameA, nameAt

     real(myprec) :: normF, normG, normAF, normAtG, F_AtG, G_AF, relerr

     call ScalarProduct(f,AtG, dVf, normF_=normF, normG_=normAtG, ScalProd_=F_AtG)
     call ScalarProduct(g,Af,  dVg, normF_=normG, normG_=normAF,  ScalProd_=G_AF)

     relerr = 2*abs(G_AF - F_AtG) / (sqrt(normF*normAtG)+sqrt(normG*normAF))
     print '(a,a,1p,e10.2)','    relErr('//nameA//'='//nameAt//''')','=',relerr
     if (.not. relerr<=tol) call error_exit('Error too large', fatal=.false.)
   end subroutine CheckAdjointness

   subroutine CheckAdjointnessOneVectorField(f, gx,gy, Axf,Ayf, ATg, tol, nameA, nameAt)
   use GridValues, only: dVc
   use Settings,   only: myprec
   use Util,       only: error_exit
   implicit none
     real(myprec),     intent(in) :: f(0:), gx(0:), gy(0:), Axf(0:), Ayf(0:), ATg(0:)
     real(myprec),     intent(in) :: tol
     character(len=*), intent(in) :: nameA, nameAt

     real(myprec) :: normF, normG, normAF, normAtG, F_AtG, G_AF, relerr

     call ScalarProduct(f, ATg, dVc,  normF_=normF, normG_=normAtG, ScalProd_=F_AtG)
     call ScalarProductVectorFields(gx,gy, Axf,Ayf,   normF_=normG, normG_=normAF,  ScalProd_=G_AF)

     relerr = 2*abs(G_AF - F_AtG) / (sqrt(normF*normAtG)+sqrt(normG*normAF))
     print '(a,a,1p,e10.2)','    relErr('//nameA//'='//nameAt//''')','=',relerr
     if (.not. relerr<=tol) call error_exit('Error too large', fatal=.false.)
   end subroutine CheckAdjointnessOneVectorField

   subroutine CheckAdjointnessVectorFields(fx,fy, gx,gy, Axf,Ayf, ATxg,ATyg, tol, nameA, nameAt)
   use Settings, only: myprec
   use Util,     only: error_exit
   implicit none
     real(myprec),     intent(in) :: fx(0:), fy(0:), gx(0:), gy(0:), Axf(0:), Ayf(0:), ATxg(0:), ATyg(0:)
     real(myprec),     intent(in) :: tol
     character(len=*), intent(in) :: nameA, nameAt

     real(myprec) :: normF, normG, normAF, normAtG, F_AtG, G_AF, relerr

     call ScalarProductVectorFields(fx,fy, ATxg,ATyg, normF_=normF, normG_=normAtG, ScalProd_=F_AtG)
     call ScalarProductVectorFields(gx,gy, Axf,Ayf,   normF_=normG, normG_=normAF,  ScalProd_=G_AF)

     relerr = 2*abs(G_AF - F_AtG) / (sqrt(normF*normAtG)+sqrt(normG*normAF))
     print '(a,a,1p,e10.2)','    relErr('//nameA//'='//nameAt//''')','=',relerr
     if (.not.relerr<=tol) call error_exit('Error too large', fatal=.false.)
   end subroutine CheckAdjointnessVectorFields
end module Checks

! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
Module Stencils
use Settings, only: myprec
! Calculate stencils for discrete derivatives and (de)staggering interpolations
! The pre-calculated stencils are no longer used and can probably go.
implicit none

   real(myprec), pointer :: deriv(:)
   real(myprec), pointer :: destagger(:)

contains

   subroutine SetTaylorDestagger(nStc)
   use Util, only: error_exit
   implicit none
      integer, intent(in) :: nStc

      integer                 :: ierr
      integer                 :: ix, jx
      real(myprec)            :: xi, xj
      real(myprec), parameter :: x0=0

      allocate(destagger(-nStc:nStc-1),stat=ierr)
      if (ierr/=0) call error_exit('allocation error')
      destagger(:) = 1
      do ix = -nStc,nStc-1
         xi = ix + 0.5d0
         do jx = -nStc,nStc-1
            if (jx/=ix) then
               xj = jx + 0.5d0
               destagger(ix) = destagger(ix) * (x0-xj)/(xi-xj)
            end if
         end do
      end do
   end subroutine SetTaylorDestagger

   subroutine SetTaylorDeriv(nStc)
   use Util, only: error_exit
   implicit none
      integer, intent(in) :: nStc

      integer                 :: ierr
      integer                 :: ix, jx
      real(myprec)            :: xi, xj
      real(myprec), parameter :: x0=0
      real(myprec)            :: f

      allocate(deriv(-nStc:nStc-1),stat=ierr)
      if (ierr/=0) call error_exit('allocation error')
      deriv(:) = 0

      do ix = -nStc,nStc-1
         xi = ix + 0.5d0
         f  = 1
         do jx = -nStc,nStc-1
            if (jx/=ix) then
               xj = jx + 0.5d0
               f = f * (x0-xj)/(xi-xj)
            end if
         end do
         do jx = -nStc,nStc-1
            if (jx/=ix) then
               xj = jx + 0.5d0
               deriv(ix) = deriv(ix) + f/(x0-xj)
            end if
         end do
      end do
   end subroutine SetTaylorDeriv

   subroutine SetDestagger(u)
   implicit none
      real(myprec), intent(in), target, contiguous :: u(:)
      destagger(-size(u,1)/2:size(u,1)/2-1) => u
   end subroutine SetDestagger

   subroutine SetDeriv(u)
   implicit none
      real(myprec), intent(in), target, contiguous :: u(:)
      deriv(-size(u,1)/2:size(u,1)/2-1) => u
   end subroutine SetDeriv

end Module Stencils

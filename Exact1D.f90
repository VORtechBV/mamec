! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module Exact1D
use Settings, only: myprec
  !
  ! Exact 1D solution: Riemann invariants
  !--------------------------------------
  ! This module provides an exact 1D solution for
  ! a hyperbolic differential equation with Riemann invariants:
  !
  !  d/dt Fplus(v1D,rho1D) + Vplus(v1D,rho1D) * d/dx Fplus(v1D,rho1D) = 0
  !  d/dt Fmin(v1D,rho1D)  + Vmin(v1D,rho1D)  * d/dx Fmin(v1D,rho1D)  = 0
  !
  ! The exact solution provided is a 1D wave.
  ! Typically, such solutions will steepen up. At time=MaxTime,
  ! the solution becomes multi-valued/discontinuous, and the function will
  ! no longer return an answer. Instead, it will terminate the program.
  !
  ! From the module StateEq, the following functions are needed
  !     Vel1D(x1), Dens1D(x1)   -     initial solutions
  !                                   for which Fmin(Vel1D,Dens1D) is constant.
  !     PropagationSpeed1D(x1)  -     Propagation speed Vplus(Vel1D,Dens1D)
  !
  ! The solution is given by
  !      v1D(x0 + PropagationSpeed1D(x0)*t,t) = vel1D(x0)      (A)
  !    rho1D(x0 + PropagationSpeed1D(x0)*t,t) = Dens1D(x0)
  !
  ! 2D solution
  !--------------------------------
  ! The 1D solution is used to create a 2D solution, given by
  !     v(x,y,t) = v1D((x-y)/sqrt(2),t),   rho(x,y,t) = rho1D((x-y)/sqrt(2),t)
  !
  ! Time derivative of the solution
  !--------------------------------
  ! The solution is constant along a characteristic line:
  !    D/Dt v1D(x0 + PropagationSpeed1D(x0)*t,t) = d/dt vel1D(x0) = 0
  !
  ! The derivative along the characteristic line is expressed in partial derivatives:
  !    d/dt v1D +  PropagationSpeed1D(x0) * d/dx v1D = 0
  !    d/dt v1D = -PropagationSpeed1D(x0) * d/dx v1D
  !
  ! We return to (A) and take the x-derivative:
  !    d/dx0  v1D(x0 + PropagationSpeed1D(x0)*t,t) = d/dx0 vel1D(x0)
  !    (1 + PropagationSpeed'(x0)*t) d/dx v1D = vel1D'(x0)
  !    d/dx v1D = vel1D'(x0)/(1 + PropagationSpeed'(x0)*t)
  !
  ! and so we find the time derivative
  !    d/dt v1D   = -PropagationSpeed1D(x0)/(1+PropagationSpeed'(x0)*t) * vel1D'(x0)
  !    d/dt rho1D = -PropagationSpeed1D(x0)/(1+PropagationSpeed'(x0)*t) * rho1D'(x0)
  !
  private
  public:: &
     MaxTime,          &       ! Max-time at which the exact solution is valid.
     ExactSolution,    &
     exactdSolutiondt, &
     InitialValues,    &
     CompareDontSave,  &
     CompareAndSave,   &
     CompareValues,    &
     CompareLocal,     &
     WriteExactSolution

  real(myprec), parameter :: sqrt2 = sqrt(real(2,myprec))
  integer, parameter      :: n1D = 10000
  real(myprec)            :: v1(-n1D:2*n1D), rho1(-n1D:2*n1D), Vplus(-n1D:2*n1D)
  real(myprec)            :: dv1dx(-n1D:2*n1D), drho1dx(-n1D:2*n1D), dVplusDx(-n1D:2*n1D), ddvdxx(-n1D:2*n1D)
  real(myprec)            :: x1(-n1D:2*n1D)
  real(myprec), save      :: tnow

  interface
     function int2real_itf(i,t) result(y)
     use Settings, only: myprec
     implicit none
        integer,      intent(in) :: i
        real(myprec), intent(in) :: t

        real(myprec):: y
     end function int2real_itf
  end interface

contains

   subroutine initExactSolution()
   use LinearWave, only: ddVel1Ddxx
   use StateEq,    only: Dens1D, Vel1D, PropagationSpeed1D, dPropagationSpeed1Ddx, dVel1Ddx, dDens1Ddx
   use Inputs,     only: eqident, eqSCALAR
   implicit none
       logical, save :: initialized = .false.
       integer       :: i

       if (initialized) return

       do i = -n1D, 2*n1D
          x1(i)      = sqrt(2d0) * i/dble(n1D)
          v1(i)      = Vel1D(x1(i))
          dv1dx(i)   = dVel1Ddx(x1(i))
          rho1(i)    = Dens1D(x1(i))
          drho1dx(i) = dDens1Ddx(x1(i))
          Vplus(i)   = PropagationSpeed1D(x1(i))
          dVplusDx(i)= dPropagationSpeed1Ddx(x1(i))
       end do
       if (eqident==eqSCALAR) then
          do i = -n1D, 2*n1D
             ddvdxx(i)  = ddVel1Ddxx(x1(i))
          end do
       end if
       tnow = 0.0
       initialized = .true.
   end subroutine initExactSolution

   subroutine exactSolution1D(time)
   use Util, only: error_exit
   implicit none
       real(myprec), intent(in)  :: time

       integer :: i

       call initExactSolution()
       if (abs(time-tnow) < 1e-14) return
       call initExactSolution()

       do i = -n1D, 2*n1D
          x1(i)  = sqrt(2d0) * i/dble(n1D) + time * Vplus(i)
       end do

       do i = 1-n1D, 2*n1D
          if (x1(i) < x1(i-1)) then
             print *,'time: ',time
             call error_exit('inverse x-axis')
          end if
       end do
   end subroutine exactSolution1D

   function MaxTime() result(tmax)
   use Inputs, only: eqident, eqSCALAR, eqLINEARFD, eqLINEARGALERKIN
   implicit none
       real(myprec) :: tmax
       integer      :: i

       call initExactSolution()
       if (eqident==eqLINEARGALERKIN .or. eqident==eqLINEARFD .or. eqident==eqSCALAR) then
          tmax = 10d0
       else
          tmax = 1e10
          do i = 0,n1D
             if (Vplus(i-1) > Vplus(i)) &
                tmax = min(tmax, sqrt(2d0)/dble(n1D) / (Vplus(i-1)-Vplus(i)) )
          end do
          tmax = tmax * 0.99
       end if
   end function MaxTime

   function vel_fun(i,t) result(res)
   implicit none
       integer,      intent(in) :: i
       real(myprec), intent(in) :: t
       real(myprec) :: res
       res = v1(i) + 0*t
   end function vel_fun

   function dens_fun(i,t) result(res)
   implicit none
       integer,      intent(in) :: i
       real(myprec), intent(in) :: t
       real(myprec) :: res
       res = rho1(i) + 0*t
   end function dens_fun

   function mom_fun(i,t) result(res)
   implicit none
       integer,      intent(in) :: i
       real(myprec), intent(in) :: t
       real(myprec) :: res
       res = rho1(i) * v1(i) + 0*t
   end function mom_fun

   function ddvdxx_fun(i,t) result(res)
   implicit none
       integer,      intent(in) :: i
       real(myprec), intent(in) :: t
       real(myprec) :: res
       res = ddvdxx(i) + 0*t
   end function ddvdxx_fun

   function dvdt_fun(i,t) result(res)
   implicit none
       integer,      intent(in) :: i
       real(myprec), intent(in) :: t
       real(myprec) :: res
       res = -Vplus(i)/(1+dVplusDx(i)*t) * dv1dx(i)
   end function dvdt_fun

   function drhodt_fun(i,t) result(res)
   implicit none
       integer,      intent(in) :: i
       real(myprec), intent(in) :: t
       real(myprec) :: res
       res = -Vplus(i)/(1+dVplusDx(i)*t) * drho1dx(i)
   end function drhodt_fun

   function dmomdt_fun(i,t) result(res)
   implicit none
       integer,      intent(in) :: i
       real(myprec), intent(in) :: t
       real(myprec) :: res
       res =  vel_fun(i,t) * drhodt_fun(i,t) + &
             dvdt_fun(i,t) * dens_fun(i,t)
   end function dmomdt_fun

   function ExactSomething(x,y,t,something) result(res)
   implicit none
      real(myprec), intent(in)  :: x, y
      real(myprec), intent(in)  :: t
      procedure (int2real_itf)  :: something

      real(myprec)       :: res
      real(myprec)       :: xy, w, s
      integer, save      :: i=0
      integer            :: di, j, k
      integer, parameter :: jfirst = -2, jlast = 2

!$OMP threadprivate(i)
      call exactSolution1D(t)

      xy = (x - y)/sqrt(2d0)
      do while(xy < x1(0))
         xy = xy + sqrt2
      end do
      do while(xy > x1(n1D))
         xy = xy - sqrt2
      end do

      di = 1024
      do while (di>1)
         di = di/2
         do while (x1(i) > xy .and. i-di>lbound(x1,1))
            i = i - di
         end do
         do while (x1(i+di)<xy .and. i+di<ubound(x1,1))
            i = i + di
         end do
      end do

      if (x1(i) <= xy .and. xy <= x1(i+1)) then
         continue
      else
         print *,'Not correct: ',x1(i),'<=',xy,'<=',x1(i+1)
      end if

      ! Interpolate (x1(i+jfirst:last),something(i+jfirst:jlast,t)) to (xy,res)
      res  = 0
      do j = jfirst, jlast
         w = 1
         s = 1
         do k = jfirst, jlast
            if (k/=j) then
               w = w * (x1(i+k)-xy)
               s = s * (x1(i+k)-x1(i+j))
            end if
         end do
         res = res + w*something(i+j,t)/s
      end do

   end function ExactSomething

   function ExactMomentum(x,y,t) result(rv)
   implicit none
      real(myprec), intent(in)  :: x, y
      real(myprec), intent(in)  :: t
      real(myprec) :: rv
      rv = ExactSomething(x,y,t,mom_fun)
   end function ExactMomentum

   function ExactdMomentumdt(x,y,t) result(rv)
   implicit none
      real(myprec), intent(in)  :: x, y
      real(myprec), intent(in)  :: t
      real(myprec) :: rv
      rv = ExactSomething(x,y,t,dmomdt_fun)
   end function ExactdMomentumdt

   function ExactddDensitydxx(x,y,t) result(res)
   implicit none
      real(myprec), intent(in)  :: x, y
      real(myprec), intent(in)  :: t
      real(myprec) :: res
      res = ExactSomething(x,y,t,ddvdxx_fun)
   end function ExactddDensitydxx

   function ExactdVelocitydt(x,y,t) result(dvdt)
   implicit none
      real(myprec), intent(in)  :: x, y
      real(myprec), intent(in)  :: t
      real(myprec) :: dvdt
      dvdt = ExactSomething(x,y,t,dvdt_fun)
   end function ExactdVelocitydt

   function ExactVelocity(x,y,t) result(v)
   implicit none
      real(myprec), intent(in)  :: x, y
      real(myprec), intent(in)  :: t
      real(myprec) :: v
      v = ExactSomething(x,y,t,vel_fun)
   end function ExactVelocity

   function ExactDensity(x,y,t) result(rho)
   implicit none
      real(myprec), intent(in)  :: x, y
      real(myprec), intent(in)  :: t
      real(myprec) :: rho
      rho = ExactSomething(x,y,t,dens_fun)
   end function ExactDensity

   function ExactdDensitydt(x,y,t) result(drhodt)
   implicit none
      real(myprec), intent(in)  :: x, y
      real(myprec), intent(in)  :: t
      real(myprec) :: drhodt
      drhodt = ExactSomething(x,y,t,drhodt_fun)
   end function ExactdDensitydt

   subroutine InitialValues(y0)
   use Inputs, only: eqident, eqSCALAR
   implicit none
      real(myprec), pointer   :: y0(:)
      real(myprec), parameter :: t=0
      call ExactSolution(t,y0)
      if (eqident==eqSCALAR) call CorrectInitialValues(y0)
   end subroutine InitialValues

   subroutine CorrectInitialValues(y0)
   !
   ! The scalar wave equation has the mass conservation property that
   !    M'(t) = M'(0)
   ! with mass M given by
   !    M  =  int_V p dV.
   ! If exact initial solutions have M'(0) = 0, then discrete initial solutions will have M'(0) \approx 0.
   ! This function modifies the given initial so M'(0) = 0, and mass stays constant throughout the simulation.
   !
   use GridValues, only: dVc
   use Inputs,     only: m,n
   implicit none
      real(myprec), target, intent(inout) :: y0(1:)

      real(myprec), pointer :: df(:)
      real(myprec)          :: sumdf
      integer               :: irow

      df => y0(m*n+1:2*m*n)
      sumdf = 0
      do irow = 1,m*n
         sumdf = sumdf + dVc(irow)*df(irow)
      end do
      df = df - sumdf
   end subroutine CorrectInitialValues

   subroutine ExactSolution(time, yout)
   use Inputs,    only: m,n, eqident, eqSCALAR, eqSHALLOW, GridFunctions
   use Util,      only: error_exit
   use GridTrafo, only: GridOrientation
   implicit none
       real(myprec), intent(in)  :: time
       real(myprec), pointer     :: yout(:)

       integer       :: irow, ierr
       real(myprec)  :: x, y
       real(myprec)  :: Ax, Ay
       real(myprec)  :: dxdxi, dydxi, dxdeta, dydeta
       real(myprec)  :: xi, eta

       call exactSolution1D(time)
       if (.not. associated(yout)) then
          if (eqident==eqSCALAR) then
             allocate(yout(2*m*n),stat=ierr)
          else
             allocate(yout(0:3*m*n-1),stat=ierr)
          end if
          if (ierr/=0) call error_exit('allocation error')
       end if
       if (eqident==eqSCALAR) then
!$OMP      PARALLEL DO PRIVATE(xi,eta,x,y)
           do irow = 1,m*n
              xi  = real((irow-1)/m,myprec)
              eta = real(mod(irow-1,m),myprec)
              call GridFunctions(xi,eta, x,y)
              yout(irow    ) = ExactDensity(x,y,time)
              yout(irow+n*m) = ExactdDensitydt(x,y,time)
           end do
!$OMP      END PARALLEL DO
       else if (eqident==eqSHALLOW) then
!$OMP      PARALLEL DO PRIVATE(xi,eta,x,y,dxdxi,dydxi,dxdeta,dydeta,Ax,Ay)
           do irow = 0,m*n-1
              eta = real(irow/m,myprec)
              xi  = real(mod(irow,m),myprec)
              call GridFunctions(xi,eta, x,y)
              yout(irow) = ExactDensity(x,y,time)

              call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
              call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
              yout(irow+  n*m) =  (Ax-Ay)*ExactMomentum(x,y,time)/sqrt2

              call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
              call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
              yout(irow+2*n*m) = -(Ay+Ax)*ExactMomentum(x,y,time)/sqrt2
           end do
!$OMP      END PARALLEL DO
       else
!$OMP      PARALLEL DO PRIVATE(xi,eta,x,y,dxdxi,dydxi,dxdeta,dydeta,Ax,Ay)
           do irow = 0,m*n-1
              eta = real(irow/m,myprec)
              xi  = real(mod(irow,m),myprec)
              call GridFunctions(xi,eta, x,y)
              yout(irow) = ExactDensity(x,y,time)

              eta = real(irow/m,myprec)
              xi  = real(mod(irow,m),myprec)
              call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
              call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
              yout(irow+  n*m) =  (Ax-Ay)*ExactVelocity(x,y,time)/sqrt2

              call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
              call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
              yout(irow+2*n*m) = -(Ay+Ax)*ExactVelocity(x,y,time)/sqrt2
           end do
!$OMP      END PARALLEL DO
       end if
   end subroutine ExactSolution

   subroutine exactdSolutiondt(time, yout)
   use Inputs,    only: m,n, eqident, eqSCALAR, eqSHALLOW, GridFunctions
   use Util,      only: error_exit
   use GridTrafo, only: GridOrientation
   implicit none
       real(myprec), intent(in)  :: time
       real(myprec), pointer     :: yout(:)

       integer                 :: irow, ierr
       real(myprec)            :: x, y
       real(myprec)            :: Ax, Ay
       real(myprec)            :: dxdxi, dydxi, dxdeta, dydeta
       real(myprec)            :: xi, eta
       real(myprec), parameter :: sqrt2 = sqrt(real(2,myprec))

       call exactSolution1D(time)

       if (.not. associated(yout)) then
          if (eqident==eqSCALAR) then
             allocate(yout(2*m*n),stat=ierr)
          else
             allocate(yout(0:3*m*n-1),stat=ierr)
          end if
          if (ierr/=0) call error_exit('allocation error')
       end if

       if (eqident==eqSCALAR) then
!$OMP      PARALLEL DO PRIVATE(xi,eta,x,y)
           do irow = 1,m*n
              xi  = real((irow-1)/m,myprec)
              eta = real(mod(irow-1,m),myprec)
              call GridFunctions(xi,eta, x,y)
              yout(irow    ) = ExactdDensitydt(x,y,time)
              yout(irow+n*m) = ExactddDensitydxx(x,y,time)
           end do
!$OMP      END PARALLEL DO
       else if (eqident==eqSHALLOW) then
!$OMP      PARALLEL DO PRIVATE(xi,eta,x,y,dxdxi,dydxi,dxdeta,dydeta,Ax,Ay)
           do irow = 0,m*n-1
              eta = real(irow/m,myprec)
              xi  = real(mod(irow,m),myprec)
              call GridFunctions(xi,eta, x,y)
              yout(irow) = ExactdDensitydt(x,y,time)

              call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
              call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
              yout(irow+  n*m) =  (Ax-Ay)*ExactdMomentumdt(x,y,time)/sqrt2

              call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
              call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
              yout(irow+2*n*m) = -(Ay+Ax)*ExactdMomentumdt(x,y,time)/sqrt2
           end do
!$OMP      END PARALLEL DO
       else
!$OMP      PARALLEL DO PRIVATE(xi,eta,x,y,dxdxi,dydxi,dxdeta,dydeta,Ax,Ay)
           do irow = 0,m*n-1
              eta = real(irow/m,myprec)
              xi  = real(mod(irow,m),myprec)
              call GridFunctions(xi,eta, x,y)
              yout(irow) = ExactdDensitydt(x,y,time)

              call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
              call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
              yout(irow+  n*m) =  (Ax-Ay)*ExactdVelocitydt(x,y,time)/sqrt2

              call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
              call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
              yout(irow+2*n*m) = -(Ay+Ax)*ExactdVelocitydt(x,y,time)/sqrt2
           end do
!$OMP      END PARALLEL DO
       end if
   end subroutine exactdSolutiondt

   subroutine WriteExactSolution(t)
   use GridTrafo, only: GridOrientation
   use Inputs,    only: m,n, eqident, eqSCALAR
   use Util,      only: error_exit
   use OutFile,   only: WriteSolution
   implicit none
      real(myprec), intent(in)  :: t

      real(myprec), pointer :: yin(:)
      integer               :: ierr

      if (eqident==eqSCALAR) then
         allocate(yin(1:2*m*n),stat=ierr)
      else
         allocate(yin(0:3*m*n-1),stat=ierr)
      end if
      if (ierr/=0) call error_exit('allocation error')
      call ExactSolution(t,yin)
      call WriteSolution(yin, t, 'exact')

      deallocate(yin,stat=ierr)
      if (ierr/=0) call error_exit('deallocation error')

   end subroutine WriteExactSolution

   function CompareValues(yin,t) result(err)
   use Inputs, only: GridFunctions, m, n
   use Util,   only: error_exit
   implicit none
      real(myprec), intent(in) :: yin(0:)
      real(myprec), intent(in) :: t
      real(myprec)             :: err(3)

      real(myprec), pointer :: yout(:)
      real(myprec)          :: sumdf, sumf
      integer               :: irow, jeq, ierr

      yout => null()
      call ExactSolution(t, yout)

      err(:) = 0
      do jeq = 1,size(yout,1)/(m*n)

         sumf  = 0
         sumdf = 0

         do irow = 0,m*n-1
            sumf  = sumf  +  yout(lbound(yout,1)+irow+(jeq-1)*n*m)**2
            sumdf = sumdf + (yout(lbound(yout,1)+irow+(jeq-1)*n*m)-yin(irow+(jeq-1)*n*m))**2
         end do

         err(jeq) = sqrt(sumdf/sumf)
      end do

      deallocate(yout,stat=ierr)
      if (ierr/=0) call error_exit('deallocation error')

   end function CompareValues

   function CompareLocal(f,dydt) result(err)
   use Inputs, only: GridFunctions, m, n
   use Util,   only: error_exit
   implicit none
      real(myprec), intent(in) :: f(0:), dydt(0:)
      real(myprec)             :: err(3,2)

      real(myprec) :: sumdf, sumf
      real(myprec) :: maxdf
      integer      :: irow, jeq, neq

      err(:,:) = 0
      neq = size(f,1)/(m*n)
      do jeq = 1,neq
         sumf  = 0
         sumdf = 0
         maxdf = 0

         do irow = 0,m*n-1
            sumf  = sumf  +  dydt(irow+(jeq-1)*n*m)**2
            sumdf = sumdf + (dydt(irow+(jeq-1)*n*m)-f(irow+(jeq-1)*n*m))**2
            maxdf = max(maxdf, abs(dydt(irow+(jeq-1)*n*m)-f(irow+(jeq-1)*n*m)))
         end do

         err(jeq,1) = sqrt(sumdf/sumf)
         err(jeq,2) = maxdf
      end do
   end function CompareLocal

   subroutine CompareDontSave(y,t)
   use Settings, only: myprec
   implicit none
      real(myprec), target,       intent(in) :: y(0:)
      real(myprec),               intent(in) :: t
      real(myprec) :: compare(3)

      compare = CompareValues(y,t)
      print '(a,f5.2,10(a,f12.8))', &
                 'Solution ',  t,': ' // &
                 'p-error = ', compare(1)*100,' %,  ' // &
                 'vx-error = ',compare(2)*100,' %,  ' // &
                 'vy-error = ',compare(3)*100,' %'
   end subroutine CompareDontSave

   subroutine CompareAndSave(y,t)
   use OutFile,  only: WriteSolution
   use Settings, only: myprec
   implicit none
      real(myprec), target,       intent(in) :: y(0:)
      real(myprec),               intent(in) :: t
      call WriteSolution(y,t)
      call CompareDontSave(y,t)
   end subroutine CompareAndSave

end module Exact1D


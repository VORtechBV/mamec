! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module FDApplyOperators
use Settings, only: myprec
! Evaluate the FD operators DIV, GRAD, DIVr, rGRAD using the pre-calculated coefficients
! calculated in CalcMatrices_FD.
implicit none

   ! Work arrays: globally allocated and used when needed.
   ! After a subroutine call is finished, their values are no longer relevant.
   ! The purpose of their global allocation is simply to avoid allocating and deallocating them
   real(myprec), pointer :: VxAtC(:), VxAtN(:), VyAtC(:), VyAtE(:)
   real(myprec), pointer :: AdvecXatC(:), AdvecYatC(:)
   real(myprec), pointer :: AdvecXatE(:), AdvecYatE(:)
   real(myprec), pointer :: AdvecXatN(:), AdvecYatN(:)
   real(myprec), pointer :: FxAtYwrk(:), FyAtXwrk(:)
   real(myprec), pointer :: FxAtN(:), FyAtE(:)
   real(myprec), pointer :: FxAtE(:), FyAtN(:)
   real(myprec), pointer :: FxAtC(:), FyAtC(:)

   real(myprec), pointer :: P(:)     =>null()
   real(myprec), pointer :: GradXP(:)=>null(),    GradYP(:)=>null()
   real(myprec), pointer :: AdvecX(:)=>null(),    AdvecY(:)=>null()
   real(myprec), pointer :: DivRV(:) =>null(),  DivRVAtE(:)=>null(), DivRVAtN(:)=>null()
   real(myprec), pointer :: rhoAtE(:)=>null(),    rhoAtN(:)=>null()
   real(myprec), pointer :: vx(:)    =>null(),        vy(:)=>null()

   ! Discretized differential operators
   real(myprec), pointer :: DivXiAtE(:,:), DivEtaAtE(:,:), DivXiAtN(:,:), DivEtaAtN(:,:)
   real(myprec), pointer :: GradXiAtE(:,:), GradEtaAtE(:,:), GradXiAtN(:,:), GradEtaAtN(:,:)

   real(myprec), pointer :: ExactInE2C(:,:) =>null(), ExactInN2C(:,:) =>null(), ExactInC2E(:,:) =>null(), ExactInC2N(:,:) =>null()
   real(myprec), pointer :: ExactOutE2C(:,:)=>null(), ExactOutN2C(:,:)=>null(), ExactOutC2E(:,:)=>null(), ExactOutC2N(:,:)=>null()


contains
  subroutine AllocateFD()
  use Util,            only: error_exit
  use Inputs,          only: GridFunctions, eqident, eqSHALLOW, deriv
  use GridTrafo,       only: GridOrientation
  use Inputs,          only: m,n
  implicit none
      integer :: ierr
      integer :: nDeriv

      nDeriv = abs(lbound(deriv,1))

      allocate( GradXiAtE( 0:m*n-1,  1-nderiv:nderiv ),    &
                GradXiAtN( 0:m*n-1,  1-nderiv:nderiv ),    &
                GradEtaAtE( 0:m*n-1, 1-nderiv:nderiv ),    &
                GradEtaAtN( 0:m*n-1, 1-nderiv:nderiv ),    &
                DivXiAtE( 0:m*n-1,  -nderiv:nderiv-1 ),    &
                DivXiAtN( 0:m*n-1,  -nderiv:nderiv-1 ),    &
                DivEtaAtE( 0:m*n-1, -nderiv:nderiv-1 ),    &
                DivEtaAtN( 0:m*n-1, -nderiv:nderiv-1 ),    &
                stat=ierr)
      if (ierr/=0) call error_exit('Allocation error')
      if (ierr/=0) stop

      allocate(VxAtN(0:n*m-1),  VxAtC(0:n*m-1),  VyAtC(0:n*m-1),  VyAtE(0:n*m-1), stat=ierr)
      if (ierr /= 0) call error_exit('allocation error')

      if (eqident == eqSHALLOW) then
         allocate(FxAtYwrk(0:n*m-1),  FyAtXwrk(0:n*m-1), FxAtE(0:n*m-1),  FyAtN(0:n*m-1),  stat=ierr)
         if (ierr /= 0) call error_exit('allocation error')
         allocate(FxAtC(0:n*m-1),  FyAtC(0:n*m-1), stat=ierr)
         if (ierr /= 0) call error_exit('allocation error')

         allocate(AdvecXatC(0:n*m-1),  AdvecYatC(0:n*m-1), &
                  AdvecXatE(0:n*m-1),  AdvecYatE(0:n*m-1), &
                  AdvecXatN(0:n*m-1),  AdvecYatN(0:n*m-1), stat=ierr)
         if (ierr /= 0) call error_exit('allocation error')
         allocate(  GradXP(0:n*m-1), GradYP(0:n*m-1),   P(0:n*m-1),        AdvecX(0:n*m-1), AdvecY(0:n*m-1), &
                    DivRV(0:n*m-1),  DivRVAtE(0:n*m-1), DivRVAtN(0:n*m-1), rhoAtE(0:n*m-1), rhoAtN(0:n*m-1), &
                    vx(0:n*m-1),     vy(0:n*m-1), stat=ierr)
         if (ierr/=0) call error_exit('allocation error')
      end if
  end subroutine AllocateFD

   ! (fAtC,gAtC) = (E2C*fAtE,N2C*gAtN)
   subroutine InterpV2C(fAtE,gAtN,fAtC,gAtC,set_zero,do_add,exactAtC, exactAtV)
   use GridValues, only: dVe, dVn, dVc
   use Inputs,     only: m,n, destagger
   use Util,       only: error_exit
   implicit none
      real(myprec),      intent(in) :: fAtE(0:), gAtN(0:)
      real(myprec)                  :: fAtC(0:), gAtC(0:)
      logical, optional, intent(in) :: exactAtC, exactAtV
      logical, optional, intent(in) :: set_zero, do_add

      integer      :: irow, di, ix, iy, ndestagger, icol
      real(myprec) :: inprod
      integer      :: j

      if (present(set_zero)) then
         if (.not. set_zero) call error_exit('not possible')
      end if
      if (present(do_add)) then
         if (.not. do_add .or. present(set_zero)) call error_exit('not possible')
      end if

      ndestagger = abs(lbound(destagger,1))

!$OMP PARALLEL DO PRIVATE(ix, iy, di, icol)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         if (present(set_zero)) then
            fAtC(irow) = 0
            gAtC(irow) = 0
         end if
         do di = -ndestagger,ndestagger - 1
            icol = modulo(ix+di,m)+m*iy
            fAtC(irow) = fAtC(irow) + destagger(di)*sqrt(dVe(icol)/dVc(irow)) * fAtE(icol)
            icol = ix + m*modulo(iy+di,n)
            gAtC(irow) = gAtC(irow) + destagger(di)*sqrt(dVn(icol)/dVc(irow)) * gAtN(icol)
         end do
      end do
!$OMP END PARALLEL DO

      if (present(exactAtV)) then
         if (.not. exactAtV) call error_exit('not possible')
         if (present(exactAtC)) call error_exit('not possible')
         if (associated(ExactInE2C)) then
            do j = 1,size(ExactInE2C,2)
               ! fAtC = (I + ExactInC2E * ExactOutC2E^T * diag(dVc)) * P2X0 * fAtE
               do iy = 0,n-1
                  inprod = 0
                  do ix = 0,m-1
                     irow   = ix + m*iy
                     inprod = inprod + ExactOutC2E(irow,j)*fAtE(irow)*dVe(irow)
                  end do
                  do ix = 0,m-1
                     irow   = ix + m*iy
                     fAtC(irow) = fAtC(irow) + inprod * ExactInC2E(irow,j)
                  end do
               end do
               do ix = 0,m-1
                  inprod = 0
                  do iy = 0,n-1
                     irow   = ix + m*iy
                     inprod = inprod + ExactOutC2N(irow,j)*gAtN(irow)*dVn(irow)
                  end do
                  do iy = 0,n-1
                     irow   = ix + m*iy
                     gAtC(irow) = gAtC(irow) + inprod * ExactInC2N(irow,j)
                  end do
               end do
            end do
         end if
      end if

      if (present(exactAtC)) then
         if (.not. exactAtC) call error_exit('not possible')
         if (present(exactAtV)) call error_exit('not possible')
         if (associated(ExactInE2C)) then
            do j = 1,size(ExactInE2C,2)
               do iy = 0,n-1
                  inprod = 0
                  do ix = 0,m-1
                     irow   = ix + m*iy
                     inprod = inprod + ExactInE2C(irow,j)*fAtE(irow)*dVe(irow)
                  end do
                  do ix = 0,m-1
                     irow   = ix + m*iy
                     fAtC(irow) = fAtC(irow) + inprod * ExactOutE2C(irow,j)
                  end do
               end do
               do ix = 0,m-1
                  inprod = 0
                  do iy = 0,n-1
                     irow   = ix + m*iy
                     inprod = inprod + ExactInN2C(irow,j)*gAtN(irow)*dVn(irow)
                  end do
                  do iy = 0,n-1
                     irow   = ix + m*iy
                     gAtC(irow) = gAtC(irow) + inprod * ExactOutN2C(irow,j)
                  end do
               end do
            end do
         end if
      end if

   end subroutine InterpV2C

   ! (fAtE,gAtN) = (C2E*fAtC,C2N*gAtN)
   subroutine InterpC2V(fAtC, gAtC,fAtE,gAtN,set_zero,do_add,exactAtC, exactAtV)
   use GridValues, only: dVe, dVn, dVc
   use Inputs,     only: m,n, destagger
   use Util,       only: error_exit
   implicit none
      real(myprec),      intent(in) :: gAtC(0:), fAtC(0:)
      real(myprec)                  :: gAtN(0:), fAtE(0:)
      logical, optional, intent(in) :: set_zero, do_add
      logical, optional, intent(in) :: exactAtC, exactAtV

      integer      :: irow, di, ix, iy, ndestagger, icol
      real(myprec) :: inprod
      integer      :: j

      if (present(set_zero)) then
         if (.not. set_zero) call error_exit('not possible')
      end if
      if (present(do_add)) then
         if (.not. do_add .or. present(set_zero)) call error_exit('not possible')
      end if

      ndestagger = abs(lbound(destagger,1))

!$OMP PARALLEL DO PRIVATE(ix, iy, di, icol)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         if (present(set_zero)) then
            fAtE(irow) = 0
            gAtN(irow) = 0
         end if
         do di = 1-ndestagger,ndestagger
            icol = modulo(ix+di,m)+m*iy
            fAtE(irow) = fAtE(irow) + destagger(-di) *sqrt(dVc(icol) / dVe(irow)) * fAtC(icol)
            icol = ix + m*modulo(iy+di,n)
            gAtN(irow) = gAtN(irow) + destagger(-di) *sqrt(dVc(icol) / dVn(irow)) * gAtC(icol)
         end do
      end do
!$OMP END PARALLEL DO

      if (present(exactAtV)) then
         if (.not. exactAtV) call error_exit('not possible')
         if (present(exactAtC)) call error_exit('not possible')
         if (associated(ExactInE2C)) then
            do j = 1,size(ExactInE2C,2)
               do iy = 0,n-1
                  inprod = 0
                  do ix = 0,m-1
                     irow   = ix + m*iy
                     inprod = inprod + ExactInC2E(irow,j)*fAtC(irow)*dVc(irow)
                  end do
                  do ix = 0,m-1
                     irow   = ix + m*iy
                     fAtE(irow) = fAtE(irow) + inprod * ExactOutC2E(irow,j)
                  end do
               end do
               do ix = 0,m-1
                  inprod = 0
                  do iy = 0,n-1
                     irow   = ix + m*iy
                     inprod = inprod + ExactInC2N(irow,j)*gAtC(irow)*dVc(irow)
                  end do
                  do iy = 0,n-1
                     irow   = ix + m*iy
                     gAtN(irow) = gAtN(irow) + inprod * ExactOutC2N(irow,j)
                  end do
               end do
            end do
         end if
      end if

      if (present(exactAtC)) then
         if (.not. exactAtC) call error_exit('not possible')
         if (present(exactAtV)) call error_exit('not possible')
         if (associated(ExactInE2C)) then
            do j = 1,size(ExactInE2C,2)
               do iy = 0,n-1
                  inprod = 0
                  do ix = 0,m-1
                     irow   = ix + m*iy
                     inprod = inprod + ExactOutE2C(irow,j)*fAtC(irow)*dVc(irow)
                  end do
                  do ix = 0,m-1
                     irow   = ix + m*iy
                     fAtE(irow) = fAtE(irow) + inprod * ExactInE2C(irow,j)
                  end do
               end do
               do ix = 0,m-1
                  inprod = 0
                  do iy = 0,n-1
                     irow   = ix + m*iy
                     inprod = inprod + ExactOutN2C(irow,j)*gAtC(irow)*dVc(irow)
                  end do
                  do iy = 0,n-1
                     irow   = ix + m*iy
                     gAtN(irow) = gAtN(irow) + inprod * ExactInN2C(irow,j)
                  end do
               end do
            end do
         end if
      end if

   end subroutine InterpC2V

   ! fAtN = X2Y * fAtE
   ! gAtE = Y2X * gAtN
   subroutine InterpVelocity(fAtE,gAtN, fAtN,gAtE, do_add, set_zero, exactIn, exactOut)
   implicit none
      real(myprec),      intent(in)    :: fAtE(0:), gAtN(0:)
      real(myprec),      intent(inout) :: fAtN(0:), gAtE(0:)
      logical, optional, intent(in)    :: do_add, set_zero
      logical, optional, intent(in)    :: exactIn, exactOut
      ! Available work arrays: VxAtC, VyAtC

      call InterpV2C(fAtE,gAtN,VxAtC,VyAtC,             set_zero=.true., exactAtV=exactIn, exactAtC=exactOut)
      call InterpC2V(          VyAtC,VxAtC, gAtE, fAtN, set_zero=set_zero, do_add = do_add, exactAtV=exactOut, exactAtC=exactIn)
   end subroutine InterpVelocity

   function InterDens(rhoa,rhob) result(dens)
   use StateEq, only: Sfun, Qfun
   implicit none
      real(myprec), intent(in) :: rhoa, rhob

      real(myprec) :: dens
      real(myprec) :: rho1, rho2, dS, dQ

      rho1 = max(rhoa,rhob)
      rho2 = min(rhoa,rhob)
      dS   = Sfun(rho1)-Sfun(rho2)
      dQ   = Qfun(rho1)-Qfun(rho2)
      dens = rho2 + (rho1-rho2)/2
      dens = dens - (dens*dS - dQ) / max(1e-6,dS)
      dens = max(rho2,min(rho1,dens))
   end function InterDens

   subroutine ApplyDensTimesGradient(rho,f,rGradX,rGradY)
   use Inputs, only: m,n, deriv
   implicit none
      real(myprec), intent(in)    :: rho(0:), f(0:)
      real(myprec), intent(inout) :: rGradX(0:), rGradY(0:)

      ! Available work arrays: VxAtN, VyAtE
      integer      :: irow, icol, di, ix, iy, nderiv, jcol
      real(myprec) :: dens

      nDeriv = abs(lbound(deriv,1))

!$OMP PARALLEL DO PRIVATE (ix,iy,di,icol,jcol,dens)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         VxAtN(irow)  = 0
         VyAtE(irow)  = 0
         rGradX(irow) = 0
         rGradY(irow) = 0
         do di = 1,nderiv
            icol = modulo(ix  +di,m) + m*iy
            jcol = modulo(ix+1-di,m) + m*iy
            dens = InterDens(rho(icol),rho(jcol))
            VyAtE(irow)  = VyAtE(irow)  + GradEtaAtE(irow,di) * dens * (f(icol) - f(jcol))
            rGradX(irow) = rGradX(irow) + GradXiAtE(irow,di)  * dens * (f(icol) - f(jcol))

            icol = ix + m*modulo(iy+  di,n)
            jcol = ix + m*modulo(iy+1-di,n)
            dens = InterDens(rho(icol),rho(jcol))
            rGradY(irow) = rGradY(irow) + GradEtaAtN(irow,di) * dens * (f(icol) - f(jcol))
            VxAtN(irow)  = VxAtN(irow)  + GradXiAtN(irow,di)  * dens * (f(icol) - f(jcol))
         end do
      end do
!$OMP END PARALLEL DO

      ! (rGradX,rGradY) += (Y2X*VxAtN, X2Y*VyAtE)
      call InterpVelocity(VyAtE, VxAtN,rGradY, rGradX, do_add = .true., exactIn=.true. )

   end subroutine ApplyDensTimesGradient

   subroutine ApplyGradient(f,GradX,GradY)
   use TestFunctions, only: TestValues
   use Inputs, only: m,n, deriv
   implicit none
      real(myprec), intent(in)  :: f(0:)
      real(myprec), intent(out) :: GradX(0:), GradY(0:)

      ! Available work arrays: VxAtN, VyAtE
      integer :: irow, icol, di, ix, iy, nderiv

      nDeriv = abs(lbound(deriv,1))

!$OMP PARALLEL DO PRIVATE (ix,iy,di,icol)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         VxAtN(irow) = 0
         VyAtE(irow) = 0
         GradX(irow) = 0
         GradY(irow) = 0
         do di = 1-nderiv,nderiv
            icol = modulo(ix+di,m) + m*iy
            VyAtE(irow) = VyAtE(irow) + GradEtaAtE(irow,di) * f(icol)
            GradX(irow) = GradX(irow) + GradXiAtE(irow,di)  * f(icol)
            icol = ix + m*modulo(iy+di,n)
            GradY(irow) = GradY(irow) + GradEtaAtN(irow,di) * f(icol)
            VxAtN(irow) = VxAtN(irow) + GradXiAtN(irow,di)  * f(icol)
         end do
      end do
!$OMP END PARALLEL DO

      ! (GradX,GradY) += (Y2X*VxAtN, X2Y*VyAtE)
      call InterpVelocity(VyAtE, VxAtN,GradY, GradX, do_add = .true., exactIn=.true. )

   end subroutine ApplyGradient

   subroutine ApplyDivergence2(vx,vy,VxAtN,VyAtE,DivV)
   use Inputs, only: m,n, deriv
   implicit none
      real(myprec), intent(in)  :: vx(0:), vy(0:)
      real(myprec), intent(in)  :: VxAtN(0:), VyAtE(0:)
      real(myprec), intent(out) :: DivV(0:)

      integer :: irow, di, ix, iy, nderiv, icol1, icol2

      nDeriv = abs(lbound(deriv,1))

!$OMP PARALLEL DO PRIVATE(ix, iy, di, icol1, icol2)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         DivV(irow) = 0
         do di = -nderiv,nderiv - 1
            icol1 = ix + m*modulo(iy+di,n)
            icol2 = modulo(ix+di,m)+m*iy
            DivV(irow) = DivV(irow) + &
                         DivXiAtE(irow,di)  * vx(   icol2) + &
                         DivEtaAtE(irow,di) * VyAtE(icol2) + &
                         DivEtaAtN(irow,di) * vy(   icol1) + &
                         DivXiAtN(irow,di)  * VxAtN(icol1)
         end do
      end do
!$OMP END PARALLEL DO

   end subroutine ApplyDivergence2

   subroutine ApplyDivergence(vx,vy,DivV)
   implicit none
      real(myprec), intent(in)  :: vx(0:), vy(0:)
      real(myprec), intent(out) :: DivV(0:)
      ! Available work arrays: VxAtN, VyAtE

      call InterpVelocity(vx,vy,VxAtN,VyAtE, set_zero=.true., exactOut = .true.)
      call ApplyDivergence2(vx,vy,VxAtN,VyAtE,DivV)
   end subroutine ApplyDivergence

   subroutine ApplyDivergenceDensTimes2(rho,vx,vy,VxAtN,VyAtE,DivRV)
   use Inputs, only: m,n, deriv
   implicit none
      real(myprec), intent(in)    :: rho(0:)
      real(myprec), intent(in)    :: vx(0:), vy(0:)
      real(myprec), intent(in)    :: VxAtN(0:), VyAtE(0:)
      real(myprec), intent(inout) :: DivRV(0:)

      integer      :: irow, di, ix, iy, nderiv, icol, jrow
      real(myprec) :: dens

      nDeriv = abs(lbound(deriv,1))

!$OMP PARALLEL DO PRIVATE(ix, iy, di, icol, jrow, dens)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         DivRV(irow) = 0
         do di = -nderiv,nderiv - 1
            icol = modulo(ix+di,m)+m*iy
            ! u-point ix-2 connects to p-point ix-3
            ! <------------*---------------->
            ! u-point ix-1 connects to p-point ix-1
            !                   <----*------>
            ! +   -    +   -    +     -     +    -    +    -    +    -    +
            !-3   -3  -2  -2   -1    -1     0    0    1    1    2    2    3
            !                               <----*---->
            ! u-point ix   connects to p-point ix+1
            !                               <--------------*-------------->
            ! u-point ix+1  connects to p-point ix+3
            ! u-point ix+id connects to p-point ix+2*id+1
            jrow = modulo(ix+2*di+1,m)+m*iy
            dens = InterDens(rho(irow),rho(jrow))
            DivRV(irow) = DivRV(irow) + &
                          dens * ( DivXiAtE(irow,di)  * vx(   icol) + &
                                   DivEtaAtE(irow,di) * VyAtE(icol) )

            icol = ix + m*modulo(iy+di,n)
            jrow = ix + m*modulo(iy+2*di+1,n)
            dens = InterDens(rho(irow),rho(jrow))
            DivRV(irow) = DivRV(irow) + &
                          dens * ( DivEtaAtN(irow,di) * vy(   icol) + &
                                   DivXiAtN(irow,di)  * VxAtN(icol) )
         end do
      end do
!$OMP END PARALLEL DO

   end subroutine ApplyDivergenceDensTimes2

   subroutine ApplyDivergenceDensTimes(rho,vx,vy,DivRhoV)
   implicit none
      real(myprec), intent(in)  :: rho(0:)
      real(myprec), intent(in)  :: vx(0:), vy(0:)
      real(myprec), intent(out) :: DivRhoV(0:)
      ! Available work arrays: VxAtN, VyAtE

      call InterpVelocity(vx,vy,VxAtN,VyAtE, set_zero=.true.,  exactOut=.true.)
      call ApplyDivergenceDensTimes2(rho,vx,vy,VxAtN,VyAtE,DivRhoV)
   end subroutine ApplyDivergenceDensTimes

   ! (AdvectionXatC, AdvectionYatC) = DIV(rho*v) * (fx,fy,FxAtN,FyAtN)
   subroutine ApplyAdvection2(rho,vx,vy,VxAtN,VyAtE,fx,fy,FxAtN,FyAtE, &
                             DivRV,AdvecXatC, AdvecYatC)
   use GridValues, only: GridOrientationXatE, GridOrientationYatE
   use GridValues, only: GridOrientationXatN, GridOrientationYatN
   use Inputs,     only: m,n, deriv
   implicit none

      ! Inputs
      real(myprec), intent(in)  :: rho(0:)
      real(myprec), intent(in)  :: vx(0:), vy(0:)
      real(myprec), intent(in)  :: VxAtN(0:), VyAtE(0:)
      real(myprec), intent(in)  :: fx(0:), fy(0:)
      real(myprec), intent(in)  :: FxAtN(0:), FyAtE(0:)

      ! Outputs
      real(myprec), intent(out) :: DivRV(0:)
      real(myprec), intent(out) :: AdvecXatC(0:), AdvecYatC(0:)

      ! Locals
      integer      :: irow, di, ix, iy, nderiv, icol, jrow
      real(myprec) :: dens

      nDeriv = abs(lbound(deriv,1))
!$OMP PARALLEL DO PRIVATE(ix, iy, di, icol, jrow, dens)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         DivRV(irow)     = 0
         AdvecXatC(irow) = 0
         AdvecYatC(irow) = 0
         do di = -nderiv,nderiv - 1
            icol = modulo(ix+di,m)+m*iy
            ! u-point ix-2 connects to p-point ix-3
            ! <------------*---------------->
            ! u-point ix-1 connects to p-point ix-1
            !                   <----*------>
            ! +   -    +   -    +     -     +    -    +    -    +    -    +
            !-3   -3  -2  -2   -1    -1     0    0    1    1    2    2    3
            !                               <----*---->
            ! u-point ix   connects to p-point ix+1
            !                               <--------------*-------------->
            ! u-point ix+1  connects to p-point ix+3
            ! u-point ix+id connects to p-point ix+2*id+1
            jrow = modulo(ix+2*di+1,m)+m*iy
            dens = InterDens(rho(irow),rho(jrow))
            DivRV(irow) = DivRV(irow) + &
                          dens * ( DivXiAtE(irow,di)  * vx(   icol) + &
                                   DivEtaAtE(irow,di) * VyAtE(icol) )

            AdvecXatC(irow) = AdvecXatC(irow) + &
                          dens * ( DivXiAtE(irow,di)  * vx(   icol) + &
                                   DivEtaAtE(irow,di) * VyAtE(icol) ) * &
                        ( GridOrientationXatE(modulo(ix+di,m),iy) * fx(icol) - &
                          GridOrientationYatE(modulo(ix+di,m),iy) * FyAtE(icol) )

            AdvecYatC(irow) = AdvecYatC(irow) + &
                          dens * ( DivXiAtE(irow,di)  * vx(   icol) + &
                                   DivEtaAtE(irow,di) * VyAtE(icol) ) * &
                        ( GridOrientationYatE(modulo(ix+di,m),iy) * fx(icol) + &
                          GridOrientationXatE(modulo(ix+di,m),iy) * FyAtE(icol) )

            icol = ix + m*modulo(iy+di,n)
            jrow = ix + m*modulo(iy+2*di+1,n)
            dens = InterDens(rho(irow),rho(jrow))
            DivRV(irow) = DivRV(irow) + &
                          dens * ( DivEtaAtN(irow,di) * vy(   icol) + &
                                   DivXiAtN(irow,di)  * VxAtN(icol) )

            AdvecXatC(irow) = AdvecXatC(irow) + &
                          dens * ( DivEtaAtN(irow,di) * vy(   icol) + &
                                   DivXiAtN(irow,di)  * VxAtN(icol) ) * &
                        ( GridOrientationXatN(ix , modulo(iy+di,n)) * FxAtN(icol) - &
                          GridOrientationYatN(ix , modulo(iy+di,n)) * fy(icol) )

            AdvecYatC(irow) = AdvecYatC(irow) + &
                          dens * ( DivEtaAtN(irow,di) * vy(   icol) + &
                                   DivXiAtN(irow,di)  * VxAtN(icol) ) * &
                        ( GridOrientationYatN(ix , modulo(iy+di,n)) * FxAtN(icol) + &
                          GridOrientationXatN(ix , modulo(iy+di,n)) * fy(icol) )
         end do

      end do
!$OMP END PARALLEL DO

   end subroutine ApplyAdvection2

   subroutine ApplyAdvectionA(rho,vx,vy,fx,fy,DivRhoV, AdvecX, AdvecY)
   use GridValues, only: GridOrientationXatE, GridOrientationYatE
   use GridValues, only: GridOrientationXatN, GridOrientationYatN
   use Inputs,     only: m,n
   implicit none
      real(myprec),          intent(in)  :: rho(0:)
      real(myprec), pointer, intent(in)  :: vx(:), vy(:)
      real(myprec), pointer, intent(in)  :: fx(:), fy(:)
      real(myprec),          intent(out) :: DivRhoV(0:)
      real(myprec),          intent(out) :: AdvecX(0:)
      real(myprec),          intent(out) :: AdvecY(0:)

      integer :: ix, iy, irow
      ! Available work arrays: VxAtN, VyAtE, FxAtN, FyAtE,
      !   AdvecXatC, AdvecYatC, AdvecXatE, AdvecXatN, AdvecYatE, AdvecYatN

      call InterpVelocity(vx,vy,VxAtN,VyAtE, set_zero=.true., exactOut=.true.)

      if (associated(vx,fx) .and. associated(vy,fy)) then
         ! (fx,fy) are the same as (vx,vy) : set pointers to interpolated values
         FxAtN => VxAtN
         FyAtE => VyAtE
      else
         FxAtN => FxAtYwrk
         FyAtE => FyAtXwrk
         call InterpVelocity(fx,fy,FxAtN,FyAtE, set_zero=.true., exactOut=.true.)
      end if

      ! Set DivRV and also the hidden fields AdvecXatC and AdvecYatC
      call ApplyAdvection2(rho,vx,vy,VxAtN,VyAtE,fx,fy,FxAtN,FyAtE,DivRhoV,AdvecXatC,AdvecYatC)

      ! Interpolate AdvecXatC and AdvecYatC to get the advections.
      call InterpC2V(AdvecXatC, AdvecYatC, AdvecXatE, AdvecYatN, set_zero=.true., exactAtC=.true.)
      call InterpC2V(AdvecYatC, AdvecXatC, AdvecYatE, AdvecXatN, set_zero=.true., exactAtC=.true.)

!$OMP PARALLEL DO PRIVATE(ix, iy)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         AdvecX(irow) =   AdvecXatE(irow) * GridOrientationXatE(ix, iy) + &
                          AdvecYatE(irow) * GridOrientationYatE(ix, iy)
         AdvecY(irow) = - AdvecXatN(irow) * GridOrientationYatN(ix, iy) + &
                          AdvecYatN(irow) * GridOrientationXatN(ix, iy)
      end do
!$OMP END PARALLEL DO
   end subroutine ApplyAdvectionA

   ! Adjoint of ApplyAdvection2:
   !  (AdvecX,AdvecY,AdvecXatN,AdvecYatE) = DIV(rho*v)^T * (FxAtC, FyAtC)
   subroutine ApplyAdvection2a(rho, vx, vy, VxAtN, VyAtE, FxAtC, FyAtC, &
                                   AdvecX, AdvecY, AdvecXatN, AdvecYatE)
   use GridValues, only: GridOrientationXatE, GridOrientationYatE
   use GridValues, only: GridOrientationXatN, GridOrientationYatN
   use Inputs,     only: m,n, deriv
   implicit none

      ! Inputs
      real(myprec), intent(in)  :: rho(0:)
      real(myprec), intent(in)  :: vx(0:),    vy(0:)
      real(myprec), intent(in)  :: VxAtN(0:), VyAtE(0:)
      real(myprec), intent(in)  :: FxAtC(0:), FyAtC(0:)

      ! Outputs
      real(myprec), intent(out)  :: AdvecX(0:), AdvecY(0:)
      real(myprec), intent(out)  :: AdvecXatN(0:), AdvecYatE(0:)

      ! Locals
      integer      :: irow, di, ix, iy, nderiv, icol, jcol
      real(myprec) :: dens

      nDeriv = abs(lbound(deriv,1))
!$OMP PARALLEL DO PRIVATE(ix, iy, di, icol, jcol, dens)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         AdvecX(irow)    = 0
         AdvecY(irow)    = 0
         AdvecXatN(irow) = 0
         AdvecYatE(irow) = 0
         do di = 1,nderiv
            icol = modulo(ix  +di,m) + m*iy
            jcol = modulo(ix+1-di,m) + m*iy
            dens = InterDens(rho(icol),rho(jcol))

            AdvecX(irow) =  AdvecX(irow) + &
                          dens * ( GradXiAtE(irow,di)  * vx(   irow) + &
                                   GradEtaAtE(irow,di) * VyAtE(irow) ) * &
                          ( GridOrientationXatE(ix,iy) * (FxAtC(icol) - FxAtC(jcol)) + &
                            GridOrientationYatE(ix,iy) * (FyAtC(icol) - FyAtC(jcol)) )

            AdvecYatE(irow) = AdvecYatE(irow)  + &
                          dens * ( GradXiAtE(irow,di)  * vx(   irow) + &
                                   GradEtaAtE(irow,di) * VyAtE(irow) ) * &
                          (- GridOrientationYatE(ix,iy) * (FxAtC(icol) - FxAtC(jcol)) + &
                             GridOrientationXatE(ix,iy) * (FyAtC(icol) - FyAtC(jcol)) )

            icol = ix + m*modulo(iy+  di,n)
            jcol = ix + m*modulo(iy+1-di,n)
            dens = InterDens(rho(icol),rho(jcol))

            AdvecY(irow) = AdvecY(irow)  + &
                          dens * ( GradEtaAtN(irow,di) * vy(   irow) + &
                                   GradXiAtN(irow,di)  * VxAtN(irow) ) * &
                          ( - GridOrientationYatN(ix,iy) * (FxAtC(icol) - FxAtC(jcol)) + &
                              GridOrientationXatN(ix,iy) * (FyAtC(icol) - FyAtC(jcol)) )

            AdvecXatN(irow) = AdvecXatN(irow) + &
                          dens * ( GradEtaAtN(irow,di) * vy(   irow) + &
                                   GradXiAtN(irow,di)  * VxAtN(irow) ) * &
                        (  GridOrientationXatN(ix,iy) * (FxAtC(icol) - FxAtC(jcol)) + &
                           GridOrientationYatN(ix,iy) * (FyAtC(icol) - FyAtC(jcol)) )
         end do
      end do
!$OMP END PARALLEL DO

   end subroutine ApplyAdvection2a

   ! Adjoint of ApplyAdvectionA
   subroutine ApplyAdvectionB(rho,vx,vy,fx,fy,AdvecX, AdvecY)
   use GridValues, only: GridOrientationXatE, GridOrientationYatE
   use GridValues, only: GridOrientationXatN, GridOrientationYatN
   use Inputs,     only: m,n
   implicit none

      ! Inputs:
      real(myprec), intent(in)  :: rho(0:)
      real(myprec), intent(in)  :: vx(0:), vy(0:)
      real(myprec), intent(in)  :: fx(0:), fy(0:)

      ! Outputs:
      real(myprec), intent(out) :: AdvecX(0:), AdvecY(0:)

      ! Locals:
      integer :: ix, iy, irow

      ! Available work arrays: FyAtN, FxAtN, FxAtE, FyAtE,
      !   VxAtN, VyAtE, FxAtN, FyAtE, AdvecXatN, AdvecYatE

      call InterpVelocity(vx,vy,VxAtN,VyAtE,set_zero=.true., exactOut=.true.)

      FxAtN => FxAtYwrk
      FyAtE => FyAtXwrk

!$OMP PARALLEL DO PRIVATE(ix, iy)
      do irow = 0,n*m-1
         ix = mod(irow,m)
         iy = irow/m
         FxAtE(irow) =   fx(irow) * GridOrientationXatE(ix, iy)
         FyAtE(irow) =   fx(irow) * GridOrientationYatE(ix, iy)
         FxAtN(irow) = - fy(irow) * GridOrientationYatN(ix, iy)
         FyAtN(irow) =   fy(irow) * GridOrientationXatN(ix, iy)
      end do
!$OMP END PARALLEL DO

      ! FxAtC = E2C * FxAtE + N2C * FxAtN
      ! FyAtC = E2C * FyAtE + N2C * FyAtN
      call InterpV2C( FxAtE, FyAtN, FxAtC, FyAtC, set_zero=.true., exactAtC=.true.)
      call InterpV2C( FyAtE, FxAtN, FyAtC, FxAtC,   do_add=.true., exactAtC=.true.)

      ! (AdvecX,AdvecY,AdvecXatN,AdvecYatE) = DIV(rho*v)^T * (FxAtC, FyAtC)
      call ApplyAdvection2a(rho, vx, vy, VxAtN, VyAtE, FxAtC, FyAtC, &
                                   AdvecX, AdvecY, AdvecXatN, AdvecYatE)

      ! AdvecX = AdvecX + Y2Xa * AdvecXatN
      ! AdvecY = AdvecY + X2Ya * AdvecXatN
      call InterpVelocity(AdvecYatE, AdvecXatN, AdvecY, AdvecX, do_add = .true., exactIn=.true.)

   end subroutine ApplyAdvectionB

   subroutine ApplyAdvection(rho,vx,vy,fx,fy,DivRhoV, DivRhoVAtE, DivRhoVAtN, AdvecX, AdvecY)
   use Inputs, only: m,n
   implicit none
      real(myprec),          intent(in)  :: rho(0:)
      real(myprec), pointer, intent(in)  :: vx(:), vy(:)
      real(myprec), pointer, intent(in)  :: fx(:), fy(:)
      real(myprec),         intent(out) :: DivRhoV(0:)
      real(myprec),         intent(out) :: DivRhoVAtE(0:)
      real(myprec),         intent(out) :: DivRhoVAtN(0:)
      real(myprec),         intent(out) :: AdvecX(0:)
      real(myprec),         intent(out) :: AdvecY(0:)

      integer :: irow

      ! Available work arrays: VxAtN, VyAtE, FxAtN, FyAtE,
      !   AdvecXatC, AdvecYatC, AdvecXatE, AdvecXatN, AdvecYatE, AdvecYatN
      call ApplyAdvectionA(rho,vx,vy,fx,fy,DivRhoV, AdvecX, AdvecY)
      call InterpC2V(DivRhoV, DivRhoV, DivRhoVatE, DivRhoVatN, set_zero=.true., exactAtC=.true.)

      ! Available work arrays: FyAtN, FxAtN, FxAtE, FyAtE,
      !   VxAtN, VyAtE, FxAtN, FyAtE, AdvecXatN, AdvecYatE
      call ApplyAdvectionB(rho,vx,vy,fx,fy,AdvecXatE, AdvecYatN)

!$OMP PARALLEL DO
      do irow = 0,n*m-1
         AdvecX(irow) =  0.5 * (AdvecX(irow) + AdvecXatE(irow) + DivRhoVatE(irow) * fx(irow))
         AdvecY(irow) =  0.5 * (AdvecY(irow) + AdvecYatN(irow) + DivRhoVatN(irow) * fy(irow))
      end do
!$OMP END PARALLEL DO

   end subroutine ApplyAdvection

end module FDApplyOperators

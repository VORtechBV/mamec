! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module GalerkinBaseFunctions
! This module provides the interpolation functions used in the Galerkin-style methods
! These functions are piecewise polynomial functions, and they are coded in two
! different ways, for historic reasons.
!
! The function PrepareLegendreInterp calculates the base functions for interpolation:
! the resulting interpolating function is the piecewise Legendre polynomial,
! where the points -nSpan:1-nSpan are used to interpolate between the points 0 and 1.
use Settings, only: myprec
implicit none

    integer, parameter      :: scaleFunction_3_1_nCont = 1
    real(myprec), parameter :: scaleFunction_3_1(0:9,0:2) = reshape([ &
        9.25925926e-01, -5.55555556e-02, -2.67841305e-15, 2.92127433e-15, -1.09634524e-15, -1.38888889e-01, &
            9.25925926e-03, -1.66533454e-15, 1.40512602e-15, -4.44089210e-16, &
        5.00000000e-01, 5.62050406e-16, -6.76542156e-16, 6.89552582e-16, -2.11636264e-16, -2.50000000e-01, &
            9.25925926e-03, 1.95156391e-15, -2.07299455e-15, 7.55472074e-16, &
        7.40740741e-02, 5.55555556e-02, 1.69265643e-15, -2.05304523e-15, 7.77156117e-16, -1.38888889e-01, &
            9.25925926e-03, 4.31946146e-15, -4.42007542e-15, 1.60288449e-15 &
       ],[10,3])

    real(myprec), pointer   :: u_scale(:,:) => null()
    real(myprec), pointer   :: u_input(:,:) => null()

contains

   subroutine PrepareLegendreInterp(u,nSpan)
   use Util, only: error_exit
   implicit none
       real(myprec), pointer :: u(:,:)
       integer, intent(in)   :: nSpan

       real(myprec), pointer :: p(:)
       integer               :: K, i, j, im
       integer               :: ierr

       K = 2*nSpan+1
       allocate(u(0:K,0:nSpan-1), p(0:K),  stat=ierr)
       if (ierr/=0) call error_exit('Allocation error')
       if (ierr/=0) stop 'error'

       do i = 0,nSpan-1
          p = 0
          p(0) = 1
          do j = i-nSpan+1,i+nSpan
              if (j/=0) then
                 do im = K,1,-1
                    p(im) = p(im) - (p(im-1)+(1+2*i)*p(im))/(2*j)
                 end do
                 p(0) = p(0) - (1+2*i)*p(0)/(2*j)
              end if
          end do
          do im = 0,K,2
             u(      im/2,i) = p(im  )
             u(K/2+1+im/2,i) = p(im+1)
          end do
       end do

       deallocate(p,  stat=ierr)
       if (ierr/=0) call error_exit('Deallocation error')

   end subroutine PrepareLegendreInterp

   ! Set the array u_scale, which is the coded scaling function
   ! The coding is used in ProcessPiecewisePolynomials and EvalPiecewisePolynomial, below
   subroutine SetUscale(u)
   implicit none
      real(myprec), intent(in), target, contiguous :: u(:,:)
      u_scale(0:size(u,1)-1,0:size(u,2)-1) => u
   end subroutine SetUscale

   subroutine ProcessPiecewisePolynomials(u_input, u, du)
   ! Convert the piecewise polynomial from the input format to a simpler format,
   ! from which u and du/dx can be evaluated by function EvalPiecewisePolynomial, below.
   use Settings, only: myprec
   implicit none

     real(myprec),          intent(in)  :: u_input(0:,0:)
     real(myprec), pointer, intent(out) :: u(:,:)
     real(myprec), pointer, intent(out) :: du(:,:)

     integer :: i
     integer :: jk, jk0, jk1
     integer :: K, nSpan

     K     = ubound(u_input,1)
     nSpan = size(u_input,2)

     allocate(u(0:K, -nSpan:nSpan), du(0:K,-nSpan:nSpan))
     u(:,:) = 0
     u(:,0:nSpan-1) = u_input(:,0:nSpan-1)
     do i = 1,nSpan
        u(:,-i) = u(:,i-1)
        u((K+1)/2:,-i) = -u((K+1)/2:,-i)
     end do

     du(:,:) = 0
     do i = -nSpan,nSpan-1
        do jk = 0,K-1
           if (mod(jk,2)==1) then
              jk0 = (jk-1)/2 + (K+1)/2
              jk1 = (jk+1)/2
           else
              jk0 = jk/2
              jk1 = jk/2 + (K+1)/2
           end if
           du(jk0,i) = u(jk1,i) * (jk+1) * 2
        end do
     end do
   end subroutine ProcessPiecewisePolynomials

   subroutine EvalPiecewisePolynomial(u_input, f, twoP, derivOrder)
   ! Calculate the function values in the points -nSpan:1/twoP:nSpan
   use Util,     only: error_exit
   use Settings, only: myprec
   implicit none
      real(myprec),              intent(in)  :: u_input(0:,0:)
      real(myprec), allocatable, intent(out) :: f(:)
      integer,                   intent(in)  :: twoP
      integer, optional,         intent(in)  :: derivOrder

      real(myprec), pointer :: u(:,:), du(:,:)
      integer               :: nSpan, K, i0
      real(myprec)          :: xi
      integer               :: i, jk, jk0, ierr, ix

      call ProcessPiecewisePolynomials(u_input, u, du)
      if (present(derivOrder)) then
         if (derivOrder==0) then
            deallocate(du)
         else if (derivOrder==1) then
            deallocate(u)
            u => du
         else
            call error_exit('only derivOrder=0 or derivOrder=1 possible')
         end if
      end if
      nSpan = ubound(u,2)
      K     = ubound(u,1)
      i0    = nSpan*(twoP+1)

      allocate(f(-i0:i0-1), stat=ierr)
      if (ierr/=0) call error_exit('Allocation error')
      print *
      do ix = -i0,i0-1
         i  = floor(dble(ix)/(twoP+1))
         xi = dble(mod(ix+nSpan*(twoP+1),twoP+1)) /twoP
         f(ix) = 0
         do jk = 0,K
            if (mod(jk,2)==1) then
               jk0 = (jk-1)/2 + (K+1)/2
            else
               jk0 = jk/2
            end if
            f(ix)  =  f(ix) + (2*xi-1)**jk * u(jk0,i)
         end do
      end do
      deallocate(u)
   end subroutine EvalPiecewisePolynomial

end module GalerkinBaseFunctions

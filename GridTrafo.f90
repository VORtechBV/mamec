! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module GridTrafo
use Settings, only: myprec
! This module calculates (x,y) and their derivatives, given (xi,eta)
implicit none

   integer,      save      :: m=30, n=30
   real(myprec), save      :: ScaleCurv = 1.0
   real(myprec), parameter :: pi = 4*atan2(1d0,1d0)

contains

   subroutine GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
   implicit none
      real(myprec), intent(in)  :: dxdxi, dydxi, dxdeta, dydeta
      real(myprec), intent(out) :: Ax, Ay

      real(myprec) :: norm

      Ax = dxdxi  + dydeta
      Ay = dxdeta - dydxi

      norm = sqrt(Ax*Ax + Ay*Ay)
      Ax = Ax / norm
      Ay = Ay / norm
   end subroutine GridOrientation

   ! The following grid functions return (x,y) as a function of (xi,eta),
   ! and their derivatives.
   !    x(xi+m,eta+n) = x(xi,eta) + (m,n) * ( cos(a),sin(a))
   !    y(xi+m,eta+n) = y(xi,eta) + (m,n) * (-sin(a),cos(a))

   subroutine GridFunctionsLin(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
   implicit none
      real(myprec),           intent(in)  :: xi, eta
      real(myprec), optional, intent(out) :: x,y
      real(myprec), optional, intent(out) :: dxdxi, dydxi, dxdeta, dydeta

      real(myprec), parameter :: a0 = 1.0
      real(myprec)            :: a

      a = ScaleCurv * a0

      ! x(xi+p*m,eta+q*n) = x(xi,eta) + p + (q-p)*a
      if (present(x))      x      =  cos(a) * xi/m + sin(a)* eta/n
      if (present(y))      y      = -sin(a) * xi/m + cos(a)* eta/n
      if (present(dxdxi))  dxdxi  =  cos(a)
      if (present(dxdeta)) dxdeta =  sin(a)
      if (present(dydxi))  dydxi  = -sin(a)
      if (present(dydeta)) dydeta =  cos(a)
   end subroutine GridFunctionsLin

   subroutine GridFunctionsCos(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
   implicit none
      real(myprec), intent(in)            :: xi, eta
      real(myprec), optional, intent(out) :: x,y
      real(myprec), optional, intent(out) :: dxdxi, dydxi, dxdeta, dydeta

      real(myprec), parameter :: a0(3,3) = reshape( [ &
           0.0, 0.05, -0.05,    -0.025, 0.04, 0.0,    0.02, -0.01, 0.0 &
           ], [3,3])
      real(myprec), parameter :: b0(3,3) = reshape( [ &
           0.0, -0.015, -0.02,       0.01, -0.03, 0.0,    0.015, -0.01, 0.0 &
           ], [3,3])
      real(myprec)            :: a(3,3), b(3,3)

      a = ScaleCurv * a0
      b = ScaleCurv * b0

      if (present(x)) &
      x = -0.1d0 + xi/m + &
       (1+cos(2*pi*xi/m)) * (a(2,1) + a(2,2) * (1+cos(2*pi*eta/n))) + &
       (1+cos(2*pi*eta/n)) * a(1,2) + &
       sin(2*pi*xi/m) * (a(3,1) + a(3,2) * (1+cos(2*pi*eta/n)))
      if (present(dxdxi)) &
      dxdxi = 1.0d0/m  &
          - 2*pi/m*sin(2*pi*xi/m) * (a(2,1) + a(2,2) * (1+cos(2*pi*eta/n))) &
          + 2*pi/m*cos(2*pi*xi/m) * (a(3,1) + a(3,2) * (1+cos(2*pi*eta/n)))
      if (present(dxdeta)) &
      dxdeta = &
         (1+cos(2*pi*xi/m)) * ( - 2*pi/n * a(2,2) * sin(2*pi*eta/n)) &
        - 2*pi/n * sin(2*pi*eta/n) * a(1,2) &
        + sin(2*pi*xi/m) * ( -2*pi/n * a(3,2) * sin(2*pi*eta/n))
      if (present(y)) &
      y = 0.1d0 + eta/n + &
         (1+cos(2*pi*xi/m)) * (b(2,1) + b(2,2) * (1+cos(2*pi*eta/n))) + &
         (1+cos(2*pi*eta/n)) * b(1,2) + &
         sin(2*pi*xi/m) * (b(3,1) + b(3,2) * (1+cos(2*pi*eta/n)));
      if (present(dydxi)) &
      dydxi = &
         - 2*pi/m*sin(2*pi*xi/m) * (b(2,1) + b(2,2) * (1+cos(2*pi*eta/n))) &
         + 2*pi/m*cos(2*pi*xi/m) * (b(3,1) + b(3,2) * (1+cos(2*pi*eta/n)))
      if (present(dydeta)) &
      dydeta = 1d0/n  &
         + (1+cos(2*pi*xi/m)) * (-2*pi/n * b(2,2) * sin(2*pi*eta/n)) &
         - 2*pi/n * sin(2*pi*eta/n) * b(1,2) &
         + sin(2*pi*xi/m) * ( -2*pi/n * b(3,2) * sin(2*pi*eta/n));
   end subroutine GridFunctionsCos

   subroutine GridFunctionsPoly(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
   implicit none
      real(myprec), intent(in)  :: xi, eta
      real(myprec), intent(out) :: x,y, dxdxi, dydxi, dxdeta, dydeta

      real(myprec)            :: f0
      real(myprec), parameter :: eps=40, eps2=200  ! Amount of grid non-uniformness (eps=eps2=0: uniform grid)
      real(myprec)            :: epsa, eps2a

      epsa  = eps*ScaleCurv/dble(n*m)**4
      eps2a = eps2*ScaleCurv/(2*m*dble(m*n)**4)

      f0 = xi*(m-xi)*eta*(n-eta)

      x      = xi/m  +   epsa   * f0*f0
      y      = eta/n +   eps2a  * f0*f0 * (2*xi-m)

      dxdxi  = 1d0/m + 2*epsa  * f0    * (m-2*xi) *  eta*(n-eta)
      dxdeta =         2*epsa  * f0    * xi*(m-xi)*  (n-2*eta)
      dydxi  =         2*eps2a * f0    * (5*xi*m-m*m-5*xi**2) *  eta*(n-eta)
      dydeta = 1d0/n + 2*eps2a * f0    * xi*(m-xi)*(2*xi-m) * (n-2*eta)
   end subroutine GridFunctionsPoly
end module GridTrafo

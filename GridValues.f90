! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module GridValues
use Settings, only: myprec
! This module stores some properties of the grid transformation (GridTrafo) at the grid points

   real(myprec), pointer :: GridOrientationXatE(:,:)
   real(myprec), pointer :: GridOrientationYatE(:,:)
   real(myprec), pointer :: GridOrientationXatN(:,:)
   real(myprec), pointer :: GridOrientationYatN(:,:)
   real(myprec), pointer :: GridOrientationXatC(:,:)
   real(myprec), pointer :: GridOrientationYatC(:,:)

   real(myprec), pointer :: dVc(:), dVe(:), dVn(:)

contains

   subroutine SetGridOri()
   use Settings,  only: myprec
   use Util,      only: error_exit
   use Inputs,    only: GridFunctions
   use GridTrafo, only: GridOrientation
   use Inputs,    only: m,n
   implicit none
      real(myprec) :: x,y, dxdxi, dydxi, dxdeta, dydeta
      real(myprec) :: Ax, Ay
      integer      :: ix, iy
      integer      :: ierr

      allocate(GridOrientationXatE(0:m-1,0:n-1), GridOrientationYatE(0:m-1,0:n-1), &
               GridOrientationXatN(0:m-1,0:n-1), GridOrientationYatN(0:m-1,0:n-1), &
               GridOrientationXatC(0:m-1,0:n-1), GridOrientationYatC(0:m-1,0:n-1), stat=ierr)
      if (ierr /= 0) call error_exit('allocation error')

      do ix = 0,m-1
         do iy = 0,n-1
           call GridFunctions(real(ix+0.5,myprec),real(iy,myprec), x,y, dxdxi, dydxi, dxdeta, dydeta)
           call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
           GridOrientationXatE(ix,iy) = Ax
           GridOrientationYatE(ix,iy) = Ay

           call GridFunctions(real(ix,myprec),real(iy+0.5,myprec), x,y, dxdxi, dydxi, dxdeta, dydeta)
           call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
           GridOrientationXatN(ix,iy) = Ax
           GridOrientationYatN(ix,iy) = Ay

           call GridFunctions(real(ix,myprec),real(iy,myprec), x,y, dxdxi, dydxi, dxdeta, dydeta)
           call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
           GridOrientationXatC(ix,iy) = Ax
           GridOrientationYatC(ix,iy) = Ay
         end do
      end do
   end subroutine SetGridOri

end module GridValues

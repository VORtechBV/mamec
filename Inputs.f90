! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module Inputs
use Settings,  only: myprec
use Stencils,  only: destagger, deriv
use GalerkinBaseFunctions, only: u_scale, u_input
use GridTrafo, only: GridFunctions => GridFunctionsCos, ScaleCurv, m, n
! Process the command line options
implicit none

   integer, parameter :: eqSCALAR=1, eqLINEARGALERKIN=2, eqLINEARFD=3, eqCOMPRESSIBLE=4, eqSHALLOW=5, eqTEST=0
   integer, save      :: eqident=0

   integer            :: P             =  5
   real(myprec)       :: rel_tol       =  1d-8
   logical            :: do_correction = .true.
   logical            :: save_snapshots= .false.
   logical            :: check_answers = .false.
   character(len=100) :: filename = 'empty_filename'
   integer            :: nSpan, K

contains

   subroutine set_Inputs(main, fromLocal)
   use Util,      only: error_exit
   use Stencils,  only: SetTaylorDestagger, SetTaylorDeriv
   use Settings,  only: fid
   use Stencils,  only: SetDestagger, SetDeriv
   use GalerkinBaseFunctions, only: setUscale, scaleFunction_3_1, PrepareLegendreInterp, u_input
   implicit none

     character(len=*), intent(in), optional :: main
     logical,          intent(in), optional :: fromLocal

     integer           :: i
     character(len=32) :: arg, val
     character(len=32) :: main_nam, wav_nam, destagger_nam, deriv_nam, reltol_nam, P_nam, curv_nam, corr_nam

     main_nam      ='test'
     i = 1
     do
        call get_command_argument(i, arg)
        if (i<command_argument_count()) call get_command_argument(i+1, val)
        i = i + 2
        if (arg=='-equation') main_nam = val
        if (i>command_argument_count()) exit
     end do
     if (present(main)) main_nam = main

     if (main_nam == 'test') then
        eqident = eqTEST
     else if (main_nam == 'scalar') then
        eqident = eqSCALAR
     else if (main_nam == 'linearGalerkin' .or. main_nam == 'test_linearGalerkin') then
        eqident = eqLINEARGALERKIN
     else if (main_nam == 'linearFD') then
        eqident = eqLINEARFD
     else if (main_nam == 'compressible') then
        eqident = eqCOMPRESSIBLE
     else if (main_nam == 'shallow' .or. main_nam == 'test_linearFD') then
        eqident = eqSHALLOW
     else
        print *,'main_nam="',main_nam,'"'
        call error_exit('input error')
     end if

     print *,'initializing for ',trim(main_nam)
     wav_nam       = ' '
     destagger_nam = ' '
     deriv_nam     = ' '
     reltol_nam    = ' '
     P_nam         = ' '
     curv_nam      = ' '
     corr_nam      = ' '

     nSpan = 2
     call SetUscale(scaleFunction_3_1)
     call PrepareLegendreInterp(u_input,nSpan)
     call SetTaylorDeriv(nSpan)
     call SetTaylorDestagger(nSpan)

     i = 1
     do
        call get_command_argument(i, arg)
        if (i<command_argument_count()) call get_command_argument(i+1, val)
        i = i + 1
        if (arg=='-equation') then
           i = i + 1
        else if (arg=='-nx') then
           read(val,*,err=9999) m
           n = m
           i = i + 1
        else if (arg=='-relTol') then
           read(val,*,err=9999) rel_tol
           i = i + 1
           write(reltol_nam,'(e12.3)') rel_tol
           reltol_nam = adjustl(reltol_nam)
        else if (arg=='-integrateP') then
           read(val,*,err=9999) P
           i = i + 1
           write(P_nam,'(i0)') P
        else if (arg=='-Taylor') then
           read(val,*,err=9999) nSpan
           call PrepareLegendreInterp(u_input,nSpan)
           i = i + 1
           write(wav_nam,'(a,i0)') 'Taylor',nSpan
        else if (arg=='-Tderiv') then
           read(val,*,err=9999) nSpan
           call SetTaylorDeriv(nSpan)
           i = i + 1
           write(deriv_nam,'(a,i0)') 'Tderiv',nSpan
        else if (arg=='-Tdestagger') then
           read(val,*,err=9999) nSpan
           call SetTaylorDestagger(nSpan)
           i = i + 1
           write(destagger_nam,'(a,i0)') 'Tdestagger',nSpan
        else if (arg=='-scaleCurv') then
           read(val,*,err=9999) ScaleCurv
           i = i + 1
           write(curv_nam,'(f5.3)') ScaleCurv
        else if (arg=='-checkAnswers') then
           check_answers = val=='yes'
           if (val/='yes' .and. val/='no') goto 9999
           i = i + 1
        else if (arg=='-correction') then
           do_correction = val=='yes'
           if (val/='yes' .and. val/='no') goto 9999
           if (.not. do_correction) then
              corr_nam = 'noCor'
           else
              corr_nam = ' '
           end if
           i = i + 1
        else if (arg=='-save_snapshots') then
           save_snapshots = val=='yes'
           if (val/='yes' .and. val/='no') goto 9999
           i = i + 1
        else
           print *,'Unrecognized argument ',arg
           stop 'input error'
        end if
        if (i>command_argument_count()) exit
     end do

     if (abs(ScaleCurv)>1e-10 .and. .not. do_correction) then
        print *,'###########################################################'
        print *,'WARNING: skipping correction in interpolation of velocities.'
        print *,'         results will not be momentum-conserving'
        print *,'###########################################################'
     end if

     write(filename,'(a,i3.3)') 'outfiles/'//trim(main_nam)//'.',m
     if (present(fromLocal)) then
        if (fromLocal) write(filename,'(a,i3.3)') 'errlocal/'//trim(main_nam)//'.',m
     end if

     if (wav_nam /= ' ') then
        filename = trim(filename)//'.'//trim(wav_nam)
     else
        wav_nam = 'not set'
     end if
     if (destagger_nam /= ' ' ) then
        filename = trim(filename)//'.'//trim(destagger_nam)
     else
        destagger_nam = 'not set'
     end if
     if (deriv_nam  /= ' ') then
        filename = trim(filename)//'.'//trim(deriv_nam)
     else
        deriv_nam = 'not set'
     end if
     if ( reltol_nam  /= ' ') then
        filename = trim(filename)//'.reltol'//trim(reltol_nam)
     else
        write(reltol_nam,'(e12.3)') rel_tol
     end if
     if (P_nam /= ' ') then
        filename = trim(filename)//'.P'//trim(P_nam)
     else
        write(P_nam,'(i0)') P
     end if
     if (curv_nam /= ' ') then
        filename = trim(filename)//'.curv'//trim(curv_nam)
     else
        write(curv_nam,'(f5.3)') ScaleCurv
     end if
     if (corr_nam /= ' ') then
        filename = trim(filename)//'.'//trim(corr_nam)
        write(corr_nam,'(i0)') P
     else
        corr_nam = 'Corr'
     end if

     filename = trim(filename)//'.out'
     print *,'Writing to file "',trim(filename),'"'
     call system('mkdir -p outfiles')
     call system('rm -f '//trim(filename))
     open(fid,FILE=filename,position="append")

     write(fid,'(a)')    'Solutions for '//trim(main_nam)
     write(fid,'(a,i0)') '   m         = ',m
     if (eqident == eqSCALAR .or. eqident == eqLINEARGALERKIN) then
        write(fid,'(a)')    '   wavelet   = "'//trim(wav_nam)//'"'
        write(fid,'(a)')    '   P         = "'//trim(P_nam)//'"'
     else
        write(fid,'(a)')    '   destagger = "'//trim(destagger_nam)//'"'
        write(fid,'(a)')    '   deriv     = "'//trim(deriv_nam)//'"'
     end if
     write(fid,'(a)')    '   reltol    = "'//trim(adjustl(reltol_nam))//'"'
     write(fid,'(a)')    '   ScaleCurv = "'//trim(curv_nam)//'"'
     write(fid,'(a)')    '   correction= "'//trim(corr_nam)//'"'
     write(fid,'(a)')    ' '
     close(fid)

     if (eqident == eqSCALAR .or. eqident == eqLINEARGALERKIN) then
        nSpan = ubound(u_input,2)+1
        K     = ubound(u_input,1)
     end if

     return
9999 continue
     print *,'error reading command line argument '//trim(arg)//' = '//trim(val)
     call error_exit('error')

   end subroutine set_Inputs
end module Inputs

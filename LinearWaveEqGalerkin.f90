! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module LinearWaveEqGalerkin
use Settings, only: myprec
! Evaluate the operators DIV and GRAD using the pre-calculated coefficients
! calculated in CalcMatrices_linearGalerkin.
implicit none

   ! Work arrays: globally allocated and used when needed.
   ! After a subroutine call is finished, their values are no longer relevant.
   ! The purpose of their global allocation is simply to avoid allocating and deallocating them
   real(myprec), pointer :: valGradX(:,:), valGradY(:,:), valDivX(:,:), valDivY(:,:)

contains

  subroutine WriteMatrices()
  use OutFile,    only: WriteDiagMatrix
  use GridValues, only: dVe, dVn, dVc
  use Inputs,     only: m,n, nSpan
  implicit none

      integer            :: irow, xi_row, eta_row, ia, icol
      integer, parameter :: fid=14
      integer            :: iaStart, iaEnd
      character(len=100) :: fname
      character(len=150) :: root

      iaStart = 0
      iaEnd   = (4*nSpan)*(4*nSpan-1)-1

      root = 'GradX'
      print *,'Writing matrix ' // trim(root)
      write(fname,'(a,i0,a,i0,a)') trim(root) // '_',m,'x',n,'.asc'
      open(fid,file=fname)
      do irow = 0,n*m-1
         xi_row   = mod(irow,m)
         eta_row  = (irow)/m

         do ia = iaStart,iaEnd
            icol =       modulo(xi_row  - 2*nSpan+1 + mod(ia,4*nSpan), m) &
                   + m * modulo(eta_row - 2*nSpan+1 + ia/(4*nSpan),    n)

            write(fid,'(i10,i10,e20.12)') irow+1,icol+1,valGradX(irow,ia)
         end do
      end do
      close(fid)

      root = 'GradY'
      print *,'Writing matrix ' // trim(root)
      write(fname,'(a,i0,a,i0,a)') trim(root) // '_',m,'x',n,'.asc'
      open(fid,file=fname)
      do irow = 0,n*m-1
         xi_row   = mod(irow,m)
         eta_row  = (irow)/m
         do ia = iaStart,iaEnd
            icol =       modulo(xi_row  - 2*nSpan+1 + ia/(4*nSpan),    m) &
                   + m * modulo(eta_row - 2*nSpan+1 + mod(ia,4*nSpan), n)
            write(fid,'(i10,i10,e20.12)') irow+1,icol+1,valGradY(irow,ia)
         end do
      end do
      close(fid)

      root = 'DivX'
      print *,'Writing matrix ' // trim(root)
      write(fname,'(a,i0,a,i0,a)') trim(root) // '_',m,'x',n,'.asc'
      open(fid,file=fname)
      do irow = 0,n*m-1
         xi_row   = mod(irow,m)
         eta_row  = (irow)/m

         do ia = iaStart,iaEnd
            icol =       modulo(xi_row  + 2*nSpan-1 - mod(ia,4*nSpan), m) &
                   + m * modulo(eta_row + 2*nSpan-1 - ia/(4*nSpan),    n)
            write(fid,'(i10,i10,e20.12)') irow+1,icol+1,valDivX(irow,ia)
         end do
      end do
      close(fid)

      root = 'DivY'
      print *,'Writing matrix ' // trim(root)
      write(fname,'(a,i0,a,i0,a)') trim(root) // '_',m,'x',n,'.asc'
      open(fid,file=fname)
      do irow = 0,n*m-1
         xi_row   = mod(irow,m)
         eta_row  = (irow)/m

         do ia = iaStart,iaEnd
            icol =       modulo(xi_row  + 2*nSpan-1 - ia/(4*nSpan),    m) &
                   + m * modulo(eta_row + 2*nSpan-1 - mod(ia,4*nSpan), n)
            write(fid,'(i10,i10,e20.12)') irow+1,icol+1,valDivY(irow,ia)
         end do
      end do
      close(fid)
      call WriteDiagMatrix(dVc,'dVc')
      call WriteDiagMatrix(dVe,'dVe')
      call WriteDiagMatrix(dVn,'dVn')
  end subroutine WriteMatrices

   subroutine ApplyGradient(f,GradX,GradY)
   use Inputs, only: m,n, nSpan
   implicit none
      real(myprec), intent(in)  :: f(0:)
      real(myprec), intent(out) :: GradX(0:), GradY(0:)

      integer      :: irow, xi_row, eta_row, ia, icol
      real(myprec) :: Gx, Gy
      integer      :: iaStart, iaEnd

      iaStart = 0
      iaEnd   = (4*nSpan)*(4*nSpan-1)-1

!$OMP PARALLEL DO PRIVATE(xi_row, eta_row, Gx, Gy, ia, icol)
      do irow = 0,n*m-1
         xi_row   = mod(irow,m)
         eta_row  = (irow)/m

         Gx=0
         Gy=0
         do ia = iaStart,iaEnd
            icol =       modulo(xi_row  - 2*nSpan+1 + mod(ia,4*nSpan), m) &
                   + m * modulo(eta_row - 2*nSpan+1 + ia/(4*nSpan),    n)
            Gx = Gx + valGradX(irow,ia) * f(icol)

            icol =       modulo(xi_row  - 2*nSpan+1 + ia/(4*nSpan),    m) &
                   + m * modulo(eta_row - 2*nSpan+1 + mod(ia,4*nSpan), n)
            Gy = Gy + valGradY(irow,ia) * f(icol)
         end do
         GradX(irow) = Gx
         GradY(irow) = Gy
      end do
!$OMP END PARALLEL DO
   end subroutine ApplyGradient

   subroutine ApplyDivergence(vx,vy,DivV)
   use Inputs, only: m,n, nSpan
   implicit none
      real(myprec), intent(in)  :: vx(0:), vy(0:)
      real(myprec), intent(out) :: DivV(0:)

      integer      :: irow, xi_row, eta_row, ia, icol
      real(myprec) :: Div
      integer      :: iaStart, iaEnd

      iaStart = 0
      iaEnd   = (4*nSpan)*(4*nSpan-1)-1

!$OMP PARALLEL DO PRIVATE(xi_row, eta_row, Div, ia, icol)
      do irow = 0,n*m-1
         xi_row   = mod(irow,m)
         eta_row  = (irow)/m

         Div=0
         do ia = iaStart,iaEnd
            icol =       modulo(xi_row  + 2*nSpan-1 - mod(ia,4*nSpan), m) &
                   + m * modulo(eta_row + 2*nSpan-1 - ia/(4*nSpan),    n)

            Div = Div + valDivX(irow,ia) * vx(icol)

            icol =       modulo(xi_row  + 2*nSpan-1 - ia/(4*nSpan),    m) &
                   + m * modulo(eta_row + 2*nSpan-1 - mod(ia,4*nSpan), n)
            Div = Div + valDivY(irow,ia) * vy(icol)
         end do
         DivV(irow) = Div
      end do
!$OMP END PARALLEL DO

   end subroutine ApplyDivergence

end module LinearWaveEqGalerkin

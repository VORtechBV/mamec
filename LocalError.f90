! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
Program LocalError
!
! Calculate the local discretization error for one of various wave equations:
!  + Scalar-wave equation:
!     ddp/dtt = LAPLACE p
!  + Linear-wave equation:
!     d rho/dt + rho0 DIV v = 0,      dv/dt + 1/rho0 GRAD p = 0                    p   = c^2 rho
!    for simplicity, this program only supports rho0=1 and c=1.
!  + Compressible-wave equation:
!     d rho/dt +  DIV rho v = 0,      dv/dt +  1/rho GRAD p = 0,                   rho = R(p)
!    An exact solution is provided for R(p) = p/c^2
!  + Shallow-water equation:
!     d rho/dt +  DIV rho v = 0,      d rho v/dt + DIV (rho v x v) + GRAD p = 0,   rho = R(p)
!    An exact solution is provided for R(p) = sqrt(2 p/g)
!
use localReferences,  only: GetReferences
use SwitchToEquation, only: Initialize, WaveFun
use Inputs,           only: save_snapshots, filename, check_answers
use Settings,         only: myprec, fid
use Util,             only: error_exit
use OutFile,          only: SaveGrid
use OutFile,          only: WriteSolution
use Exact1D,          only: CompareLocal, MaxTime, ExactSolution, ExactdSolutiondt
implicit none

    real(myprec)          :: ref(10)
    logical               :: found
    real(myprec), pointer :: f(:), dydt(:), y(:)
    real(myprec)          :: err(3,2)
    real(myprec)          :: tmax, time
    integer, parameter    :: ntout=10
    integer               :: ierr
    integer               :: i

    y    => null()
    dydt => null()

    call Initialize(fromLocal=.true.)
    if (save_snapshots) call SaveGrid()

    tmax = MaxTime()
    do i = 0,ntout
       time = i * tmax / ntout
       call ExactSolution(    time, y   )
       call ExactdSolutiondt( time, dydt)
       if (i==0) then  ! because now we have the bounds of y
          allocate(f(lbound(y,1):ubound(y,1)),stat=ierr)
          if (ierr/=0) call error_exit('allocation error')
       end if
       call WaveFun(f, time, y)
       err = CompareLocal(f,dydt)
       if (save_snapshots) then
          call WriteSolution(f, time, 'ddt_discrete')
          call WriteSolution(dydt, time, 'ddt_exact')
       end if
       open(fid,FILE=filename,position="append")
       write(fid,'(a,f0.3,1p,10(a,e11.3))') 't = ',time,' relerr drhodt=',err(1,1),' dvdt=(',err(2,1),',',err(3,1),')'
       write(fid,'(a,f0.3,1p,10(a,e11.3))') 't = ',time,' abserr drhodt=',err(1,2),' dvdt=(',err(2,2),',',err(3,2),')'
       write(fid,*)
       close(fid)
       write(*,'(a,f0.3,1p,10(a,e11.3))') 't = ',time,' relerr drhodt=',err(1,1),' dvdt=(',err(2,1),',',err(3,1),')'
       write(*,'(a,f0.3,1p,10(a,e11.3))') 't = ',time,' abserr drhodt=',err(1,2),' dvdt=(',err(2,2),',',err(3,2),')'
       write(*,*)
       if (check_answers) then
          call GetReferences(i, ref, found)
          if (.not. found) call error_exit('references not found')
          if (abs(err(1,1) - ref(2)) > 0.01*max(1e-12,abs(err(1,1))) .or. &
              abs(err(2,1) - ref(3)) > 0.01*max(1e-12,abs(err(2,1))) .or. &
              abs(err(3,1) - ref(4)) > 0.01*max(1e-12,abs(err(3,1))) .or. &
              abs(err(1,2) - ref(5)) > 0.01*max(1e-12,abs(err(1,2))) .or. &
              abs(err(2,2) - ref(6)) > 0.01*max(1e-12,abs(err(2,2))) .or. &
              abs(err(3,2) - ref(7)) > 0.01*max(1e-12,abs(err(3,2))) ) call error_exit('answers differ from references')
          print *,'results correspond to references'
       end if
    end do

end Program LocalError

---
title: "Interview: Generations about racism"
author: Volkskrant
date: May 21, 2021
geometry: margin=2cm
output: pdf_document
header-includes: |
    \usepackage{xcolor}
---
In this blog, we give an explanation of our recently-published research on symmetry-preserving discretizations
that conserve physical quantities, and its applications.

First, we introduce to you several different fields of applications.

We start with turbulent flows (link naar wiki). 
Turbulent flows are present everywhere: in the air, in water, but also in many industrial applications (https://blog.spatial.com/cfd-modeling-applications).
These flows are described mathematically by the Navier-Stokes equations.
The most-used, and most-flexible tool to calculate the corresponding turbulent flows, is so-called direct numerical simulation 
(DNS)\footnote{Tot voor kort werd DNS volgens mij niet zoveel gedaan, omdat het te duur was. Hoe is dat nu?}
In DNS, the Navier-Stokes equations are solved without the use of a turbulence model: very fine grids,
and very small time steps are needed to find an accurate approximation of the solution.
It is possible that the required grid is too fine: the corresponding model cannot be calculated with a current (super) computer. 
Taking a coarser grid will certainly have consequences for the accuracy of the approximation.
However, if the used method conserves the mass, momentum and
energy of the system, then the approximation will keep 
stable\footnote{De mate van turbulentie wordt bepaald door het energie-evenwicht: de energie in de hoofdstroom komt in de grotere draaikolken terecht en dan steeds in kleinere totdat de kleinste draaikolken aan diffusie/viscositeit ten onder gaan en de energie wordt omgezet in warmte. Dat evenwicht kun je niet goed krijgen als je instabiel bent (logisch) maar ook niet als je te stabiel bent, want dan is er numerieke of artificiele diffusie/viscositeit}, even when shock waves are present in the flow. 

Links:

+  [\color{blue}Formulation of Kinetic Energy Preserving Conservative Schemes for Gas Dynamics and Direct Numerical Simulation of One-Dimensional Viscous Compressible Flow in a Shock Tube Using Entropy and Kinetic Energy Preserving Schemes
](https://www.researchgate.net/publication/220396022_Formulation_of_Kinetic_Energy_Preserving_Conservative_Schemes_for_Gas_Dynamics_and_Direct_Numerical_Simulation_of_One-Dimensional_Viscous_Compressible_Flow_in_a_Shock_Tube_Using_Entropy_and_Kinetic_En) 
+ [\color{blue}A direct numerical simulation code for incompressible turbulent boundary layers](https://torroja.dmt.upm.es/pubs/2007/Simens_etal_ETSIA_MF071.pdf)

Energy conservation is also very important in predicting earth quakes. The seismic energy of an earth quake is very important
for the amount of damage. Calculations that apply seismic energy are more useful than the ones that use seismic moments.
(link: https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2020JB020186)
Also in the oil and gas industry, energy conservation is important, for instance in exotherm processes in oil wells. 
(link:  https://www.scirp.org/journal/paperinformation.aspx?paperid=71179)
Energy conservation also plays a role in the corresponding reservoir simulations.
(link: https://www.spe-aberdeen.org/wp-content/uploads/2018/06/Devex-2018-Introduction-to-Reservoir-Simulation.pdf)
\footnote{Waarom wordt de seismic research niet genoemd? Daar zijn wij mee bezig! Of mag dat niet?}

A last example in which energy conservation is important, is calculations involving shallow-water equations.
See for example http://empslocal.ex.ac.uk/people/staff/gv219/ecmm719/ess-ecmm719.pdf, and https://en.wikipedia.org/wiki/Shallow_water_equations
Here, we would like to also mention the work of Guus Stelling, who invented a method that conserves either momentum, or energy locally, by choice. 
https://onlinelibrary.wiley.com/doi/abs/10.1002/fld.1722

As we have seen above, energy conservation is very important to find accurate approximations of flow phenomena. 
If energy (or mass, or momentum) are not conserved, then simulations can fade away or become unstable, see for example Figure .
In order to obtain conservation of mass, momentum and energy, we need special numerical methods. 

Let us now look at the construction of such a method. 
For each real-world flow phenomenon (such as in the examples above), a mathematical formulation can be made. This model contains differential 
operators that describe how the flow evolves in time.
Usually, the flow state can be described by an infinite number of values that belong to the flow. In order to apply computer simulations to it, 
the number of values should be reduced to some large finite number. We call this new formulation a discretization of the continuous model.

In the resulting discrete model of the flow phenomenon, the (continuous)
differential operators have been replaced by (discrete) difference operators. 
Unfortunately, not all properties of the differential operators are automatically inherited by their discrete approximations. 
For example, symmetry and positiveness\footnote{Deze woorden betekenen heel iets anders dan de lezer zou kunnen vermoeden! Ik denk niet dat je het moet uitleggen, maar misschien wel benoemen en een link naar ergens waar symmetry/self-adjointness en positive (definite)ness uitgelegd worden.} may be lost in the discretization process, and mass, momentum, and energy may not be conserved.

Symmetry-preserving methods, or mimetic methods, aim to preserve certain properties of a continuous operator in its discrete counterpart. 
The use of symmetry-preserving discretizations makes it possible to construct discrete models which allow all the manipulations
needed to prove stability and (discrete) conservation in the same way they were proven in the original continuous model. 
In our paper, we present a new finite-difference symmetry-preserving discretization of arbitrary order on structured curvilinear staggered grids.

The novelty of our work is that it combines 

+ arbitrary order of convergence (related to accuracy), 
+ orthogonal and non-orthogonal structured curvilinear staggered meshes (see Figure ), and 
+ the applicability to a wide variety phenomena, such as the shallow-water equations, while conserving mass, momentum and energy.

The existing models in the literature are not at the same time applicable for arbitrary discretization orders, sophisticated operators, 
and curvilinear staggered grids.
Or, they are not able to conserve mass, momentum, and energy together.
During the ECCOMAS conference (link), we heard about another trick that can be useful to obtain similar results, but this is not yet
investigated. 

Our new method is obtained by writing the discrete equations in a special form. Here, we separate the easy (linear) parts from the difficult
(non-linear) parts. These parts of the equation should all satisfy certain properties (related to so-called null spaces, symmetry, and self-adjointness). 
Next to the conservation of mass, momentum and energy, the resulting method also yields an adjoint operator that is meaningful. 
This has advantages with respect to accuracy and stability when optimizing the approximation.

In the paper, we describe the corresponding discretization method that satisfies all requirements. 
Some of the techniques are already succesfully applied in projects of our 
customers. For example, the research was inspired by calculations that are done by Rijkswaterstaat (link). 
At this moment, we are working on a time-integration method and solver that conserve mass, momentum and energy. 

At VORtech, we have several people that apply these kind of complex mathematical techniques in their daily work. 
Although we are mostly focused on direct applications, still, there is some room for challenging research. 
In this way, we are also known in the academic world!


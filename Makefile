FOPTIONS_gfortran_debug=-O0 -Wall -g  -fopenmp -ffree-line-length-none -fcheck=all -Werror
FOPTIONS_ifort_debug=-O0 -warn all -g  -qopenmp -check all
FOPTIONS_gfortran_release=-O3 -Wall -g  -fopenmp -ffree-line-length-none -Werror
FOPTIONS_ifort_release=-O3 -warn all -g  -qopenmp

OPKDA_FOPTIONS_gfortran_debug=-O0 -w -g  -fopenmp -ffree-line-length-none -fcheck=all 
OPKDA_FOPTIONS_ifort_debug=-O0 -g  -qopenmp -check all
OPKDA_FOPTIONS_gfortran_release=-O3 -w -g  -fopenmp -ffree-line-length-none 
OPKDA_FOPTIONS_ifort_release=-O3 -g  -qopenmp

MAKE_TYPE ?= debug

FCompiler ?= gfortran

FOPTIONS=$(FOPTIONS_$(FCompiler)_$(MAKE_TYPE))
OPKDA_FOPTIONS=$(OPKDA_FOPTIONS_$(FCompiler)_$(MAKE_TYPE))

OBJS=CalcMatrices_Scalar_$(MAKE_TYPE).o           \
     Settings_$(MAKE_TYPE).o         Util_$(MAKE_TYPE).o                 GalerkinBaseFunctions_$(MAKE_TYPE).o                    \
     Inputs_$(MAKE_TYPE).o           GridTrafo_$(MAKE_TYPE).o            CalcMatrices_LinearGalerkin_$(MAKE_TYPE).o \
     Destagger_$(MAKE_TYPE).o        CalcMatrices_FD_$(MAKE_TYPE).o      \
     FDApplyOperators_$(MAKE_TYPE).o LinearWaveEqGalerkin_$(MAKE_TYPE).o Exact1D_$(MAKE_TYPE).o                     \
     StateEq_$(MAKE_TYPE).o          Checks_$(MAKE_TYPE).o                      \
     TestFunctions_$(MAKE_TYPE).o    GridValues_$(MAKE_TYPE).o           Tests_$(MAKE_TYPE).o                       \
     OutFile_$(MAKE_TYPE).o              SwitchToEquation_$(MAKE_TYPE).o reference_$(MAKE_TYPE).o           \
     lsode_wrapper_$(MAKE_TYPE).o    WaveEquations_$(MAKE_TYPE).o        CalculateTotals_$(MAKE_TYPE).o

MAINS=Simulate_$(MAKE_TYPE).exe        LocalError_$(MAKE_TYPE).exe \
      test_FD_$(MAKE_TYPE).exe         test_FD_accuracy_$(MAKE_TYPE).exe  \
      test_linearGalerkin_accuracy_$(MAKE_TYPE).exe test_linearGalerkin_$(MAKE_TYPE).exe     

all:	$(OBJS) $(MAINS)

depend:	
	./get_dependencies.sh > depends.make
clean:	
	rm -f *.o *.mod *.exe *_genmod.f90 ../lsode/*.o 

test:	unit local

localref:	all
	n_error=0; \
	./LocalError_$(MAKE_TYPE).exe -equation scalar -Taylor 3 -nx 23 -checkAnswers no; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./LocalError_$(MAKE_TYPE).exe -equation linearGalerkin -Taylor 3 -nx 25 -checkAnswers no ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./LocalError_$(MAKE_TYPE).exe -equation linearFD -Tderiv 3 -Tdestagger 3 -nx 27 -checkAnswers no ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./LocalError_$(MAKE_TYPE).exe -equation compressible -Tderiv 3 -Tdestagger 3 -nx 29 -checkAnswers no ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./LocalError_$(MAKE_TYPE).exe -equation shallow -Tderiv 3 -Tdestagger 3 -nx 31 -checkAnswers no ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`

local:	all
	n_error=0; \
	./LocalError_$(MAKE_TYPE).exe -equation scalar -Taylor 3 -nx 23 -checkAnswers yes; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./LocalError_$(MAKE_TYPE).exe -equation linearGalerkin -Taylor 3 -nx 25 -checkAnswers yes ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./LocalError_$(MAKE_TYPE).exe -equation linearFD -Tderiv 3 -Tdestagger 3 -nx 27 -checkAnswers yes ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./LocalError_$(MAKE_TYPE).exe -equation compressible -Tderiv 3 -Tdestagger 3 -nx 29 -checkAnswers yes ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./LocalError_$(MAKE_TYPE).exe -equation shallow -Tderiv 3 -Tdestagger 3 -nx 31 -checkAnswers yes ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	echo $$n_error | awk ' \
	    { \
		if ($$1>0) {print "############### "$$1" tests failed ###############";} \
		else      {print "Tests finished successfully";} \
	    }' ; \
	exit $$n_error 

unit:   all
	n_error=0; \
	./test_FD_$(MAKE_TYPE).exe    -nx 20 -Tderiv 5 -Tdestagger 5; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./test_FD_$(MAKE_TYPE).exe    -nx 40 -correction no -scaleCurv 0 -Tderiv 3 -Tdestagger 3; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./test_FD_accuracy_$(MAKE_TYPE).exe    -nx 100 -Tderiv 5 -Tdestagger 5; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./test_FD_accuracy_$(MAKE_TYPE).exe    -nx 100 -correction no -scaleCurv 0 -Tderiv 4 -Tdestagger 4; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./test_linearGalerkin_$(MAKE_TYPE).exe -nx 20 -Taylor 3 ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	./test_linearGalerkin_accuracy_$(MAKE_TYPE).exe -nx 35 -Taylor 3 ; \
	n_error=`echo $$? $$n_error | awk '{print $$1+$$2}'`; \
	echo $$n_error | awk ' \
	    { \
		if ($$1>0) {print "############### "$$1" tests failed ###############";} \
		else      {print "Tests finished successfully";} \
	    }' ; \
	exit $$n_error 

lsode_wrapper_$(MAKE_TYPE).o:	lsode_wrapper.f90
	@if [ ! -f ../lsode/opkda1.f ]; then \
		echo '  ***************************************************************************'; \
		echo '  *   This program requires the files opkda1.f, opkda2.f                    *'; \
		echo '  *   and opkdmain.f from ODEPACK.                                          *'; \
		echo '  *   You can download ODEPACK from                                         *'; \
		echo '  *      https://computation.llnl.gov/casc/odepack/download/opkd_agree.html *'; \
		echo '  *   then place the downloaded files in the directory ../lsode/            *'; \
		echo '  ***************************************************************************'; \
		exit 1; \
	fi;
	$(FCompiler) -c $(OPKDA_FOPTIONS)  -o ../lsode/opkda1_$(MAKE_TYPE).o ../lsode/opkda1.f
	$(FCompiler) -c $(OPKDA_FOPTIONS)  -o ../lsode/opkda2_$(MAKE_TYPE).o ../lsode/opkda2.f
	$(FCompiler) -c $(OPKDA_FOPTIONS)  -o ../lsode/opkdmain_$(MAKE_TYPE).o ../lsode/opkdmain.f
	$(FCompiler) -c $(OPKDA_FOPTIONS) $(<) -o $(<:.f90=_$(MAKE_TYPE).o)

%_$(MAKE_TYPE).o:	%.f90
	$(FCompiler) -c $(FOPTIONS) $(<) -o $(<:.f90=_$(MAKE_TYPE).o)

%.exe:	%.o $(OBJS)
	$(FCompiler) $(FOPTIONS) $(OBJS) -o $(<:.o=.exe) $(<:_$(MAKE_TYPE)=) \
	 ../lsode/opkda1_$(MAKE_TYPE).o \
	 ../lsode/opkda2_$(MAKE_TYPE).o \
	 ../lsode/opkdmain_$(MAKE_TYPE).o



include depends.make 


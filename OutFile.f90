! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module OutFile
! Write results to output file.
contains

  subroutine WriteSubSolution(field,field_name,suffix,time_str)
  use Settings, only: myprec, fid
  use Inputs,   only: m,n
  implicit none
     real(myprec), pointer        :: field(:)
     character(len=*), intent(in) :: field_name, suffix, time_str

     character(len=100) :: file_name
     integer            :: irow

     write(file_name,'(a,i0,a,i0,a)') field_name//'_'//suffix,m,'x',n,'_'//trim(time_str)//'.asc'
     open(fid,file=file_name)
     do irow = 0,m*n-1
        write(fid,'(e19.11)') field(irow)
     end do
     close(fid)
     print *,'Written file ' // trim(file_name)

  end subroutine WriteSubSolution

  subroutine WriteSolution(y,time, suffix)
  use Util, only: error_exit
  use Settings, only: myprec
  use Inputs,   only: m,n
  implicit none

     real(myprec),     target,   intent(in) :: y(0:)
     real(myprec),               intent(in) :: time
     character(len=*), optional, intent(in) :: suffix

     character(len=100)    :: suf, tstr
     real(myprec), pointer :: rho(:), vx(:), vy(:)
     real(myprec), pointer :: p(:), dpdt(:)

     suf = ' '
     if (present(suffix)) suf = trim(suffix)//'_'
     write(tstr,'(f10.6)') time
     tstr = adjustl(tstr)

     if (size(y,1)==3*m*n) then
        rho(0:m*n-1) => y(    0:  m*n-1)
        vx( 0:m*n-1) => y(  m*n:2*m*n-1)
        vy( 0:m*n-1) => y(2*m*n:3*m*n-1)

        call WriteSubSolution(rho,'rho',trim(suf),trim(tstr))
        call WriteSubSolution(vx, 'vx', trim(suf),trim(tstr))
        call WriteSubSolution(vy, 'vy', trim(suf),trim(tstr))
     else if (size(y,1)==2*m*n) then
        p(    0:m*n-1) => y(       0:  m*n-1)
        dpdt( 0:m*n-1) => y(  m*n:2*m*n-1)

        call WriteSubSolution(p,   'p',    trim(suf),trim(tstr))
        call WriteSubSolution(dpdt,'dpdt', trim(suf),trim(tstr))
     else
        call error_exit('illegal dimension')
     end if
  end subroutine writeSolution

  subroutine WriteDiagMatrix(vecQ,root)
  use Settings, only: myprec, fid
  use Inputs,   only: m,n
  implicit none
     real(myprec), pointer, intent(out) :: vecQ(:)
     character(len=*),     intent(in)   :: root

     character(len=100) :: fname
     integer            :: irow

     print *,'Writing diagonal matrix ' // root
     write(fname,'(a,i0,a,i0,a)') root // 'a_',m,'x',n,'.asc'
     open(fid,file=fname)
     do irow = lbound(vecQ,1),ubound(vecQ,1)
        write(fid,'(i10,e19.11)') irow,vecQ(irow)
     end do
     close(fid)
     print *,'Written matrix ' // root
  end subroutine writeDiagMatrix

  subroutine WriteMatrix(valA,root)
  use Settings, only: myprec, fid
  use Inputs,   only: m,n
  implicit none
     real(myprec), pointer, intent(out) :: valA(:,:)
     character(len=*),      intent(in)  :: root

     character(len=100) :: fname
     integer            :: eta_col, xi_col, eta_row, xi_row, irow, icol
     integer            :: ia, nSpan

     nSpan = nint((sqrt(real(size(valA,2))+1))/4)

     print *,'Writing matrix ' // root
     write(fname,'(a,i0,a,i0,a)') root // 'a_',m,'x',n,'.asc'
     open(fid,file=fname)
     do irow = 1,m*n
        xi_row = mod(irow-1,m)
        eta_row  = (irow-1)/m
        do ia = 1,(4*nSpan-1) * (4*nSpan-1)
           eta_col  = modulo(eta_row + mod(ia-1,4*nSpan-1)+1-2*nSpan,m)
           xi_col   = modulo(xi_row + (ia-1)/(4*nSpan-1)+1-2*nSpan,n)
           icol     = m*eta_col + xi_col + 1
           write(fid,'(2i10,e19.11)') irow,icol,valA(irow,ia)
        end do
     end do
     close(fid)
     print *,'Written matrix ' // root
  end subroutine writeMatrix

  subroutine saveGrid()
  use Inputs,   only: GridFunctions, m, n
  use Settings, only: myprec, fid
  implicit none
      real(myprec)       :: xi, eta
      real(myprec)       :: x,y, dxdxi, dydxi, dxdeta, dydeta
      integer            :: i
      character(len=100) :: fname

      write(fname,'(10(a,i0))') 'grid_',m,'x',n,'.asc'
      open(fid,file=fname)
      do i = 0,n*m-1
         eta  = i/m
         xi = mod(i,m)
         call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
         write(fid,'(2(e19.11,a))') x,' ',y
      end do
      close(fid)
      write(fname,'(10(a,i0))') 'gridX_',m,'x',n,'.asc'
      open(fid,file=fname)
      do i = 0,n*m-1
         eta  = i/m
         xi = mod(i,m)
         call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
         write(fid,'(2(e19.11,a))') x,' ',y
      end do
      close(fid)
      write(fname,'(10(a,i0))') 'gridY_',m,'x',n,'.asc'
      open(fid,file=fname)
      do i = 0,n*m-1
         eta  = i/m
         xi = mod(i,m)
         call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
         write(fid,'(2(e19.11,a))') x,' ',y
      end do
      close(fid)
  end subroutine saveGrid
end module OutFile

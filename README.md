---
geometry: margin=2cm
---
# Symmetry-preserving finite-difference discretizations of arbitrary order on structured curvilinear staggered grids

## Introduction

This directory contains the source files for the symmetry-preserving numerical simulation of the following 5 models:

1. Scalar-wave equation;
2. Linear-wave equations (Galerkin-type discretization); 
3. Linear-wave equations (Finite-difference discretization); 
4. Compressible-wave equations; 
5. Shallow-water equations.

## Installation

The installation expects one of the compilers gfortran or ifort to be running and functional. The compiler is called for 
compiling as well as linking.  We used gfortran version 7.3.0 and ifort 17.0.1, but there is no reason why the software 
should not run with other compilers.  We only tested on the OS Ubuntu 18.04, but again, we expect no problems on other 
OSs. Possibly, some changes will be needed to run on Windows (e.g. because it uses a different file separator token).

Any one of the following four commands will compile and link the code:
```
   MAKE_TYPE=release FCompiler=gfortran make -j
   MAKE_TYPE=debug   FCompiler=gfortran make -j 
   MAKE_TYPE=release FCompiler=ifort    make -j
   MAKE_TYPE=debug   FCompiler=ifort    make -j 
```
When running make for the first time, you will be prompted with the following message:
```
  ***************************************************************************
  *   This program requires the files opkda1.f, opkda2.f                    *
  *   and opkdmain.f from ODEPACK.                                          *
  *   You can download ODEPACK from                                         *
  *      https://computation.llnl.gov/casc/odepack/download/opkd_agree.html *
  *   then place the downloaded files in the directory ../lsode/            *
  ***************************************************************************
```
Compiling will be stopped at this message until you download the files 
__opkda1.f__, __opkda2.f__ and __opkdmain.f__ from ODEPACK at the given address, and copy the files to the directory indicated by the message.
After that, compiling results in the executable files
```
   LocalError_release.exe
   Simulate_release.exe
   test_FD_accuracy_release.exe  
   test_FD_release.exe
   test_linearGalerkin_accuracy_release.exe  
   test_linearGalerkin_release.exe
```
or the debug-versions of the same executables.

## Testing

The software can be tested by running 
```
   make unit
   make local
```
Both tests should end with the message
```
   Tests finished successfully
```

## Running the software
Simulations can be run using the executable __Simulate\_release.exe__. It requires the command-line option __-equation (string)__, for which 
the options __scalar__, __linearGalerkin__, __linearFD__, __compressible__ and __shallow__ are available. All other options have defaults:

+ __-nx (int=20)__ grid size (same value taken in x- and y-direction);
+ __-relTol (float=1e-8)__ relative tolerance in time integration;
+ __-scaleCurv (float=1.0)__ amount of curvature in grid lines (__0.0__: uniform grid; __1.0__: curvilinear grid used in the paper);
+ __-save\_snapshots (yes/no=no)__ save ASCII files with solution values for visualization or inspection;

For the equations that are discretized using a Galerkin-type approach (__scalar__ and __linearGalerkin__), there are the following additional options:

+ __-Taylor (int=2)__ span of Lagrange interpolation function (order of accuracy is __2 * nSpan__).

For the equations that are discretized using a finite-difference technique (__linearFD__, __compressible__ and __shallow__), there are the following additional options:

+ __-Tderiv (int=2)__ span of the stencils in numerical differentiation (order of accuracy is __2 * nSpan__);
+ __-Tdestagger (int=2)__ span of the stencils in interpolations (order of accuracy is __2 * nSpan__);

The program __LocalError\_release.exe__ does no simulation but calculates local truncation errors. To do this, it 

1. fills the field __fExact__ with samples of a known exact solution;
2. fills another field __dfdtApprox__ with approximate time derivatives by applying the discretized model equations;
3. fills a third field __dfdtExact__ with the exact time derivative of the known solution;
4. reports 2-norm and inf-norm of the difference between __dfdtApprox__ and __dfdtExact__.

The executable has the following additional option:

+ __-checkAnswers (yes/no=yes)__ compare results to reference results, stored in the file __reference.f90__. Exit with an error message if results do not correspond to references. 

The program writes its output to files in the directory __outfiles/__ (__Simulate\_release.exe__) or __outerr/__ (__LocalError\_release.exe__), and will 
produce errors if this output directory is not available for writing. The repository you have downloaded is set up to make sure the output directories exist.


## Auxiliary scripts
Some auxiliary scripts are part of this repository. They are the files __* .sh__ and __plot.py__. The scripts have (rudimentary) manuals inside them.



! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module Settings

   integer,      parameter :: NORM_TWO=2, NORM_INF=100
   integer,      parameter :: myprec = kind(1d0)
   real(myprec), parameter :: threshold = 1e-3
   integer, parameter :: fid=14
   integer, parameter :: tMASS=1,   tINTERNAL=4, tMOMENTUM_U=2, tMOMENTUM_V=3, tKINETIC=5, tENERGY=6
   integer, parameter :: tNTOTALS=6
   integer, parameter :: STDOUT = 6

end module Settings

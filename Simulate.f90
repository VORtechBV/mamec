! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
Program Simulate
!
! Simulate one of various wave equations:
!  + Scalar-wave equation:
!     ddp/dtt = LAPLACE p
!  + Linear-wave equation:
!     d rho/dt + rho0 DIV v = 0,      dv/dt + 1/rho GRAD p = 0,              rho = c^2/p
!    For simplicity, this program only supports rho0=1, c=1.
!  + Compressible-wave equation:
!     d rho/dt + DIV rho v = 0,       dv/dt + 1/rho GRAD p = 0,              rho = R(p)
!    Exact solution is available for R(p) = p/c^2
!  + Shallow-water equation:
!     drho/dt + DIV rho v = 0,        dv/dt + DIV (rho v x v) + GRAD p = 0,  rho = R(p)
!    Exact solution is available for R(p) = sqrt(2p/g)
!
use Exact1D,          only: CompareValues, CompareAndSave, CompareDontSave, InitialValues, WriteExactSolution, MaxTime
use Inputs,           only: rel_tol, save_snapshots, filename
use lsode_wrapper,    only: wrapped_dlsode
use OutFile,          only: SaveGrid, WriteSolution
use Settings,         only: myprec, fid, tMASS, tKINETIC, tMOMENTUM_U, tMOMENTUM_V, tINTERNAL, tNTOTALS, tENERGY
use SwitchToEquation, only: Initialize, CalcTotals, WaveFun
use Util,             only: error_exit
implicit none

    real(myprec), pointer   :: y0(:), yout(:,:)
    real(myprec), pointer   :: f0(:)
    real(myprec)            :: err(3)
    real(myprec)            :: Totals(tnTOTALS), Totals0(tnTOTALS), dTotals(tnTOTALS)
    real(myprec)            :: tmax
    real(myprec)            :: tout(10)
    real(myprec), parameter :: t0 = 0
    integer                 :: ierr
    integer                 :: i

    y0   => null()
    yout => null()

    call Initialize()
    call InitialValues(y0)
    Totals0 = CalcTotals(y0)

    print '(10(a,f12.8))', &
        'Total mass=',Totals0(tMASS),',  ' // &
        'momentum=',Totals0(tMOMENTUM_U),',',Totals0(tMOMENTUM_V), &
        ', energy=',Totals0(tINTERNAL),'+',Totals0(tKINETIC),'=',Totals0(tENERGY)

    tmax = MaxTime()
    do i = 1,size(tout,1)
       tout(i) = i * tmax / size(tout,1)
    end do

    if (save_snapshots) then
       allocate(f0(lbound(y0,1):ubound(y0,1)),stat=ierr)
       if (ierr/=0) call error_exit('allocation error')
       call WaveFun(f0, real(0.0,myprec), y0)
       call SaveGrid()
       call WriteExactSolution(real(0.0,myprec))
       call WriteSolution(f0, real(0.0,myprec), 'ddt')
       call WriteSolution(y0, real(0.0,myprec))
       do i = 1,size(tout,1)
          call WriteExactSolution(tout(i))
       end do
       ! Time integration
       call wrapped_dlsode(WaveFun, y0, t0, tout,yout, rel_tol = rel_tol, ProcessFunction= CompareAndSave)
    else
       ! Time integration
       call wrapped_dlsode(WaveFun, y0, t0, tout,yout, rel_tol = rel_tol, ProcessFunction= CompareDontSave)
    end if

    open(fid,FILE=filename,position="append")
    do i = 1,size(tout,1)
       err = CompareValues(yout(:,i),tout(i))
       write(fid, '(a,f5.2,10(a,f12.8))') &
            'Solution ',  tout(i),': ' // &
            'p-error = ', err(1)*100,' %,  ' // &
            'vx-error = ',err(2)*100,' %,  ' // &
            'vy-error = ',err(3)*100,' %'
    end do

    write(fid,'(a)')    ' '

    do i = 1,size(tout,1)
       Totals = CalcTotals(yout(:,i))
       dTotals = Totals0 - Totals
       write(fid,'(a,f5.2,3(1p,a,e10.2),(1p,a,e14.6),a,f10.5,a,1p,e10.2,a)') &
             'Time = ',tout(i),', Loss of ' // &
             'mass=',     100*dTotals(tMASS)      / Totals0(tMASS),' %,  ' // &
             'momentum=(',100*dTotals(tMOMENTUM_U)/ Totals0(tMOMENTUM_U),',', &
                          100*dTotals(tMOMENTUM_V)/ Totals0(tMOMENTUM_V),') %, ' // &
             ' energy=',  100*dTotals(tINTERNAL)  / Totals0(tENERGY),'+', &
                          100*dTotals(tKINETIC)   / Totals0(tENERGY),'=', &
                          100*dTotals(tENERGY)    / Totals0(tENERGY),' %'
    end do
    close(fid)
end Program Simulate

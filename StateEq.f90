! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module LinearWave
! This module supports the state equation
!     R(p) = p
! for the wave equation
!     dv/dt = -grad p
!     dp/dt = -div  v
!
! There are 1D solutions with p=v:
!     dv/dt = -dv/dx
!
! However, all functions will be expressed in terms of the
! density rho, for instance the pressure function
!     P(rho) = rho
!
use Settings, only: myprec
implicit none
  real(myprec), parameter :: scale = sqrt(real(0.03,myprec))/1.5, sqrt2 = sqrt(real(2,myprec))
contains

  function ddVel1Ddxx(x1) result(ddv1dxx)
  use GridTrafo, only: pi
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: ddv1dxx

      real(myprec) :: xi, dxi, dxi2
      real(myprec) :: y, dy, dy2

      xi   =  sin(x1*sqrt(2d0)*pi)/(sqrt(2d0)*pi)
      dxi  =  cos(x1*sqrt(2d0)*pi)
      dxi2 = -sin(x1*sqrt(2d0)*pi)*(sqrt(2d0)*pi)

      y     = - (xi**2) /(scale*scale)
      dy    = -2*xi*dxi /(scale*scale)
      dy2   = (-2*xi*dxi2 - 2*dxi**2)/(scale*scale)

      ddv1dxx = exp( y ) * (dy**2 + dy2)
  end function ddVel1Ddxx

  function dVel1Ddx(x1) result(dv1dx)
  use GridTrafo, only: pi
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: dv1dx

      real(myprec) :: x

      x = sin(x1*sqrt(2d0)*pi)/(sqrt(2d0)*pi)
      dv1dx = -2*x*exp( -(x**2)/(scale*scale) ) / (scale*scale) * &
              cos(x1*sqrt(2d0)*pi)
  end function dVel1Ddx

  function Vel1D(x1) result(v1)
  use GridTrafo, only: pi
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: v1

      real(myprec) :: x

      x  = sin(x1*sqrt(2d0)*pi)/(sqrt(2d0)*pi)
      v1 = exp( -(x**2)/(scale*scale) )
  end function Vel1D

  function dPropagationSpeed1Ddx(x1) result(dVdx)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: dVdx
      dVdx  = 0*x1
  end function dPropagationSpeed1Ddx

  function PropagationSpeed1D(x1) result(Vplus)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: Vplus
      Vplus  = 1d0+0*x1
  end function PropagationSpeed1D

  function dDens1Ddx(x1) result(drho1dx)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: drho1dx
      drho1dx   =  dVel1Ddx(x1)
  end function dDens1Ddx

  function Dens1D(x1) result(rho1)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: rho1
      rho1   = Vel1D(x1)
  end function Dens1D

  function Pres(rho) result(p)
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: p
      p = rho
  end function Pres

  function e_int(p) result(e)
  implicit none
      real(myprec), intent(in) :: p
      real(myprec)             :: e
      e = p*p
  end function e_int
end module LinearWave

module PerfectGas
! This module supports the state equation
!     R(p) = p/c^2
! for the wave equation
!     dv/dt = -grad Qfun = -1/rho * grad p
!
! However, all functions will be expressed in terms of the
! density rho, for instance the pressure function
!     P(rho) = rho*c^2
!
! This module provides the following functions:
!     Q(p)     = int^p 1/R(y) dy
!     S(p)     = int^p 1/R(y)^2 dy
!     e_int(p) = int^p (R(p)-R(y))/R(y)^2 dy
!
use Settings, only: myprec
implicit none
  real(myprec), parameter :: c    = 2.5
  real(myprec), parameter :: v0   = 0.0
  real(myprec), parameter :: ampl = 0.1
contains

  function dVel1Ddx(x1) result(dv1dx)
  use GridTrafo, only: pi
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: dv1dx
      dv1dx = ampl*2*sqrt(2d0)*pi*cos(2*x1*sqrt(2d0)*pi)
  end function dVel1Ddx

  function Vel1D(x1) result(v1)
  use GridTrafo, only: pi
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: v1
      v1  = v0 + ampl * (1.0 + sin(2*x1*sqrt(2d0)*pi) )
  end function Vel1D

  function dPropagationSpeed1Ddx(x1) result(dVdx)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: dVdx
      dVdx  = 0.5 * dVel1Ddx(x1) * (1 + Vel1D(x1)/sqrt(Vel1D(x1)**2+4*c*c))
  end function dPropagationSpeed1Ddx

  function PropagationSpeed1D(x1) result(Vplus)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: Vplus
      Vplus  = 0.5 * (Vel1D(x1) + sqrt(Vel1D(x1)**2+4*c*c))
  end function PropagationSpeed1D

  function dDens1Ddx(x1) result(drho1dx)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: drho1dx
      drho1dx   =   dPropagationSpeed1Ddx(x1) * exp(Vel1D(x1) * PropagationSpeed1D(x1)/(2*c*c)) &
                  +    PropagationSpeed1D(x1) * exp(Vel1D(x1) * PropagationSpeed1D(x1)/(2*c*c)) &
                        * (  dVel1Ddx(x1) * PropagationSpeed1D(x1)/(2*c*c) &
                           + Vel1D(x1) * dPropagationSpeed1Ddx(x1)/(2*c*c) )
  end function dDens1Ddx

  function Dens1D(x1) result(rho1)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: rho1
      rho1   = PropagationSpeed1D(x1) * exp(Vel1D(x1) * PropagationSpeed1D(x1)/(2*c*c))
  end function Dens1D

  function Pres(rho) result(p)
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: p
      p = rho * c*c
  end function Pres

  ! Expressing Q in terms of rho:
  !     Q(p) = int^p 1/R(y) dy
  !          = int^p c^2/y dy
  !          = c^2 (log(p)-log(p0))
  !          = c^2 (log(rho*c^2) - log(p0))
  !          = c^2 (log(rho) + log(c^2) - log(p0))
  !          = c^2 log(rho)
  !  when choosing p0=c^2.
  function Qfun(rho) result(q)
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: q
      q = c*c * log(rho)
  end function Qfun

  ! Expressing S in terms of rho:
  !     S(p) = int^p 1/R^2(y) dy
  !          = int^p c^4/y^2 dy
  !          = -c^4/p
  !          = -c^2/rho
  function Sfun(rho) result(s)
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: s
      s = -c*c / rho
  end function Sfun

  ! Expressing e_int in terms of rho:
  !     e_int(p) = c*c * (rho/rho_min - log(rho))
  !              = p/rho_min - c*c*log(p) + c*c*log(c^2)
  !    e'_int(p) = 1/rho_min - c*c/p
  !    e'_int(p)/R'(p) = 1/p_min - 1/p = int^p 1/y^2 dy
  function e_int(rho) result(e)
  use Util, only: error_exit
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: e
      real(myprec), parameter :: rho_min = 0.1
      if (rho<=0) call error_exit('Negative density?!')
      e = 2*c*c * (rho/rho_min - log(rho))  ! energy increasing for rho > rho_min
  end function e_int
end module PerfectGas

module ShallowWaterState
! This module supports the state equation
!     P(rho) = g * rho**2/2          R(p) = sqrt(2p/g)
! for the shallow-water equation
!     d (rho*v)/dt + Advec(rho,v) + grad p = 0
! However, all functions will be expressed in terms of the
!     Qfun(p) = p
!     Sfun(p) = int^p 1 /R(y) dy
!     e_int  = int^p R'(y)  * Sfun(y) dy
!
!     d (rho*v*v/2)/dt = -<v, grad p>  = -<v, rGrad s> = <s, DIVr v> = -<s, d rho/dt> = -< 1, d e_int/dt>
!
!     Qfun(p) = p
!     Sfun(p) =
!
!     d e_int(rho) /dt = s(rho) * d rho/dt
!     e'_int(rho) d rho /dt = s(rho) * d rho/dt
!     e'_int(rho) = s(rho)
!
!     d e_int / drho = Sfun
!     d e_int / dp / (drho/dp) = Sfun
!     d e_int / dp = (drho/dp) * Sfun(p)
!
!
use Settings, only: myprec
implicit none
  real(myprec), parameter :: g    = 9.81
  real(myprec), parameter :: v0   = 0.3
  real(myprec), parameter :: rho0 = 1.0
  real(myprec), parameter :: ampl = 0.1
contains

   !  1D shallow-water equation:
   !     dv/dt    +   v dv/dx + g d rho/dx = 0
   !     d rho/dt + rho dv/dx + v d rho/dx = 0
   !     d/dt sqrt(g rho) = sqrt(g) / (2 sqrt(rho)) d rho/dt = 1/2 sqrt(g/rho) d rho/dt
   !     d/dt sqrt(g rho) + 1/2 sqrt(g/rho) rho dv/dx + 1/2 sqrt(g/rho) v d rho/dx         = 0
   !     d/dt sqrt(g rho) + 1/2 sqrt(g rho)     dv/dx +     sqrt(g)     v d/dx sqrt(rho)   = 0
   !     d/dt sqrt(g rho) + 1/2 sqrt(g rho)     dv/dx +                 v d/dx sqrt(g rho) = 0
   !
   !     dv/dt + v dv/dx + g             d      rho   /dx = 0
   !     dv/dt + v dv/dx + g 2 sqrt(rho) d sqrt(rho)  /dx = 0
   !     dv/dt + v dv/dx + 2 sqrt(g rho) d sqrt(g rho)/dx = 0
   !
   !     d/dt sqrt(g rho) + 1/2 sqrt(g rho) dv/dx +              v d/dx sqrt(g rho) = 0
   !     d/dt   v         +               v dv/dx + 2 sqrt(g rho)  d/dx sqrt(g rho) = 0
   !
   !     d/dt sqrt(g rho) + 1/2 sqrt(g rho) dv/dx +              v d/dx sqrt(g rho) = 0
   !
   !     d/dt   v/2       +               v d/dx v/2 +   sqrt(g rho)  d/dx sqrt(g rho) = 0
   !     d/dt sqrt(g rho) +     sqrt(g rho) d/dx v/2 +              v d/dx sqrt(g rho) = 0
   !
   !     d/dt (v/2+sqrt(g rho)) +   (v+sqrt(g rho)) d/dx (v/2+sqrt(g rho)) = 0
   !     d/dt (v/2-sqrt(g rho)) +   (v-sqrt(g rho)) d/dx (v/2-sqrt(g rho)) = 0
   !
   ! Forward wave:
   !      Fback = v - 2*sqrt(g rho) = const = v0
   !
   !      v = 2*sqrt(g rho) + v0

   !      v = 2*sqrt(g rho) + v0
   !

  function dVel1Ddx(x1) result(dv1dx)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: dv1dx
      dv1dx  = dDens1Ddx(x1)*sqrt(g/Dens1D(x1))
  end function dVel1Ddx

  function Vel1D(x1) result(v1)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: v1
      v1  = v0 + 2*sqrt(g*Dens1D(x1))
  end function Vel1D

  function dDens1Ddx(x1) result(drho1dx)
  use GridTrafo, only: pi
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: drho1dx
      drho1dx  = 2*sqrt(2d0)*pi* ampl * cos(2*x1*sqrt(2d0)*pi)
  end function dDens1Ddx

  function Dens1D(x1) result(rho1)
  use GridTrafo, only: pi
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: rho1
      rho1  = rho0 + ampl * (1.0 + sin(2*x1*sqrt(2d0)*pi) )
  end function Dens1D

  function dPropagationSpeed1Ddx(x1) result(dVdx)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: dVdx
      dVdx  = 1.5*dDens1Ddx(x1)*sqrt(g/Dens1D(x1))
  end function dPropagationSpeed1Ddx

  function PropagationSpeed1D(x1) result(Vplus)
  implicit none
      real(myprec), intent(in) :: x1
      real(myprec)             :: Vplus
      Vplus  = v0 + 3*sqrt(g * Dens1D(x1))
  end function PropagationSpeed1D

  function Pres(rho) result(p)
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: p
      p = g * rho**2/2
  end function Pres

  ! From FDApplyOperators:
  !   dens = (Qfun(rhoa)-Qfun(rhob)) /
  !          (Sfun(rhoa)-Sfun(rhob))
  ! In SWE:
  !   grad p = dens grad sfun
  !   dens = grad p / grad sfun
  !   R(p) = grad p / (Sfun'(p)*grad p)
  !   R(p) = 1 / Sfun'(p)
  !
  !
  !   q = p;  s = g*rho
  function Qfun(rho) result(q)
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: q
      q = g *  rho**2 / 2
  end function Qfun

  !   Sfun(p) = int^p 1 /R(y) dy = g * rho
  function Sfun(rho) result(s)
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: s
      s = g * rho
  end function Sfun

  ! Expressing e_int in terms of rho:
  !     e'_int(rho) = s(rho)   =>   e_int = g * rho**2/2
  !     e_int  = int^p R'(y)  * Sfun(y) dy
  !            = int^p sqrt(2/g) / (2sqrt(y)) * g*sqrt(2y/g) dy
  !            = int^p sqrt(2/g)/2 *1/ (sqrt(y)) * g*sqrt(2/g) *sqrt(y) dy
  !            = int^p 1 dy = p = g * rho**2/2
  function e_int(rho) result(e)
  use Util, only: error_exit
  implicit none
      real(myprec), intent(in) :: rho
      real(myprec)             :: e
      if (rho<=0) call error_exit('Negative density?!')
      e = g * rho**2/2
  end function e_int

end module ShallowWaterState

module StateEq
use Settings,          only: myprec

   interface
      function ScalarFunction_itf(x) result(y)
      use Settings, only: myprec
      implicit none
         real(myprec), intent(in) :: x
         real(myprec)             :: y
      end function ScalarFunction_itf
   end interface

   procedure (ScalarFunction_itf),  pointer :: Qfun                  => null()
   procedure (ScalarFunction_itf),  pointer :: Pres                  => null()
   procedure (ScalarFunction_itf),  pointer :: Sfun                  => null()
   procedure (ScalarFunction_itf),  pointer :: e_int                 => null()
   procedure (ScalarFunction_itf),  pointer :: Dens1D                => null()
   procedure (ScalarFunction_itf),  pointer :: Vel1D                 => null()
   procedure (ScalarFunction_itf),  pointer :: PropagationSpeed1D    => null()
   procedure (ScalarFunction_itf),  pointer :: dDens1Ddx             => null()
   procedure (ScalarFunction_itf),  pointer :: dVel1Ddx              => null()
   procedure (ScalarFunction_itf),  pointer :: dPropagationSpeed1Ddx => null()

contains

    subroutine GetP(dens,p)
    implicit none
       real(myprec), intent(in)  :: dens(:)
       real(myprec), intent(out) :: p(:)

       integer :: i
!OMP PARALLEL DO PRIVATE(i)
       do i = 1,size(dens,1)
          p(i) = Pres(dens(i))
       end do
!OMP END PARALLEL DO
    end subroutine GetP

    subroutine GetInternal(dens,ei)
    implicit none
       real(myprec), intent(in)  :: dens(:)
       real(myprec), intent(out) :: ei(:)

       integer :: i
!OMP PARALLEL DO PRIVATE(i)
       do i = 1,size(dens,1)
          ei(i) = e_int(dens(i))
       end do
!OMP END PARALLEL DO
    end subroutine GetInternal

    subroutine GetS(dens,s)
    implicit none
       real(myprec), intent(in)  :: dens(:)
       real(myprec), intent(out) :: s(:)

       integer :: i
!OMP PARALLEL DO PRIVATE(i)
       do i = 1,size(dens,1)
          s(i) = Sfun(dens(i))
       end do
!OMP END PARALLEL DO
    end subroutine GetS

    subroutine GetQ(dens,q)
    implicit none
       real(myprec), intent(in)  :: dens(:)
       real(myprec), intent(out) :: q(:)

       integer :: i
!OMP PARALLEL DO PRIVATE(i)
       do i = 1,size(dens,1)
          q(i) = Qfun(dens(i))
       end do
!OMP END PARALLEL DO
    end subroutine GetQ

   subroutine SwitchLinearWave()
   use LinearWave, only: PPres=>Pres, Pe_int => e_int
   use LinearWave, only: PVel1D=>Vel1D, PDens1D=>Dens1D, PPropagationSpeed1D=>PropagationSpeed1D
   use LinearWave, only: PdVel1Ddx=>dVel1Ddx, PdPropagationSpeed1Ddx=>dPropagationSpeed1Ddx, PdDens1Ddx=>dDens1Ddx
   implicit none
     Qfun   => null()
     Pres   => PPres
     Sfun   => null()
     e_int  => Pe_int
     Dens1D => PDens1D
     Vel1D  => PVel1D
     dDens1Ddx             => PdDens1Ddx
     dVel1Ddx              => PdVel1Ddx
     PropagationSpeed1D    => PPropagationSpeed1D
     dPropagationSpeed1Ddx => PdPropagationSpeed1Ddx
   end subroutine SwitchLinearWave

   subroutine SwitchCompressibleWave()
   use PerfectGas, only: PPres=>Pres, QPfun=>Qfun, SPfun=>Sfun, eP_int=>e_int
   use PerfectGas, only: PVel1D=>Vel1D, PDens1D=>Dens1D, PPropagationSpeed1D=>PropagationSpeed1D
   use PerfectGas, only: PdVel1Ddx=>dVel1Ddx, PdPropagationSpeed1Ddx=>dPropagationSpeed1Ddx, PdDens1Ddx=>dDens1Ddx
   implicit none
     Qfun   => QPfun
     Pres   => PPres
     Sfun   => SPfun
     e_int  => eP_int
     Dens1D => PDens1D
     Vel1D  => PVel1D
     dDens1Ddx             => PdDens1Ddx
     dVel1Ddx              => PdVel1Ddx
     PropagationSpeed1D    => PPropagationSpeed1D
     dPropagationSpeed1Ddx => PdPropagationSpeed1Ddx
   end subroutine SwitchCompressibleWave

   subroutine SwitchShallowWater()
   use ShallowWaterState, only: PSres=>Pres, QSfun=>Qfun, SSfun=>Sfun, eS_int=>e_int
   use ShallowWaterState, only: SVel1D=>Vel1D, SDens1D=>Dens1D, SPropagationSpeed1D=>PropagationSpeed1D
   use ShallowWaterState, only: SdVel1Ddx=>dVel1Ddx, SdPropagationSpeed1Ddx=>dPropagationSpeed1Ddx, SdDens1Ddx=>dDens1Ddx
   implicit none
     Qfun  => QSfun
     Pres  => PSres
     Sfun  => SSfun
     e_int => eS_int
     Dens1D => SDens1D
     Vel1D  => SVel1D
     dDens1Ddx             => SdDens1Ddx
     dVel1Ddx              => SdVel1Ddx
     PropagationSpeed1D    => SPropagationSpeed1D
     dPropagationSpeed1Ddx => SdPropagationSpeed1Ddx
   end subroutine SwitchShallowWater
end module StateEq

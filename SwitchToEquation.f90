! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module SwitchToEquation
! This module initializes the arrays and functions for the chosen model
   interface
      function Totals_itf(yin) result(Totals)
      use Settings, only: myprec, tnTOTALS
      implicit none
         real(myprec), target, intent(in) :: yin(0:)
         real(myprec)                     :: Totals(tnTOTALS)
      end function Totals_itf

      subroutine WaveFun_itf(f,t,y)
      use Settings, only: myprec
      implicit none
         real(myprec), target, intent(out) :: f(0:)
         real(myprec),         intent(in)  :: t
         real(myprec), target, intent(in)  :: y(0:)
      end subroutine WaveFun_itf
   end interface

   procedure (Totals_itf),   pointer :: CalcTotals => null()
   procedure (WaveFun_itf),  pointer :: WaveFun    => null()

contains

   subroutine Initialize(eqnam, fromLocal)
   use CalcMatrices_Scalar,         only: CalcScalarMatrix  => CalcMatrices
   use CalcMatrices_FD,             only: CalcFDMatrices    => CalcMatrices
   use CalcMatrices_LinearGalerkin, only: CalcStaggerMatrix => CalcMatrices
   use CalculateTotals,             only: CalcTotalsFromDpDt, CalcTotalsFromMomentum, CalcTotalsFromVel
   use Inputs,                      only: set_Inputs, eqident, eqSCALAR, eqLINEARGALERKIN, eqLINEARFD, eqCOMPRESSIBLE, eqSHALLOW
   use GridValues,                  only: SetGridOri
   use StateEq,                     only: SwitchLinearWave, SwitchCompressibleWave, SwitchShallowWater
   use Util,                        only: error_exit
   use WaveEquations,               only: ScalarWaveFun, LinearGalerkinWaveFun, LinearFDWaveFun, CompressibleWaveFun, ShallowWaterWaveFun
   implicit none

     character(len=*), optional, intent(in) :: eqnam
     logical,          optional, intent(in) :: fromLocal

     call set_Inputs(eqnam, fromLocal)
     call SetGridOri()

     if (eqident==eqSCALAR) then
        call SwitchLinearWave()
        CalcTotals => CalcTotalsFromDpDt
        WaveFun    => ScalarWaveFun
        call CalcScalarMatrix()
     elseif (eqident == eqLINEARGALERKIN) then
        call SwitchLinearWave()
        CalcTotals => CalcTotalsFromVel
        WaveFun    => LinearGalerkinWaveFun
        call CalcStaggerMatrix()
     elseif (eqident == eqLINEARFD) then
        call SwitchLinearWave()
        CalcTotals => CalcTotalsFromVel
        WaveFun    => LinearFDWaveFun
        call CalcFDMatrices()
     elseif (eqident==eqCOMPRESSIBLE) then
        call SwitchCompressibleWave()
        CalcTotals => CalcTotalsFromVel
        WaveFun    => CompressibleWaveFun
        call CalcFDMatrices()
     elseif (eqident==eqSHALLOW) then
        call SwitchShallowWater()
        CalcTotals => CalcTotalsFromMomentum
        WaveFun    => ShallowWaterWaveFun
        call CalcFDMatrices()
     else
        call error_exit('input error')
     end if
   end subroutine Initialize
end module SwitchToEquation

! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module TestFunctions
! This model provides all kinds of functions to construct tests with
contains
   subroutine TestValues(f,vx,vy, dfdx, dfdy, DivV,VxAtN,VyAtE, &
                         Rho,RhoAtE,RhoAtN,Rhox,Rhoy, DivRV, &
                         ux, uy, wx, wy, &
                         vx10, vy10, vx01, vy01, &
                         vx10AtC, vy10AtC, AdvectionX, AdvectionY)
   use GridTrafo, only: GridOrientation, pi
   use Settings,  only: myprec
   use Inputs,    only: m,n, GridFunctions
   implicit none
      !  x-freq   x-fase0      x-amp    y-freq   y-fase0
      real(myprec), parameter :: parP(2,5) = reshape( (/ &
             1d0,           0d0,       &
            -dble(pi)*0.02d0,       0d0,       &
             1d0,           3d0,       &
             0d0,           3d0,       &
             0d0,           -0.56d0*dble(pi)   /), (/2,5/))
      real(myprec), parameter :: parVx(2,5) = reshape( (/ &
             1d0,           0d0,       &
            -dble(pi)/2,    0d0,       &
             0.5d0/dble(pi),3d0,       &
             0d0,           1d0,       &
             0d0,           0d0        /), (/2,5/))
      real(myprec), parameter :: parVy(2,5) = reshape( (/ &
             1d0,           0d0,       &
            -dble(pi)/2,          0d0,       &
             0.5d0/dble(pi),        3d0,       &
             0d0,           1d0,       &
             0d0,           0d0        /), (/2,5/))
      real(myprec), parameter :: parUx(2,5) = reshape( (/ &
             1d0,           0d0,       &
            -dble(pi)/2,          0d0,       &
             2.2d0,         3d0,       &
             2d0,           1.3d0,     &
             0.1d0,         2.0d0      /), (/2,5/))
      real(myprec), parameter :: parUy(2,5) = reshape( (/ &
             2d0,           1d0,       &
            -dble(pi)/2,          0.1d0,     &
             1.7d0,         1d0,       &
             1d0,           0d0,       &
             0.8d0,         0d0        /), (/2,5/))
      real(myprec), parameter :: parWx(2,5) = reshape( (/ &
             1d0,           1d0,       &
            -0.8d0,         0d0,       &
             0.86d0,        1.9d0,     &
             0d0,           2d0,       &
             0d0,           1.7d0      /), (/2,5/))
      real(myprec), parameter :: parWy(2,5) = reshape( (/ &
             0d0,           2d0,       &
             0d0,           1.2d0,     &
             1.34d0,        2d0,       &
             1d0,           1d0,       &
             1.2d0,         0.3d0      /), (/2,5/))
      real(myprec), parameter :: parDens(3,5) = reshape( (/ &
             0d0,           1d0,       0d0,   &
             0d0,           2d0,       0d0,   &
             3d0,           1d0,       1.5d0, &
             0d0,           0d0,       2d0,   &
             0d0,           0d0,       0.2d0  /), (/3,5/))
      real(myprec), optional, pointer :: f(:), vx(:), vy(:)
      real(myprec), optional, pointer :: dfdx(:), dfdy(:), DivV(:), DivRV(:)
      real(myprec), optional, pointer :: ux(:), uy(:), wx(:), wy(:)
      real(myprec), optional, pointer :: vx10(:), vy10(:), vx01(:), vy01(:)
      real(myprec), optional, pointer :: VxAtN(:),VyAtE(:)
      real(myprec), optional, pointer :: Vx10AtC(:),Vy10AtC(:)
      real(myprec), optional, pointer :: Rho(:), RhoAtE(:), RhoAtN(:), Rhox(:), Rhoy(:)
      real(myprec), optional, pointer :: AdvectionX(:), AdvectionY(:)
      real(myprec)                    :: xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta
      real(myprec)                    :: Ax, Ay
      integer                         :: irow

      if (present(f)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            f(irow)  = Pfun(x,y)
         end do
      end if

      if (present(vx10atC)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi, eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            vx10AtC(irow) = Ax * 1 + Ay * 0
         end do
      end if

      if (present(vy10AtC)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            vy10AtC(irow) = -Ay * 1 + Ax * 0
         end do
      end if
      if (present(vx10)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            vx10(irow) = Ax * 1 + Ay * 0
         end do
      end if

      if (present(vy10)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            vy10(irow) = -Ay * 1 + Ax * 0
         end do
      end if

      if (present(vx01)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            vx01(irow) = Ax * 0 + Ay * 1
         end do
      end if

      if (present(vy01)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            vy01(irow) = -Ay * 0 + Ax * 1
         end do
      end if

      if (present(vx)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            vx(irow) = Ax * VelXfun(x,y) + Ay * VelYfun(x,y)
         end do
      end if

      if (present(vy)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            vy(irow) = -Ay * VelXfun(x,y) + Ax * VelYfun(x,y)
         end do
      end if

      if (present(ux)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            ux(irow)  = Ax*VelUxFun(x,y) + Ay*VelUyFun(x,y)
         end do
      end if

      if (present(AdvectionX)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            AdvectionX(irow)  = Ax*AdvectionXFun(x,y) + Ay*AdvectionYFun(x,y)
         end do
      end if

      if (present(AdvectionY)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            AdvectionY(irow)  = -Ay*AdvectionXFun(x,y) + Ax*AdvectionYFun(x,y)
         end do
      end if

      if (present(uy)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            uy(irow) = -Ay * VelUxfun(x,y) + Ax * VelUyfun(x,y)
         end do
      end if

      if (present(wx)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            wx(irow)  = Ax*VelWxFun(x,y) + Ay*VelWyFun(x,y)
         end do
      end if

      if (present(wy)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            wy(irow) = -Ay * VelWxfun(x,y) + Ax * VelWyfun(x,y)
         end do
      end if


      if (present(dfdx)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            dfdx(irow)  = Ax*dPdxFun(x,y) + Ay*dPdyFun(x,y)
         end do
      end if

      if (present(dfdy)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            dfdy(irow)  = -Ay*dPdxFun(x,y) + Ax*dPdyFun(x,y)
         end do
      end if
      if (present(DivV)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            DivV(irow) = dVelXdxfun(x,y) + dVelYdYfun(x,y)
         end do
      end if

      if (present(VxAtN)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            VxAtN(irow) = Ax * VelXfun(x,y) + Ay * VelYfun(x,y)
         end do
      end if

      if (present(VyAtE)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            VyAtE(irow) = -Ay * VelXfun(x,y) + Ax * VelYfun(x,y)
         end do
      end if

      if (present(Rho)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            Rho(irow) =  Rhofun(x,y)
         end do
      end if

      if (present(RhoAtE)) then
         print *,'RhoAtE is present'
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi+0.5,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            RhoAtE(irow) =  Rhofun(x,y)
         end do
      end if

      if (present(RhoAtN)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta+0.5, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            RhoAtN(irow) =  Rhofun(x,y)
         end do
      end if

      if (present(Rhox)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            call GridOrientation(dxdxi, dydxi, dxdeta, dydeta, Ax, Ay)
            Rhox(irow) =   Ax * dRhodxFun(x,y) + Ay * dRhodyFun(x,y)
            Rhoy(irow) =  -Ay * dRhodxFun(x,y) + Ax * dRhodyFun(x,y)
         end do
      end if

      if (present(DivRV)) then
         do irow = 0,m*n-1
            eta = irow/m
            xi  = mod(irow,m)
            call GridFunctions(xi,eta, x,y, dxdxi, dydxi, dxdeta, dydeta)
            DivRV(irow) =   Rhofun(x,y) * (dVelXdxfun(x,y) + dVelYdYfun(x,y)) + &
                            dRhodxfun(x,y) * VelXfun(x,y) + &
                            dRhodyfun(x,y) * VelYfun(x,y)
         end do
      end if

   contains

      real(myprec) function GenFun(par, x,y,dx,dy)
      implicit none
         real(myprec), intent(in)           :: par(:,:)
         real(myprec), intent(in)           :: x,y
         integer,      intent(in), optional :: dx, dy

         integer      :: dx_, dy_
         integer      :: j, p, k
         real(myprec) :: termx, termy

         dx_=0
         if (present(dx)) dx_ = dx
         dy_=0
         if (present(dy)) dy_ = dy

         Genfun = 0
         do j = 1,size(par,1)
            termx = 1
            p = nint(par(j,1))
            if (mod(dx_,2) == 0) then
               termx =         cos(p*2*pi*x + par(j,2))
            else
               termx = -p*2*pi*sin(p*2*pi*x + par(j,2))
            end if
            do k = 1,dx_/2,2
               termx = -(p*2*pi)**2*termx
            end do
            termy = 1
            p = nint(par(j,4))
            if (mod(dy_,2) == 0) then
               termy =         cos(p*2*pi*y + par(j,5))
            else
               termy = -p*2*pi*sin(p*2*pi*y + par(j,5))
            end if
            do k = 1,dy_/2,2
               termy = -(p*2*pi)**2*termy
            end do
            Genfun = Genfun + termx * termy * par(j,3)
         end do
      end function Genfun

      real(myprec) function AdvectionXFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         AdvectionXfun = VelXfun(x,y)    * Rhofun(x,y)    * dVelUxDxFun(x,y)  + &
                         VelXfun(x,y)    * dRhoDxfun(x,y) *  VelUxFun(x,y)  + &
                         dVelXdxFun(x,y) * Rhofun(x,y)    *  VelUxFun(x,y)  + &
                         VelYfun(x,y)    * Rhofun(x,y)    * dVelUxDyFun(x,y)  + &
                         VelYfun(x,y)    * dRhoDyfun(x,y) *  VelUxFun(x,y)  + &
                         dVelYdyFun(x,y) * Rhofun(x,y)    *  VelUxFun(x,y)
      end function AdvectionXFun

      real(myprec) function AdvectionYFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         AdvectionYfun =  VelXfun(x,y)   *  Rhofun(x,y)   * dVelUyDxFun(x,y)  + &
                          VelXfun(x,y)   * dRhoDxfun(x,y) *  VelUyFun(x,y)  + &
                         dVelXdxFun(x,y) *  Rhofun(x,y)   *  VelUyFun(x,y)  + &
                          VelYfun(x,y)   *  Rhofun(x,y)   * dVelUyDyFun(x,y)  + &
                          VelYfun(x,y)   * dRhoDyfun(x,y) *  VelUyFun(x,y)  + &
                         dVelYdyFun(x,y) *  Rhofun(x,y)   *  VelUyFun(x,y)
      end function AdvectionYFun

      ! Pressure:
      real(myprec) function Pfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         Pfun = GenFun(parP, x,y)
      end function Pfun

      ! x-velocity:
      real(myprec) function VelXfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         VelXfun = GenFun(parVx, x,y)
      end function VelXfun

      ! y-velocity:
      real(myprec) function VelYfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         VelYfun = GenFun(parVy, x,y)
      end function VelYfun

      ! alt x-velocity u:
      real(myprec) function VelUxfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         VelUxfun = GenFun(parUx, x,y)
      end function VelUxfun

      real(myprec) function dVelUxDxfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dVelUxDxfun = GenFun(parUx, x,y,dx=1)
      end function dVelUxDxfun
      real(myprec) function dVelUxDyfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dVelUxDyfun = GenFun(parUx, x,y,dy=1)
      end function dVelUxDyfun

      ! alt y-velocity u:
      real(myprec) function VelUyfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         VelUyfun = GenFun(parUy, x,y)
      end function VelUyfun
      real(myprec) function dVelUyDxfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dVelUyDxfun = GenFun(parUy, x,y,dx=1)
      end function dVelUyDxfun
      real(myprec) function dVelUyDyfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dVelUyDyfun = GenFun(parUy, x,y,dy=1)
      end function dVelUyDyfun

      ! alt x-velocity w:
      real(myprec) function VelWxfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         VelWxfun = GenFun(parWx, x,y)
      end function VelWxfun

      ! alt y-velocity w:
      real(myprec) function VelWyfun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         VelWyfun = GenFun(parWy, x,y)
      end function VelWyfun

      ! Density:
      real(myprec) function Rhofun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         Rhofun = GenFun(parDens, x,y)
      end function Rhofun

      real(myprec) function dVelXdxFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dVelXdxFun = GenFun(parVx, x,y, dx=1)
      end function dVelXdxFun

      real(myprec) function dVelXdyFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dVelXdyFun = GenFun(parVx, x,y, dy=1)
      end function dVelXdyFun


      real(myprec) function dVelYdxFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dVelYdxFun = GenFun(parVy, x,y, dx=1)
      end function dVelYdxFun

      real(myprec) function dVelYdyFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dVelYdyFun = GenFun(parVy, x,y, dy=1)
      end function dVelYdyFun


      real(myprec) function dPdxFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dPdxFun = GenFun(parP, x,y, dx=1)
      end function dPdxFun

      real(myprec) function dPdyFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dPdyFun = GenFun(parP, x,y, dy=1)
      end function dPdyFun


      real(myprec) function dRhodxFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dRhodxFun = GenFun(parDens, x,y, dx=1)
      end function dRhodxFun

      real(myprec) function dRhodyFun(x,y)
      implicit none
         real(myprec), intent(in) :: x,y
         dRhodyFun = GenFun(parDens, x,y, dy=1)
      end function dRhodyFun
   end subroutine TestValues
end module TestFunctions

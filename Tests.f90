! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module Tests
implicit none
! This module provides all the tests used in the unit-test programs test_*.exe
contains

  subroutine TestQuadratures()
  use Util,       only: error_exit
  use GridValues, only: dVe, dVn, dVc
  use Inputs,     only: m,n
  use Settings,   only: myprec
  implicit none

    real(myprec)            :: totQ, totQx, totQy
    real(myprec)            :: error
    integer                 :: irow
    real(myprec), parameter :: tol=1e-6


    print *,'Testing EXACT integration of constant scalars'
    totQ  = 0
    totQx = 0
    totQy = 0
    do irow = 0,n*m-1
       totQ  = totQ  + dVc(irow)
       totQx = totQx + dVe(irow)
       totQy = totQy + dVn(irow)
    end do
    error = totQ-1
    print '(a,1p,e10.2)','     error( int 1_P = 1 )  = ',error
    if (abs(error) > tol) call error_exit('error too large', fatal=.false.)

    error = totQx-1
    print '(a,1p,e10.2)','     error( int 1_X = 1 )  = ',error
    if (abs(error) > tol) call error_exit('error too large', fatal=.false.)

    error = totQy-1
    print '(a,1p,e10.2)','     error( int 1_Y = 1 )  = ',error
    if (abs(error) > tol) call error_exit('error too large', fatal=.false.)
    print *
  end subroutine TestQuadratures

    subroutine TestChainRule()
    use Checks,           only: CheckSame
    use FDApplyOperators, only: ApplyDensTimesGradient, ApplyGradient
    use Inputs,           only: m,n
    use Settings,         only: myprec
    use StateEq,          only: GetQ, GetS
    use TestFunctions,    only: TestValues
    use Util,             only: error_exit
    implicit none
      real(myprec), pointer   :: rho(:), dqdxi(:), dqdeta(:), r_dsdxi(:), r_dsdeta(:)
      real(myprec), pointer   :: q(:), s(:)
      real(myprec), parameter :: tol=1e-10
      integer                 :: ierr

      print *,'Testing chain rule: EXACT equality of GRAD*q and rGRAD*s'

      allocate(q(0:m*n-1),              s(0:m*n-1), &
               dqdxi(0:m*n-1),     dqdeta(0:m*n-1), &
               r_dsdxi(0:m*n-1), r_dsdeta(0:m*n-1), &
               rho(0:m*n-1),             stat=ierr)
      if (ierr /= 0) call error_exit('allocation error')

      ! Set a function for rho
      call TestValues(q,dqdxi,dqdeta,Rho=rho)
      q      = 0  ! Erase nonnecessary data
      dqdxi  = 0  ! Erase nonnecessary data
      dqdeta = 0  ! Erase nonnecessary data

      call GetQ(rho,q)
      call GetS(rho,s)
      call ApplyGradient(q,dqdxi,dqdeta)
      call ApplyDensTimesGradient(rho,s,r_dsdxi,r_dsdeta)
      call CheckSame(r_dsdxi,  dqdxi,  tol, 'r_dsdxi',  'dqdxi')
      call CheckSame(r_dsdeta, dqdeta, tol, 'r_dsdeta', 'dqdeta')

      deallocate(q,s,dqdxi,dqdeta,r_dsdxi,r_dsdeta,rho,stat=ierr)
      if (ierr /= 0) call error_exit('deallocation error')
      print *
    end subroutine TestChainRule

    subroutine TestSymmetryRho()
    use Settings,         only: myprec
    use Checks,           only: CheckAdjointnessOneVectorField
    use Util,             only: error_exit
    use Inputs,           only: m,n
    use TestFunctions,    only: TestValues
    use FDApplyOperators, only: ApplyDensTimesGradient, ApplyDivergenceDensTimes
    implicit none
      integer                 :: ierr
      real(myprec), pointer   :: rGx(:), rGy(:), f(:)
      real(myprec), pointer   :: vx(:), vy(:), DivRV(:), rho(:)
      real(myprec), parameter :: tol=1e-10

      print *,'Testing EXACT adjointness of DIVr and -rGRAD'

      allocate(vx(0:n*m), vy(0:n*m), DivRV(0:n*m), rho(0:n*m), &
               rGx(0:n*m), rGy(0:n*m), f(0:n*m), stat=ierr)
      if (ierr/=0) call error_exit('allocation error')

      call TestValues(f,vx,vy,Rho=rho)
      call ApplyDivergenceDensTimes(rho,vx,vy,DivRV)
      call ApplyDensTimesGradient(rho,f,rGx,rGy)
      f = -f
      call CheckAdjointnessOneVectorField(f, vx,vy, rGx,rGy, DivRV, tol, 'rGrad', '-DivR')

      deallocate(vx,vy,DivRV,rho,rGx,rGy,f,stat=ierr)
      if (ierr/=0) call error_exit('deallocation error')
      print *
    end subroutine TestSymmetryRho

    subroutine TestAccuracyRho()
    use Util,             only: error_exit
    use Settings,         only: myprec
    use TestFunctions,    only: TestValues
    use FDApplyOperators, only: ApplyDensTimesGradient, ApplyDivergenceDensTimes, ApplyAdvectionA, ApplyAdvection
    use Inputs,           only: m,n
    use OutFile,          only: WriteDiagMatrix
    use Checks,           only: CheckSame
    implicit none
      integer                 :: ierr
      real(myprec), pointer   :: fx(:), fy(:), DivRVxy(:)
      real(myprec), pointer   :: rGx(:), rGy(:), f(:)
      real(myprec), pointer   :: vx(:), vy(:), DivRV(:), DivRVAtE(:), DivRVAtN(:)
      real(myprec), pointer   :: Rho(:), RhoAtE(:), RhoAtN(:)
      real(myprec), pointer   :: AdvecX(:), AdvecY(:)
      real(myprec), pointer   :: AdvecXa(:), AdvecYa(:)
      real(myprec), parameter :: tol=4e-6

      allocate(fx(0:n*m-1), fy(0:n*m-1), DivRVxy(0:n*m-1), vx(0:n*m-1), vy(0:n*m-1), &
               DivRV(0:n*m-1), DivRVAtE(0:n*m-1), DivRVAtN(0:n*m-1), &
               rGx(0:n*m-1), rGy(0:n*m-1), f(0:n*m-1), Rho(0:n*m-1), RhoAtE(0:n*m-1), RhoAtN(0:n*m-1), &
               AdvecXa(0:n*m-1), AdvecYa(0:n*m-1), AdvecX(0:n*m-1), AdvecY(0:n*m-1), stat=ierr)
      if (ierr/=0) call error_exit('allocation error')

      call TestValues(f=f, vx=vx, vy=vy, dfdx=fx,dfdy=fy, Rho=rho, DivRV=DivRVxy, RhoAtE=RhoAtE, RhoAtN=RhoAtN, AdvectionX=AdvecX, AdvectionY=AdvecY)

      call ApplyDivergenceDensTimes(rho,vx,vy,DivRV)
      call ApplyDensTimesGradient(rho,f,rGx,rGy)

      print *,'Testing ACCURACY of rGRAD and DIVr'
      call CheckSame(rGx,RhoAtE*fx,tol,nameF='rGx',nameG='r*f')
      call CheckSame(rGy,RhoAtN*fy,tol,nameF='rGy',nameG='r*f')
      call CheckSame(DivRv,DivRVxy,tol,nameF='DivRV',nameG='DivR*v')

      print *,'Testing ACCURACY of Advection operator'
      call TestValues(ux=fx, uy=fy)
      call ApplyAdvectionA(rho,vx,vy,fx,fy,DivRV, AdvecXa, AdvecYa)
      call CheckSame(AdvecX,AdvecXa,tol,nameF='AdvecXU',nameG='AdvecX*U')
      call CheckSame(AdvecY,AdvecYa,tol,nameF='AdvecYU',nameG='AdvecY*U')

      print *,'Testing ACCURACY of Advection combined operator'
      call ApplyAdvection(rho,vx,vy,fx,fy,DivRV, DivRVAtE, DivRVAtN, AdvecXa, AdvecYa)
      call CheckSame(AdvecX,AdvecXa,tol,nameF='AdvecXU',nameG='AdvecX*U')
      call CheckSame(AdvecY,AdvecYa,tol,nameF='AdvecYU',nameG='AdvecY*U')

      deallocate(vx,vy,DivRV,rGx,rGy,f,fx,fy,DivRVxy,Rho,RhoAtE,RhoAtN,AdvecX,AdvecY,AdvecXa,AdvecYa,stat=ierr)
      if (ierr/=0) call error_exit('deallocation error')
      print *
    end subroutine TestAccuracyRho

   subroutine TestGradConst(ApplyGradient)
   use WaveEquations, only: ApplyGradient_itf
   use Settings,      only: myprec
   use Util,          only: error_exit
   use Checks,        only: CheckZero
   use Inputs,        only: m,n
   implicit none
     procedure (ApplyGradient_itf) :: ApplyGradient
     integer                       :: ierr
     real(myprec), pointer         :: Gx(:), Gy(:), f(:)
     real(myprec), parameter       :: tol = 1e-8

     print *,'Testing that a constant is EXACTLY gradient-free'
     allocate(Gx(0:n*m), Gy(0:n*m), f(0:n*m), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')
     f = 1

     call ApplyGradient(f,Gx,Gy)
     call CheckZero(Gx,tol,nameF='GradX*1')
     call CheckZero(Gy,tol,nameF='GradY*1')

     deallocate(Gx,Gy,f,stat=ierr)
     if (ierr/=0) call error_exit('deallocation error')
     print *
   end subroutine TestGradConst

   subroutine TestSymmetry(ApplyDivergence , ApplyGradient)
   use WaveEquations, only: ApplyGradient_itf, ApplyDivergence_itf
   use Settings,      only: myprec
   use Checks,        only: CheckAdjointnessOneVectorField
   use TestFunctions, only: TestValues
   use Util,          only: error_exit
   use Inputs,        only: m,n
   implicit none
     procedure (ApplyGradient_itf)   :: ApplyGradient
     procedure (ApplyDivergence_itf) :: ApplyDivergence

     integer                 :: ierr
     real(myprec), pointer   :: Gx(:), Gy(:), f(:)
     real(myprec), pointer   :: vx(:), vy(:), DivV(:)
     real(myprec), parameter :: tol=1e-10

     print *,'Testing EXACT adjointness of divergence and -gradient'

     allocate(vx(0:n*m), vy(0:n*m), DivV(0:n*m), &
              Gx(0:n*m), Gy(0:n*m), f(0:n*m), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')
     call TestValues(f,vx,vy)
     call ApplyDivergence(vx,vy,DivV)
     call ApplyGradient(f,Gx,Gy)
     f = -f
     call CheckAdjointnessOneVectorField(f, vx,vy, Gx,Gy, DivV, tol, 'Grad', '-Div')

     deallocate(vx,vy,DivV,Gx,Gy,f,stat=ierr)
     if (ierr/=0) call error_exit('deallocation error')
     print *
   end subroutine TestSymmetry

   subroutine TestAccuracy(ApplyDivergence , ApplyGradient,tol)
   use WaveEquations, only: ApplyGradient_itf, ApplyDivergence_itf
   use Settings,      only: myprec
   use TestFunctions, only: TestValues
   use Util,          only: error_exit
   use Inputs,        only: m,n
   use Checks,        only: CheckSame
   implicit none
     real(myprec), intent(in), optional :: tol
     procedure (ApplyGradient_itf)      :: ApplyGradient
     procedure (ApplyDivergence_itf)    :: ApplyDivergence

     integer               :: ierr
     real(myprec), pointer :: dfdx(:), dfdy(:), DivVxy(:)
     real(myprec), pointer :: Gx(:), Gy(:), f(:)
     real(myprec), pointer :: vx(:), vy(:), DivV(:)
     real(myprec)          :: tol_a

     tol_a = 1e-6
     if (present(tol)) tol_a = tol

     allocate(dfdx(0:n*m-1), dfdy(0:n*m-1), DivVxy(0:n*m-1), &
              vx(0:n*m-1), vy(0:n*m-1), DivV(0:n*m-1), &
              Gx(0:n*m-1), Gy(0:n*m-1), f(0:n*m-1), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')

     call TestValues(f,vx,vy,dfdx,dfdy,DivVxy)
     call ApplyDivergence(vx,vy,DivV)
     call ApplyGradient(f,Gx,Gy)

     print *,'Testing ACCURACY of gradient and divergence'
     call CheckSame(dfdx, Gx, tol_a, 'df/dx', 'GradX*f')
     call CheckSame(dfdy, Gy, tol_a, 'df/dy', 'GradY*f')
     call CheckSame(DivV, DivVxy, tol_a, 'DivV', 'Div*v')

     deallocate(vx,vy,DivV,Gx,Gy,f,dfdx,dfdy,DivVxy,stat=ierr)
     if (ierr/=0) call error_exit('deallocation error')
     print *
   end subroutine TestAccuracy

   subroutine TestDivConst(ApplyDivergence)
   use WaveEquations, only: ApplyDivergence_itf
   use Settings,      only: myprec
   use Util,          only: error_exit
   use Inputs,        only: m,n
   use TestFunctions, only: TestValues
   use Checks,        only: CheckZero
   implicit none
     procedure (ApplyDivergence_itf) :: ApplyDivergence

     ! Locals
     integer                 :: ierr
     real(myprec), pointer   :: DivV(:)
     real(myprec), parameter :: tol = 1e-8
     real(myprec), pointer   :: vx01(:), vy01(:)
     real(myprec), pointer   :: vx10(:), vy10(:)

     allocate( vx10(0:n*m),    vy10(0:n*m),    vx01(0:n*m),    vy01(0:n*m),    &
               DivV(0:n*m), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')

     call TestValues( vx10=vx10, vy10=vy10, vx01=vx01, vy01=vy01)

     print *,'Testing that constant flow is EXACTLY divergence-free'
     ! Test 1: v=(1,0)
     call ApplyDivergence(vx10,vy10,DivV)
     call CheckZero(DivV,tol,nameF='Div(1,0)')

     ! Test 2: v=(0,1)
     call ApplyDivergence(vx01,vy01,DivV)
     call CheckZero(DivV,tol,nameF='Div(0,1)')

     deallocate(vx10,vy10,vx01,vy01,DivV,stat=ierr)
     if (ierr/=0) call error_exit('deallocation error')
     print *
   end subroutine TestDivConst

   subroutine TestAdjointInterp()
   use FDApplyOperators, only: VxAtC, VyAtC, VyAtE, VxAtN, InterpC2V, InterpC2V, InterpV2C, InterpVelocity
   use Settings,         only: myprec
   use TestFunctions,    only: TestValues
   use Checks,           only: CheckAdjointness
   use GridValues,       only: dVe, dVn, dVc
   use Inputs,           only: m,n
   use Util,             only: error_exit
   implicit none

      real(myprec), pointer   :: VxAtE(:), VyAtN(:), GAtE(:), FAtN(:)
      real(myprec), pointer   :: FAtE(:), GAtN(:)
      real(myprec), pointer   :: FAtC(:), GAtC(:)
      integer                 :: ierr
      real(myprec), parameter :: tol=1e-10

      print *,'Testing EXACT adjointness of interpolations'

      allocate( VxAtE(0:n*m-1), VyAtN(0:n*m-1), &
                GAtE(0:n*m-1),  FAtN(0:n*m-1), &
                FAtE(0:n*m-1),  GAtN(0:n*m-1), FAtC(0:n*m-1), GAtC(0:n*m-1), &
                stat=ierr)
      if (ierr/=0) call error_exit('allocation error')

      ! Initialize values:
      call TestValues(FAtE, VxAtC, VyAtC, GAtN)

      ! Calculate
      !    GAtE = InterpY2Xa * GAtN
      !    FatN = InterpX2Ya * FAtE
      call InterpV2C(FAtE, GAtN, FatC,GAtC, set_zero = .true., exactAtV=.true. )
      call InterpC2V(VxAtC, VyAtC, VxAtE,VyAtN, set_zero = .true., exactAtV=.true. )
      call CheckAdjointness(FatE,VxAtC, FatC,VxAtE, dVe, dVc, tol, 'E2C', 'P2Xa')
      call CheckAdjointness(GatN,VyAtC, GatC,VyAtN, dVn, dVc, tol, 'N2C', 'P2Ya')
      call InterpV2C(FAtE, GAtN, FatC,GAtC, set_zero = .true., exactAtC=.true. )
      call InterpC2V(VxAtC, VyAtC, VxAtE,VyAtN, set_zero = .true., exactAtC=.true. )
      call CheckAdjointness(FatE,VxAtC, FatC,VxAtE, dVe, dVc, tol, 'X2Pa', 'C2E')
      call CheckAdjointness(GatN,VyAtC, GatC,VyAtN, dVn, dVc, tol, 'Y2Pa', 'C2N')

      call TestValues(FAtE, VxAtN, VyAtN, GAtN)
      ! Calculate
      !    GAtE = InterpY2Xa * GAtN
      !    FatN = InterpX2Ya * FAtE
      call InterpVelocity(FAtE, GAtN, FatN,GAtE, set_zero = .true., exactOut=.true. )
      ! Calculate
      !    VxAtN = InterpX2Y * VxAtE
      !    VyAtE = InterpY2X * VyAtN
      call InterpVelocity(VxAtE, VyAtN, VxAtN, VyAtE, set_zero = .true., exactIn=.true.)

      ! Testing that
      !    GAtE * VxAtE  =  FAtN * VxAtN
      !    GatN * VyAtN  =  FAtE * VyAtE
      call CheckAdjointness(FatE,VyAtN, FatN,VyAtE, dVe, dVn, tol, 'X2Y', 'Y2Xa')
      call CheckAdjointness(GatN,VxAtE, GatE,VxAtN, dVn, dVe, tol, 'Y2X', 'X2Ya')

      deallocate( VxAtE, VyAtN, GatE,  FatN, FAtE,  GAtN, stat=ierr)
      if (ierr/=0) call error_exit('deallocation error')

      print *
   end subroutine TestAdjointInterp

   subroutine TestInterp()
   use FDApplyOperators,   only: InterpV2C, InterpC2V
   use Settings,   only: myprec
   use Inputs,     only: m,n, scaleCurv
   use Util,       only: error_exit
   use TestFunctions, only: TestValues
   use Checks,        only: CheckSame
   implicit none
      real(myprec), pointer   :: vx10AtC(:), vy10AtC(:)
      real(myprec), pointer   :: vx01AtC(:), vy01AtC(:)
      real(myprec), pointer   :: vx01(:), vy01(:)
      real(myprec), pointer   :: vx10(:), vy10(:)
      real(myprec), pointer   :: vx(:), vy(:)
      integer                 :: ierr
      real(myprec)            :: tol_a
      real(myprec), parameter :: tol=1e-10

      tol_a = tol
      if (abs(scaleCurv)<1e-10) tol_a = 2e-7

      print *,'Testing EXACT interpolation of grid orientations'
      allocate( vx10AtC(0:n*m), vy10AtC(0:n*m), vx01AtC(0:n*m), vy01AtC(0:n*m), &
                vx10(0:n*m),    vy10(0:n*m),    vx01(0:n*m),    vy01(0:n*m),    &
                vx(0:n*m),      vy(0:n*m),      stat=ierr)
      if (ierr/=0) call error_exit('allocation error')

      call TestValues( vx10=vx10, vy10=vy10, vx01=vx01, vy01=vy01, &
                       vx10AtC=vx10AtC, vy10AtC=vy10AtC)
      vx01AtC = -vy10AtC
      vy01AtC =  vx10AtC

      call InterpV2C(vx10, vy10, vx, vy, set_zero=.true., exactAtC=.true.)
      call CheckSame(vx, vx10AtC, tol_a, 'vx10AtC', 'E2C*vx10')
      call CheckSame(vy, vy10AtC, tol_a, 'vy10AtC', 'N2C*vy10')

      call InterpC2V(vx10AtC, vy10AtC, vx, vy, set_zero=.true., exactAtV=.true.)
      call CheckSame(vx10, vx, tol_a, 'vx10', 'C2E*vx10AtC')
      call CheckSame(vy10, vy, tol_a, 'vy10', 'C2N*vy10AtC')

      call InterpV2C(vx01, vy01, vx, vy, set_zero=.true., exactAtC=.true.)
      call CheckSame(vx, vx01AtC, tol_a, 'vx01AtC', 'E2C*vx01')
      call CheckSame(vy, vy01AtC, tol_a, 'vy01AtC', 'N2C*vy01')

      call InterpC2V(vx01AtC, vy01AtC, vx, vy, set_zero=.true., exactAtV=.true.)
      call CheckSame(vx01, vx, tol_a, 'vx01', 'C2E*vx01AtC')
      call CheckSame(vy01, vy, tol_a, 'vy01', 'C2N*vy01AtC')

      deallocate( vx10AtC, vy10AtC, vx01AtC, vy01AtC, vx10, vy10, vx01, vy01, vx, vy, stat=ierr)
      if (ierr/=0) call error_exit('deallocation error')

      print *
   end subroutine TestInterp

   subroutine TestAdvection()
   use FDApplyOperators, only: InterpVelocity, InterpC2V, ApplyAdvectionB, ApplyAdvectionA, ApplyDivergenceDensTimes, ApplyAdvection
   use Settings,         only: myprec
   use Checks,           only: CheckSame, ScalarProductVectorFields, CheckZero, CheckMomentumFree, CheckAdjointnessVectorFields
   use Util,             only: error_exit
   use Inputs,           only: m,n, scaleCurv
   use TestFunctions,    only: TestValues
   implicit none

     ! Locals
     integer                 :: ierr
     real(myprec), pointer   :: vx(:), vy(:), fx(:), fy(:), rho(:), gx(:), gy(:)
     real(myprec), pointer   :: vx10(:), vy10(:), vx01(:), vy01(:)
     real(myprec), pointer   :: AdvecX1(:), AdvecY1(:)
     real(myprec), pointer   :: AdvecX(:), AdvecY(:), DivRV(:), DivRV1(:), DivRVatE(:), DivRVatN(:)
     real(myprec)            :: normAdvec, tol_a
     real(myprec), parameter :: tol=1e-10

     tol_a = tol
     if (abs(scaleCurv)<1e-10) tol_a = 2e-7

     allocate(rho(0:n*m), vx(0:n*m), vy(0:n*m), fx(0:n*m), fy(0:n*m), gx(0:n*m), gy(0:n*m), &
              vx10(0:n*m), vy10(0:n*m), vx01(0:n*m), vy01(0:n*m), &
              DivRV(0:n*m), DivRV1(0:n*m), DivRVAtE(0:n*m), DivRVAtN(0:n*m), &
              AdvecX(0:n*m), AdvecY(0:n*m), &
              AdvecX1(0:n*m), AdvecY1(0:n*m), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')

     call TestValues(vx=vx,vy=vy,Rho=rho, vx10=vx10, vy10=vy10, vx01=vx01, vy01=vy01)

     ! The divergence-style advection operator has the following properties:
     !     1) momentum neutral: <constant_flow , advection(rho,vx,vy) * (fx,fy)> = 0
     !     2) advecting a constant returns divergence:
     !        advection(rho,vx,vy) * constant_flow(cx,cy) = (cx,cy) .* (InterpP2Va * DIVr(rho) * (vx,vy))

     ! DivRV = DIVr(rho) * (vx,vy)
     call ApplyDivergenceDensTimes(rho,vx,vy,DivRV)

     print *,'Testing EXACT properties of advection operator'

     ! Test 1: f=(1,0)
     call ApplyAdvectionA(rho,vx,vy,vx10,vy10,DivRV1, AdvecX, AdvecY)
     call InterpC2V(DivRV, DivRV, DivRVatE, DivRVatN, set_zero=.true., exactAtC=.true.)

     ! Check that DivRV as calculated by ApplyAdvectionA is the same as that
     ! calculated by ApplyDivergenceDensTimes.
     call CheckSame(DivRV,DivRV1,tol,nameF='Div',nameG='Div''')

     ! Check that AdvectionA(rho,v) * [1,0] = [1,0] * Div(rho*v)
     call CheckSame(AdvecX, vx10*DivRVatE,tol_a, nameF='AdvecX*10', nameG='10x*DivRV')
     call CheckSame(AdvecY, vy10*DivRVatN,tol,   nameF='AdvecY*10', nameG='10y*DivRV')
     call CheckMomentumFree(AdvecX,AdvecY,tol,nameF='Advec*10')

     ! Check the grad-style advection
     call ScalarProductVectorFields(AdvecX,AdvecX,AdvecX,AdvecX, normF_=normAdvec)
     call ApplyAdvectionB(rho,vx,vy,vx10,vy10,AdvecX, AdvecY)

     ! Check that AdvectionB(rho,v) * [1,0] = [0,0]
     call CheckZero(AdvecX,tol = 1e-10*normAdvec,nameF='AdvecXA*const10')
     call CheckZero(AdvecY,tol = 1e-10*normAdvec,nameF='AdvecYA*const10')

     call CheckMomentumFree(AdvecX-vx10*DivRVAtE,AdvecY-vy10*DivRVAtN,tol,nameF='(AdvecA-diag(DivRV))*10')

     ! Test 2: f=(0,1)
     call ApplyAdvectionA(rho,vx,vy,vx01,vy01,DivRV1, AdvecX, AdvecY)

     ! Check that AdvectionA(rho,v) * [1,0] = [1,0] * Div(rho*v)
     call CheckSame(AdvecX,vx01*DivRVatE,tol,nameF='AdvecX*01',nameG='01x*DivRV')
     call CheckSame(AdvecY,vy01*DivRVatN,tol_a,nameF='AdvecY*01',nameG='01y*DivRV')
     call CheckMomentumFree(AdvecX,AdvecY,tol,nameF='Advec*01')

     ! Check the grad-style advection
     call ScalarProductVectorFields(AdvecX,AdvecX,AdvecX,AdvecX, normF_=normAdvec)
     call ApplyAdvectionB(rho,vx,vy,vx01,vy01,AdvecX, AdvecY)

     ! Check that AdvectionB(rho,v) * [1,0] = [0,0]
     call CheckZero(AdvecX,tol = 1e-10*normAdvec,nameF='AdvecXA*const01')
     call CheckZero(AdvecY,tol = 1e-10*normAdvec,nameF='AdvecYA*const01')

     call CheckMomentumFree(AdvecX-vx01*DivRVAtE,AdvecY-vy01*DivRVAtN,tol,nameF='(AdvecA-diag(DivRV))*01')

     ! Test 3: Advection f=v:
     fx = vx
     fy = vy
     call ApplyAdvectionA(rho,vx,vy,vx,vy,DivRV1, AdvecX, AdvecY)
     call ApplyAdvectionA(rho,vx,vy,fx,fy,DivRV1, AdvecX1, AdvecY1)

     ! Check that AdvectionA(rho,v) * f=v = AdvectionA(rho,v) * v
     call CheckSame(AdvecX,AdvecX1,tol,nameF='AdvecX*{f-v}',nameG='AdvecX*v')
     call CheckSame(AdvecY,AdvecY1,tol,nameF='AdvecY*{f-v}',nameG='AdvecY*v')

     ! Test 4:  f=(fx,fy), g=(gx,gy) with test functions
     call TestValues(ux=fx, uy=fy, wx=gx, wy=gy)

     ! Apply divergence operator : (AdvecX,AdvecY) = Div(rho*v*f)

     ! Check that Div-style advection is momentum-free

     call ApplyAdvectionA(rho,vx,vy,gx,gy,DivRV, AdvecX, AdvecY)
     call ApplyAdvectionB(rho,vx,vy,gx,gy, AdvecX1, AdvecY1)
     !
     ! <const , AdvecA * g> = 0

     ! AdvecB = -AdvecA'
     ! AdvecA * const = DivRV * const

     ! <g, AdvecA * const>  = g' * diag(DivRV) * diag(volQV) * const
     !
     ! <const, AdvecB * g> = g' * diag(volQV) * AdvecA
     !
     call CheckMomentumFree( AdvecX,AdvecY,tol,nameF='Advec*g')
     call CheckMomentumFree( AdvecX1+gx*DivRVAtE, &
                             AdvecY1+gy*DivRVAtN, tol, nameF='(AdvecA-diag(DivRV))*g')

     call ApplyAdvectionA(rho,vx,vy,fx,fy,DivRV, AdvecX, AdvecY)
     call CheckMomentumFree(AdvecX,AdvecY,tol,nameF='Advec')

     ! Check that Div-style and Grad-style advections are each other's adjoints
     fx = -fx
     fy = -fy
     call CheckAdjointnessVectorFields(fx,fy, gx,gy, AdvecX,AdvecY, AdvecX1,AdvecY1, tol, 'AdvecA', 'AdvecA''')

     print *
     print *,'Testing EXACT properties of combined advection operator'
     ! Test 1: f=(1,0)
     call ApplyAdvection(rho,vx,vy,vx10,vy10,DivRV1, DivRVAtE, DivRVAtN, AdvecX, AdvecY)

     ! Check that DivRV as calculated by ApplyAdvection is the same as that
     ! calculated by ApplyDivergenceDensTimes.
     call CheckSame(DivRV,DivRV1,tol,nameF='Div',nameG='Div''')

     ! Check that Advection(rho,v) * [1,0] = [1,0] * Div(rho*v)
     call CheckSame(AdvecX,vx10*DivRVatE,tol_a,nameF='AdvecX*10',nameG='10x*DivRV')
     call CheckSame(AdvecY,vy10*DivRVatN,tol,nameF='AdvecY*10',nameG='10y*DivRV')

     ! Test 2: f=(0,1)
     call ApplyAdvection(rho,vx,vy,vx01,vy01,DivRV1, DivRVAtE, DivRVAtN, AdvecX, AdvecY)

     ! Check that Advection(rho,v) * [1,0] = [1,0] * Div(rho*v)
     call CheckSame(AdvecX,vx01*DivRVatE,tol_a,nameF='AdvecX*01',nameG='01x*DivRV')
     call CheckSame(AdvecY,vy01*DivRVatN,tol_a,nameF='AdvecY*01',nameG='01y*DivRV')

     ! Test 4:  f=(fx,fy), g=(gx,gy) with test functions
     call TestValues(ux=fx, uy=fy, wx=gx, wy=gy)
     call ApplyAdvection(rho,vx,vy,fx,fy,DivRV1, DivRVAtE, DivRVAtN, AdvecX, AdvecY)
     call ApplyAdvection(rho,vx,vy,gx,gy,DivRV1, DivRVAtE, DivRVAtN, AdvecX1, AdvecY1)

     call CheckMomentumFree(AdvecX, AdvecY, tol_a, nameF='Advec*f')
     call CheckMomentumFree(AdvecX1,AdvecY1,tol,   nameF='Advec*g')

     AdvecX1 = DivRVAtE * gx  - AdvecX1
     AdvecY1 = DivRVAtN * gy  - AdvecY1

     ! Check that Div-style and Grad-style advections are each other's adjoints
     call CheckAdjointnessVectorFields(fx,fy, gx,gy, AdvecX,AdvecY, AdvecX1,AdvecY1, tol, 'AdvecA', 'AdvecA''')

     deallocate(rho, vx, vy, fx, fy, DivRV, DivRV1, DivRVAtE, DivRVAtN, AdvecX, AdvecY, AdvecX1, AdvecY1, &
                gx, gy, vx01, vy01, vx10, vy10, stat=ierr)
     if (ierr/=0) call error_exit('deallocation error')
     print *
   end subroutine TestAdvection

   subroutine TestAccuracy2()
   use FDApplyOperators, only: VxAtN, VyAtE, InterpVelocity, ApplyDivergence2
   use Settings,         only: myprec
   use Checks,           only: CheckSame
   use Util,             only: error_exit
   use TestFunctions,    only: TestValues
   use Inputs,           only: m,n
   implicit none
     integer                 :: ierr
     real(myprec), pointer   :: DivVxy(:)
     real(myprec), pointer   :: vx(:), vy(:), DivV(:), VxAtY2(:), VyAtX2(:)
     real(myprec), parameter :: tol=1e-6

     allocate( DivVxy(0:n*m-1), &
               VxAtY2(0:n*m-1), VyAtX2(0:n*m-1), &
               vx(0:n*m-1), vy(0:n*m-1), DivV(0:n*m-1), stat=ierr)
     if (ierr/=0) call error_exit('allocation error')

     call TestValues(vx=vx,vy=vy,DivV=DivVxy,VxAtN=VxAtY2,VyAtE=VyAtX2)

     print *,'Testing ACCURACY of interpolations and divergence'
     call InterpVelocity(vx,vy,VxAtN,VyAtE, set_zero=.true., exactIn=.true.)
     call CheckSame( VxAtN,VxAtY2, tol, 'X2Y*vx','VxAtN')
     call CheckSame( VyAtE,VyAtX2, tol, 'Y2X*vy','VyAtE')
     call InterpVelocity(vx,vy,VxAtN,VyAtE, set_zero=.true., exactOut=.true.)
     call CheckSame( VxAtN,VxAtY2, tol, 'X2Ya*vx','VxAtN')
     call CheckSame( VyAtE,VyAtX2, tol, 'Y2Xa*vy','VyAtE')
     call InterpVelocity(vx,vy,VxAtN,VyAtE, set_zero=.true.)
     call CheckSame( VxAtN,VxAtY2, tol, 'X2Y0*vx','VxAtN')
     call CheckSame( VyAtE,VyAtX2, tol, 'Y2X0*vy','VyAtE')

     call ApplyDivergence2(vx,vy,VxAtY2,VyAtX2,DivV)
     call CheckSame( DivV,DivVxy, tol, 'Div2*v','DivV')

     deallocate(vx,vy,DivV,DivVxy,stat=ierr)
     if (ierr/=0) call error_exit('deallocation error')
     print *
   end subroutine TestAccuracy2
end module Tests

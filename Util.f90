! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module Util
integer, save :: n_error = 0
contains

   subroutine error_exit(messag, fatal)
   implicit none
      character(len=*), intent(in) :: messag
      logical, optional, intent(in) :: fatal

      print *,'******* ERROR: ' // trim(messag)
      if (.not. present(fatal)) stop 255
      if (fatal) stop 255
      n_error = n_error + 1
   end subroutine error_exit

   subroutine end_program(prog_name)
   implicit none
      character(len=*), intent(in) :: prog_name

      if (n_error>0) then
         print '(10(a,i0))',  '******* Program '//trim(prog_name)//' ended with ',n_error,' reported errors'
         call exit(n_error)
      else
         print *,'Program '//trim(prog_name)//' ended successfully'
      end if

   end subroutine end_program

end module Util


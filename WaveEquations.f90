! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module WaveEquations
! This model provides the function WaveFun for each supported model, that
! is the input for the time integration function (e.g. wrapped_lsode)
   interface

      subroutine ApplyGradient_itf(f,GradX,GradY)
      use Settings, only: myprec
      implicit none
         real(myprec), intent(in)  :: f(0:)
         real(myprec), intent(out) :: GradX(0:), GradY(0:)
      end subroutine ApplyGradient_itf

      subroutine ApplyDivergence_itf(vx,vy,DivV)
      use Settings, only: myprec
      implicit none
         real(myprec), intent(in)  :: vx(0:), vy(0:)
         real(myprec), intent(out) :: DivV(0:)
      end subroutine ApplyDivergence_itf

   end interface

contains

   subroutine ShallowWaterWaveFun(dydt,t,y)
   use Settings,         only: myprec
   use Util,             only: error_exit
   use FDApplyOperators, only: DivRV, DivRVatE, DivRVatN, p, rhoAtE, rhoAtN, GradYP, GradXP, vx, vy, AdvecX, AdvecY
   use FDApplyOperators, only: ApplyGradient, InterpC2V, ApplyAdvection
   use Inputs,           only: m,n
   use StateEq,          only: GetP
   implicit none
      real(myprec), target, intent(out) :: dydt(0:)   ! time derivative of model state
      real(myprec),         intent(in)  :: t
      real(myprec), target, intent(in)  :: y(0:)      ! model state

      real(myprec), pointer :: rho(:), rvx(:), rvy(:)
      real(myprec), pointer :: drhodt(:), drvxdt(:), drvydt(:)
      integer :: irow
      if (.false.) dydt(1) = t ! This line is only here so 't' is not an unused argument

      ! Model state consists of density and two momentum components
      rho(0:m*n-1)  => y(    0:  m*n-1)
      rvx(0:m*n-1)  => y(  m*n:2*m*n-1)
      rvy(0:m*n-1)  => y(2*m*n:3*m*n-1)

      ! Model state time deriv consists of d/dt of density and two momentum components
      drhodt(0:m*n-1)  => dydt(    0:  m*n-1)
      drvxdt(0:m*n-1)  => dydt(  m*n:2*m*n-1)
      drvydt(0:m*n-1)  => dydt(2*m*n:3*m*n-1)

      ! Interpolate density (calculate rhoAtE and rhoAtN from rho)
      call InterpC2V(rho, rho, rhoAtE, rhoAtN, set_zero=.true., exactAtC=.true.)

      ! Calculate velocity = momentum/density
!$OMP PARALLEL DO
      do irow = 0,m*n-1
         vx(irow)  = rvx(irow) / rhoAtE(irow)
         vy(irow)  = rvy(irow) / rhoAtN(irow)
      end do
!$OMP END PARALLEL DO

      ! Apply ADVEC(rho) * v, DIVr*v and GRAD*P(rho)
      call ApplyAdvection(rho,vx,vy,vx,vy, DivRV, DivRVAtE, DivRVAtN, AdvecX, AdvecY)
      call GetP(rho,p)
      call ApplyGradient(p,GradXP,GradYP)

      ! Add minus sign
!$OMP PARALLEL DO
      do irow = 0,m*n-1
         drhodt(irow) = -DivRV(irow)
         drvxdt(irow) = -AdvecX(irow) - GradXP(irow)
         drvydt(irow) = -AdvecY(irow) - GradYP(irow)
      end do
!$OMP END PARALLEL DO

   end subroutine ShallowWaterWaveFun

   subroutine CompressibleWaveFun(dydt,t,y)
   use Util,             only: error_exit
   use Settings,         only: myprec
   use FDApplyOperators, only: ApplyGradient, ApplyDivergenceDensTimes
   use Inputs,           only: m,n
   use StateEq,          only: GetQ
   implicit none
      real(myprec), target, intent(out) :: dydt(0:)   ! time derivative of model state
      real(myprec),         intent(in)  :: t
      real(myprec), target, intent(in)  :: y(0:)      ! model state

      real(myprec), pointer :: q(:)
      real(myprec), pointer :: rho(:), vx(:), vy(:)
      real(myprec), pointer :: drhodt(:), dvxdt(:), dvydt(:)
      integer :: ierr
      integer :: irow
      if (.false.) dydt(1) = t ! This line is only here so 't' is not an unused argument

      ! Model state consists of density and two velocity components
      rho(0:m*n-1) => y(    0:  m*n-1)
      vx(0:m*n-1)  => y(  m*n:2*m*n-1)
      vy(0:m*n-1)  => y(2*m*n:3*m*n-1)

      ! Model state time deriv consists of d/dt of density and two velocity components
      drhodt(0:m*n-1) => dydt(    0:  m*n-1)
      dvxdt(0:m*n-1)  => dydt(  m*n:2*m*n-1)
      dvydt(0:m*n-1)  => dydt(2*m*n:3*m*n-1)

      ! Calculate the Q(rho) function
      allocate(q(0:m*n-1),stat=ierr)
      if (ierr/=0) call error_exit('allocation error')
      if (ierr/=0) stop 'error'
      call GetQ(rho,q)

      ! Apply operator DIVr to v, and apply GRAD to q, because
      !    d rho/dt = -DIVr v;     dv/dt = -GRAD Q(rho)        (B)
      call ApplyDivergenceDensTimes(rho,vx,vy,drhodt)
      call ApplyGradient(q,dvxdt,dvydt)

      ! Add minus sign (see (B))
!$OMP PARALLEL DO
      do irow = 0,m*n-1
         drhodt(irow) = -drhodt(irow)
         dvxdt(irow)  = -dvxdt(irow)
         dvydt(irow)  = -dvydt(irow)
      end do
!$OMP END PARALLEL DO

      deallocate(q,stat=ierr)
      if (ierr/=0) call error_exit('allocation error')

   end subroutine CompressibleWaveFun

   subroutine ScalarWaveFun(dydt,t,y)
   use CalcMatrices_Scalar, only: LaplaceMatrix
   use Inputs,   only: m,n
   use Settings, only: myprec
   implicit none
      real(myprec), target, intent(out) :: dydt(:)
      real(myprec), intent(in)  :: t
      real(myprec), target, intent(in)  :: y(:)

      real(myprec), pointer :: p(:), dpdt(:), dp_dt(:), d2pdt2(:)
      integer :: eta_col, xi_col, eta_row, xi_row
      integer :: irow, icol
      integer :: ia, nSpan
      if (.false.) dydt(1) = t ! This line is only here so 't' is not an unused argument

      nSpan = nint((sqrt(real(size(LaplaceMatrix,2))+1))/4)

      ! y = [p;dp/dt]       dy/dt = [dp/dt; d2 p /dt2] = [dp/dt; LAPL * p]
      p      =>    y(    1:  m*n)
      dp_dt  =>    y(m*n+1:2*m*n)
      dpdt   => dydt(    1:  m*n)
      d2pdt2 => dydt(m*n+1:2*m*n)


      ! Matrix-vector multiply: d2 p/ dt2 = LAPL * p
!$OMP PARALLEL DO PRIVATE (xi_row, eta_row, ia, eta_col, xi_col, icol)
      do irow = 1,m*n
         dpdt(irow) = dp_dt(irow)
         d2pdt2(irow) = 0
         xi_row  = mod(irow-1,m)
         eta_row = (irow-1)/m
         do ia = 1,(4*nSpan-1) * (4*nSpan-1)
            eta_col  = modulo(eta_row + mod(ia-1,4*nSpan-1)+1-2*nSpan,m)
            xi_col   = modulo(xi_row + (ia-1)/(4*nSpan-1)+1-2*nSpan,n)
            icol    = xi_col + m * eta_col + 1
            d2pdt2(irow) = d2pdt2(irow) + LaplaceMatrix(irow,ia) * p(icol)
         end do
      end do
!$OMP END PARALLEL DO
   end subroutine ScalarWaveFun

   subroutine LinearWaveFun(dydt,t,y,ApplyGradient,ApplyDivergence)
   use Inputs,   only: m,n
   use Settings, only: myprec
   implicit none
      real(myprec), target, intent(out) :: dydt(0:)   ! time derivative of model state
      real(myprec),         intent(in)  :: t
      real(myprec), target, intent(in)  :: y(0:)      ! model state
      procedure (ApplyGradient_itf)   :: ApplyGradient
      procedure (ApplyDivergence_itf) :: ApplyDivergence

      real(myprec), pointer :: p(:), vx(:), vy(:)
      real(myprec), pointer :: dpdt(:), dvxdt(:), dvydt(:)
      integer :: irow
      if (.false.) dydt(1) = t ! This line is only here so 't' is not an unused argument

      ! Model state consists of pressure and two velocity components
      p( 0:m*n-1)  => y(    0:  m*n-1)
      vx(0:m*n-1)  => y(  m*n:2*m*n-1)
      vy(0:m*n-1)  => y(2*m*n:3*m*n-1)

      ! Model state time deriv consists of d/dt of pressure and two velocity components
      dpdt( 0:m*n-1)  => dydt(    0:  m*n-1)
      dvxdt(0:m*n-1)  => dydt(  m*n:2*m*n-1)
      dvydt(0:m*n-1)  => dydt(2*m*n:3*m*n-1)


      ! Calculate gradient and divergence. These are almost the time derivatives, because
      !    dp/dt = -DIV * v ;        dv/dt = - GRAD p         (A)
      call ApplyDivergence(vx,vy,dpdt)
      call ApplyGradient(p,dvxdt,dvydt)

      ! Add minus sign (see (A))
!$OMP PARALLEL DO
      do irow = 0,m*n-1
         dpdt(irow)  = -dpdt(irow)
         dvxdt(irow) = -dvxdt(irow)
         dvydt(irow) = -dvydt(irow)
      end do
!$OMP END PARALLEL DO
   end subroutine LinearWaveFun

   subroutine LinearGalerkinWaveFun(dydt,t,y)
   use Settings,             only: myprec
   use LinearWaveEqGalerkin, only: ApplyGradient, ApplyDivergence
   implicit none
      real(myprec), target, intent(out) :: dydt(0:)   ! time derivative of model state
      real(myprec),         intent(in)  :: t
      real(myprec), target, intent(in)  :: y(0:)      ! model state

      call LinearWaveFun(dydt,t,y, ApplyGradient, ApplyDivergence)
   end subroutine LinearGalerkinWaveFun

   subroutine LinearFDWaveFun(dydt,t,y)
   use Settings,         only: myprec
   use FDApplyOperators, only: ApplyGradient, ApplyDivergence
   implicit none
      real(myprec), target, intent(out) :: dydt(0:)   ! time derivative of model state
      real(myprec),         intent(in)  :: t
      real(myprec), target, intent(in)  :: y(0:)      ! model state

      call LinearWaveFun(dydt,t,y, ApplyGradient, ApplyDivergence)
   end subroutine LinearFDWaveFun

end module WaveEquations


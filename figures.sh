#!/bin/bash

# Usage:
#      ./figures.sh
# Run the simulations needed for the graphs in the article.
export MAKE_TYPE=release 
make -j all 

if [ 1 = 1 ]; then
   dir=fig_in_shallow_unif
   rm -rf *.asc $dir
   ./Simulate_${MAKE_TYPE}.exe -equation shallow -Tderiv 2 -Tdestagger 2 -nx 80 -scaleCurv 0 -relTol 1e-11 -save_snapshots yes
   mkdir $dir
   mv *.asc $dir/ 

   dir=fig_in_shallow_curv
   rm -rf *.asc $dir
   ./Simulate_${MAKE_TYPE}.exe -equation shallow -Tderiv 2 -Tdestagger 2 -nx 80 -relTol 1e-11 -save_snapshots yes
   mkdir $dir
   mv *.asc $dir/ 
fi

if [ 1 = 1 ]; then
   dir=fig_in_compressible_unif
   rm -rf *.asc $dir
   ./Simulate_${MAKE_TYPE}.exe -equation compressible  -Tderiv 2 -Tdestagger 2 -nx 40 -scaleCurv 0 -relTol 1e-11 -save_snapshots yes
   mkdir $dir
   mv *.asc $dir/ 
fi

if [ 1 = 1 ]; then
   dir=fig_in_scalar_unif 
   rm -rf *.asc $dir
   ./Simulate_${MAKE_TYPE}.exe -equation scalar -Taylor 4 -nx 20 -scaleCurv 0 -relTol 1e-11 -save_snapshots yes
   mkdir $dir
   mv *.asc $dir/ 
   
   dir=fig_in_scalar_curv
   rm -rf *.asc $dir
   ./Simulate_${MAKE_TYPE}.exe -equation scalar -Taylor 4 -nx 20 -relTol 1e-11 -save_snapshots yes
   mkdir $dir
   mv *.asc $dir/ 
fi



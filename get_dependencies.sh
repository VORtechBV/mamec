#!/bin/bash
#
# Usage: 
#     make depend
# Fill in dependencies for Makefile
#
for f90file in *.f90
do
   Obj=`echo $f90file | sed 's/\.f90/_$(MAKE_TYPE).o/'`
   mods=''
   for mod in `grep '^ *use\>' -ir $f90file | sed 's/,/ /' | awk '{print $2}' | sort | uniq`
   do
      mods="$mods "`grep -il "^ *module $mod\>" *.f90 | \
                       grep -v "\<$f90file\>" | \
                          sed 's/\.f90/_$(MAKE_TYPE).o/'`
   done
   echo "$Obj:	$mods"
done


! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
module lsode_wrapper
! This module makes ODEPACK's subroutine LSODE accessible through a more modern interface.
   private

   public:: wrapped_dlsode, print_dlsode_diagnostics

   integer, save :: nSteps, nEval
   double precision, save :: tStart, tEnd

   interface

      subroutine dlsode (f, neq, y, t, tout, itol, rtol, atol, itask, istate, iopt, rwork, lrw, iwork, liw, jac, mf)
      implicit none
         external                        :: f       ! Subroutine for right-hand-side vector f.
         integer,          intent(in)    :: neq     ! The size of the ODE system (number of first-order ordinary differential equations).
                                                    ! Used only for input.  NEQ may be decreased, but not increased, during the problem.
                                                    ! If NEQ is decreased (with ISTATE = 3 on input), the remaining components of Y should
                                                    ! be left undisturbed, if these are to be accessed in F and/or JAC.
                                                    !
                                                    ! Normally, NEQ is a scalar, and it is generally referred to as a scalar in this user
                                                    ! interface description.  However, NEQ may be an array, with NEQ(1) set to the system size.
                                                    ! (The DLSODE package accesses only NEQ(1).) In either case, this parameter is passed as the
                                                    ! NEQ argument in all calls to F and JAC.  Hence, if it is an array, locations NEQ(2),...
                                                    ! may be used to store other integer data and pass it to F and/or JAC.  Subroutines
                                                    ! F and/or JAC must include NEQ in a DIMENSION statement in that case.
         double precision, intent(inout) :: y(*)    ! Array of values of the y(t) vector, of length NEQ.
                                                    !   Input:  For the first call, Y should contain the values of y(t) at t = T.
                                                    !           (Y is an input variable only if ISTATE = 1.)
                                                    !   Output: On return, Y will contain the values at the new t-value.
         double precision, intent(inout) :: t       ! The independent variable.  On input, T is used only on the first call,
                                                    ! as the initial point of the integration.
                                                    ! On output, after each call, T is the value at which a computed solution Y is evaluated
                                                    ! (usually the same as TOUT).  On an error return, T is the farthest point reached.
         double precision, intent(in)    :: tout    ! Next point where output is desired (.NE. T).
         integer,          intent(in)    :: itol    ! 1, 2, 3 or 4 according as
                                                    !    ATOL (below) is a scalar (1,3) or an array (2,4)
                                                    !    RTOL (below) is a scalar (1,2) or an array (3,4)
         double precision, intent(in)    :: rtol(*) ! Relative tolerance parameter (see itol).
         double precision, intent(in)    :: atol(*) ! Absolute tolerance parameter (see itol).
                                                    ! The estimated local error in Y(i) will be controlled so as to be roughly less (in magnitude) than
                                                    !    EWT(i) = RTOL*ABS(Y(i)) + ATOL     if ITOL = 1, or
                                                    !    EWT(i) = RTOL*ABS(Y(i)) + ATOL(i)  if ITOL = 2.
                                                    ! Thus the local error test passes if, in each component, either the absolute error is less than
                                                    ! ATOL (or ATOL(i)), or the relative error is less than RTOL.
                                                    !
                                                    ! Use RTOL = 0.0 for pure absolute error control, and
                                                    ! use ATOL = 0.0 (or ATOL(i) = 0.0) for pure relative error control.
                                                    ! Caution:  Actual (global) errors may exceed these local tolerances, so choose them conservatively.
         integer,          intent(in)    :: itask   ! Flag indicating the task DLSODE is to perform.
                                                    ! Use ITASK = 1 for normal computation of output values of y at t = TOUT.
                                                    !  1  Normal computation of output values of y(t) at t = TOUT (by overshooting and interpolating).
                                                    !  2  Take one step only and return.
                                                    !  3  Stop at the first internal mesh point at or beyond t = TOUT and return.
                                                    !  4  Normal computation of output values of y(t) at t = TOUT but without overshooting t = TCRIT.
                                                    !     TCRIT must be input as RWORK(1).
                                                    !     TCRIT may be equal to or beyond TOUT, but not behind it in the direction of integration.
                                                    !     This option is useful if the problem has a singularity at or beyond t = TCRIT.
                                                    !  5  Take one step, without passing TCRIT, and return.  TCRIT must be input as RWORK(1).
        integer,           intent(inout) :: istate  ! Index used for input and output to specify the state of the calculation.
                                                    ! Input:
                                                    !  1   This is the first call for a problem.
                                                    !  2   This is a subsequent call.
                                                    ! Output:
                                                    !  1   Nothing was done, because TOUT was equal to T.
                                                    !  2   DLSODE was successful (otherwise, negative).
                                                    !      Note that ISTATE need not be modified after a successful return.
                                                    ! -1   Excess work done on this call (perhaps wrong MF).
                                                    ! -2   Excess accuracy requested (tolerances too small).
                                                    ! -3   Illegal input detected (see printed message).
                                                    ! -4   Repeated error test failures (check all inputs).
                                                    ! -5   Repeated convergence failures (perhaps bad Jacobian supplied or wrong choice of MF or tolerances).
                                                    ! -6   Error weight became zero during problem (solution component i vanished, and ATOL or ATOL(i) = 0.).
        integer,           intent(in)    :: iopt    !  Flag indicating whether optional inputs are used:
                                                    !  0   No
                                                    !  1   Yes.  (See "Optional inputs" under "Long Description," Part 1.)
        double precision                 :: rwork(*)! Real work array of length at least:
                                                    !     20 + 16*NEQ                    for MF = 10,
                                                    !     22 +  9*NEQ + NEQ**2           for MF = 21 or 22,
                                                    !     22 + 10*NEQ + (2*ML + MU)*NEQ  for MF = 24 or 25.
        integer,           intent(in)    :: lrw     ! Declared length of RWORK (in user's DIMENSION statement).
        integer                          :: iwork(*)! Integer work array of length at least:
                                                    !           20        for MF = 10,
                                                    !           20 + NEQ  for MF = 21, 22, 24, or 25.
                                                    !
                                                    ! If MF = 24 or 25, input in IWORK(1),IWORK(2) the lower and upper Jacobian half-bandwidths ML,MU.
                                                    !
                                                    ! On return, IWORK contains information that may be of interest to the user:
                                                    !
                                                    !    Name   Location   Meaning
                                                    !    -----  ---------  -----------------------------------------
                                                    !    NST    IWORK(11)  Number of steps taken for the problem so far.
                                                    !    NFE    IWORK(12)  Number of f evaluations for the problem so far.
                                                    !    NJE    IWORK(13)  Number of Jacobian evaluations (and of matrix LU decompositions)
                                                    !                      for the problem so far.
                                                    !    NQU    IWORK(14)  Method order last used (successfully).
                                                    !    LENRW  IWORK(17)  Length of RWORK actually required.
                                                    !                      This is defined on normal returns and on an illegal input return for
                                                    !                      insufficient storage.
                                                    !    LENIW  IWORK(18)  Length of IWORK actually required.
                                                    !                      This is defined on normal returns and on an illegal input return for
                                                    !                      insufficient storage.
         integer, intent(in)             :: liw     ! Declared length of IWORK (in user's DIMENSION statement).
         external                        :: Jac     ! Subroutine for Jacobian matrix (MF = 21 or 24).  If used, this name must be declared
                                                    ! EXTERNAL in calling program.  If not used, pass a dummy name.  The form of JAC must be:
                                                    !     SUBROUTINE JAC (NEQ, T, Y, ML, MU, PD, NROWPD)
                                                    !     INTEGER  NEQ, ML, MU, NROWPD
                                                    !     DOUBLE PRECISION  T, Y(*), PD(NROWPD,*)
                                                    ! See item c, under "Description" below for more information about JAC.
         integer, intent(in)             :: mf      ! Method flag.  Standard values are:
                                                    !   10  Nonstiff (Adams) method, no Jacobian used.
                                                    !   21  Stiff (BDF) method, user-supplied full Jacobian.
                                                    !   22  Stiff method, internally generated full Jacobian.
                                                    !   24  Stiff method, user-supplied banded Jacobian.
                                                    !   25  Stiff method, internally generated banded Jacobian.
      end subroutine dlsode

      ! Format of the ODE-function that returns the time derivative dy/dt given the time t and the state y
      subroutine odefun_itf(dydt,t,y)
      use Settings, only: myprec
      implicit none
         real(myprec), target, intent(out) :: dydt(0:)
         real(myprec),         intent(in)  :: t
         real(myprec), target, intent(in)  :: y(0:)
      end subroutine odefun_itf

      subroutine ProcessFunction_itf(y, time)
      use Settings, only: myprec
      implicit none
         real(myprec), target,       intent(in) :: y(0:)
         real(myprec),               intent(in) :: time
      end subroutine ProcessFunction_itf

   end interface

   procedure (odefun_itf), pointer :: current_odefun => null()

contains

      subroutine print_dlsode_diagnostics(fid)
      use Settings, only: STDOUT
      implicit none
         integer, optional, intent(in) :: fid
         integer :: fjd
         fjd = STDOUT
         if (present(fid)) fjd = fid
         write(fjd,'(2(a,i0),(1p,a,e10.2),0p,a,f0.2)') &
            '   nSteps=',nSteps,',  nEval=',nEval,', avg time step=',(tEnd-tStart)/nSteps,',' // &
            ' avg eval/step=',dble(nEval)/dble(nSteps)
      end subroutine print_dlsode_diagnostics

      subroutine no_jac(neq, t, y, ml, mu, pd, nrowpd)
      use Util,     only: error_exit
      implicit none
         integer, intent(in)           :: neq, ml, mu, nrowpd
         double precision, intent(in)  :: t
         double precision, intent(in)  :: y(*)
         double precision, intent(out) :: pd(nrowpd,*)

         call error_exit('This Jacobean function is empty')
         pd(1,1) = ml + mu + neq + t + y(1)
      end subroutine no_jac

      subroutine  f(neq, t, y, ydot)
      use Util,     only: error_exit
      implicit none
         integer, intent(in)           :: neq
         double precision, intent(in)  :: t
         double precision, target, intent(in)  :: y(*)
         double precision, target, intent(out) :: ydot(*)

         double precision, pointer :: y1(:), y1dot(:)

         y1(0:neq-1)    => y(1:neq)
         y1dot(0:neq-1) => ydot(1:neq)
         if (.not. associated(current_odefun)) call error_exit('no odefun has been set')
         call current_odefun(y1dot,t,y1)
      end subroutine f

      subroutine wrapped_dlsode(odefun, y0, t0, tout, yout, dtmax, rel_tol, use_norm, abs_tol, rel_tols, abs_tols, ProcessFunction)
      use Settings, only: myprec, NORM_TWO, NORM_INF
      use Settings, only: myprec
      use Util,     only: error_exit
      implicit none

         ! Arguments
         procedure (odefun_itf)             :: odefun
         real(myprec),           intent(in) :: y0(:)
         real(myprec),           intent(in) :: t0
         real(myprec),           intent(in) :: tout(:)
         real(myprec), pointer              :: yout(:,:) ! results

         real(myprec), optional, intent(in) :: dtmax    ! maximum time step
         real(myprec), optional, intent(in) :: rel_tol
         integer,      optional, intent(in) :: use_norm ! NORM_TWO or NORM_INF: norm to use in error evaluation
         real(myprec), optional, intent(in) :: abs_tol
         real(myprec), optional, target, intent(in) :: rel_tols(:)
         real(myprec), optional, target, intent(in) :: abs_tols(:)
         procedure (ProcessFunction_itf),  optional :: ProcessFunction

         ! Locals
         integer  :: neq, ntout
         integer  :: ierr
         integer  :: itol

         real(myprec), pointer     :: y(:)

         real(myprec), parameter   :: USE_DEFAULT = 0d0
         real(myprec), parameter   :: dtInit = USE_DEFAULT, dtMaxDefault = USE_DEFAULT, dtMinDefault = USE_DEFAULT
         integer,      parameter   :: itask = 1, iopt = 1, liw = 20, mf = 10
         integer,      parameter   :: nStepsMax = 1e6, MaxOrd = int(USE_DEFAULT), nWarnMax = int(USE_DEFAULT)
         integer                   :: lrw
         real(myprec), allocatable :: rwork(:)
         real(myprec), pointer     :: atol(:), rtol(:)
         integer                   :: istate, iwork(20)
         integer                   :: it
         double precision          :: t

         if (present(use_norm)) then
            if (use_norm == NORM_TWO) call error_exit('Only INF-norm error control is supported')
         end if

         itol = 1
         if (.not. present(abs_tols)  .and. .not. present(rel_tols))  itol = 1
         if (      present(abs_tols)  .and. .not. present(rel_tols))  itol = 2
         if (.not. present(abs_tols)  .and.       present(rel_tols))  itol = 3
         if (      present(abs_tols)  .and.       present(rel_tols))  itol = 4
         if (present(abs_tol) .and. present(abs_tols)) call error_exit('specify only abs_tol or abs_tols, not both')
         if (present(rel_tol) .and. present(rel_tols)) call error_exit('specify only rel_tol or rel_tols, not both')
         if (.not. present(abs_tol) .and. .not. present(abs_tols) .and. &
             .not. present(rel_tol) .and. .not. present(rel_tols)) call error_exit('specify at least one of rel_tol, rel_tols, abs_tol or abs_tols')

         if (present(abs_tols)) then
            atol => abs_tols
         else
            allocate(atol(1), stat=ierr)
            if (ierr/=0) call error_exit('Allocation error')
            if (present(abs_tol)) then
               atol(1) = abs_tol
            else
               atol(1) = 0d0
            end if
         end if
         if (present(rel_tols)) then
            rtol => rel_tols
         else
            allocate(rtol(1), stat=ierr)
            if (ierr/=0) call error_exit('Allocation error')
            if (present(rel_tol)) then
               rtol(1) = rel_tol
            else
               rtol(1) = 0d0
            end if
         end if

         neq = size(y0,1)
         ntout = size(tout,1)
         current_odefun => odefun
         if (associated(yout)) then
            if (size(yout,1)/=neq .or. size(yout,2)/=ntout) then
               print '(10(a,i0))', 'yout(',lbound(yout,1),':',ubound(yout,1),',',lbound(yout,2),':',ubound(yout,2),')'
               print '(10(a,i0))', 'y0(',lbound(y0,1),':',ubound(y0,1),')'
               print '(10(a,i0))', 'tout(',lbound(tout,1),':',ubound(tout,1),')'
               call error_exit('illegal dimensions')
            end if
         else
            allocate(yout(neq,ntout),stat=ierr)
            if (ierr/=0) call error_exit('allocation error')
         end if

         if (mf==10) then
            lrw = 20 + 16*neq
         else if (mf == 21 .or. mf == 22) then
            lrw = 22 +  9*NEQ + NEQ**2
         else
            lrw = 22 + 10*neq ! + (2*ml + mu)*neq
         end if
         allocate(rwork(lrw), stat=ierr)
         if (ierr/=0) call error_exit('Allocation error rwork')

         rwork(5) = (tout(1)-t0)/1000
         rwork(6) = dtMaxDefault
         rwork(7) = dtMinDefault
         rwork(8:10) = USE_DEFAULT
         if (present(dtmax)) rwork(6) = dtmax

         iwork(5) = MaxOrd
         iwork(6) = nStepsMax
         iwork(7) = nWarnMax
         iwork(8:10) = int(USE_DEFAULT)

         nSteps = 0
         nEval  = 0
         tStart = t0

         yout(:,1) = y0
         t         = t0
         istate    = 1
         do it = 1,size(tout,1)
            y(0:) => yout(:,it)
            if (it>1) y(:) = yout(:,it-1)
            tEnd = tout(it)
            call dlsode(f, neq, y, t, tEnd, itol, rtol, atol, itask, istate, iopt, rwork, lrw, iwork, liw, no_jac, mf)
            if (istate<0) call error_exit('Error from dlsode')
            nSteps = iwork(11)
            nEval  = iwork(12)
            call print_dlsode_diagnostics()
            if (present(ProcessFunction)) call ProcessFunction(y,t)
         end do

         if (.not. present(abs_tols)) then
            deallocate(atol, stat=ierr)
            if (ierr/=0) call error_exit('Deallocation error atol')
         end if
         if (.not. present(rel_tols)) then
            deallocate(rtol, stat=ierr)
            if (ierr/=0) call error_exit('Deallocation error rtol')
         end if
         deallocate(rwork, stat=ierr)
         if (ierr/=0) call error_exit('Deallocation error rwork')

      end subroutine wrapped_dlsode

end module lsode_wrapper

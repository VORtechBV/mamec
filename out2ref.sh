#!/bin/bash
#
# Usage: 
#              ./out2ref > references.f90
#
# Create a new version of module LocalReferences.
# When you know that current solutions are correct, run this script and then
# compare the file 
#    git difftool ./references.f90 
# because the script is not perfect.
#
# The reference solutions are usen in the test 
#     make local

function one_output (){

cat $1 | sed -e 's/=/ == /' -e 's/) *$/ )/' -e 's/,/ /' | awk -v it=0 '
{
  if ($1 == "Solutions") {
     if ($3=="scalar") {
        print "      if (eqident == eqSCALAR .and. &";
     } else if ($3=="linearGalerkin") {
        print "      if (eqident == eqLINEARGALERKIN .and. &";
     } else if ($3=="linearFD") {
        print "      if (eqident == eqLINEARFD .and. &";
     } else if ($3=="compressible") {
        print "      if (eqident == eqCOMPRESSIBLE .and. &";
     } else if ($3=="shallow") {
        print "      if (eqident == eqSHALLOW .and. &";
     }
  }
  if ($2 == "==" && $1 != "t") {
     if ($1 == "correction") {
        print "       "$0") then"
     } else {
        print "       "$0" .and. &"
     }
  }
  if ($1 == "t" && $4 == "relerr") {
     print "         if (it == "it") then"
     print "            found = .true."
     print "            ref(1) = "$3
     print "            ref(2) = "$6
     print "            ref(3) = "$8
     print "            ref(4) = "$9
  }                
  if ($1 == "t" && $4 == "abserr") {
     print "            ref(5) = "$6
     print "            ref(6) = "$8
     print "            ref(7) = "$9
     print "         end if"
     it = it + 1;
  }                
}'
echo "      end if"
}

echo "
module localReferences
contains
   subroutine GetReferences(it, ref, found)
   use Settings, only: myprec
   use Inputs, only: m, nSpan, eqident, eqSCALAR, eqLINEARGALERKIN, eqLINEARFD, eqCOMPRESSIBLE, eqSHALLOW, &
                     Pint => P, ScaleCurv_val => ScaleCurv, rel_tol
   implicit none
      integer,      intent(in) :: it
      real(myprec), intent(out) :: ref(:)
      logical,      intent(out) :: found
      character(len=32) :: wavelet, destagger, deriv, reltol, P, ScaleCurv, correction

      found = .false.
      if (eqident == eqSCALAR .or. eqident == eqLINEARGALERKIN) then
         write(wavelet,'(a,i0)') 'Taylor',nSpan
         destagger  =  'not set'
         deriv      =  'not set'
      else 
         wavelet    =  'not set'
         write(destagger,'(a,i0)') 'Tdestagger',nSpan
         write(deriv,'(a,i0)') 'Tderiv',nSpan
      end if
      correction =  'Corr'
      write(reltol,'(e12.3)') rel_tol
      write(P,'(i0)') Pint
      write(ScaleCurv,'(f5.3)') ScaleCurv_val
          
"
one_output errlocal/scalar.023.Taylor3.out          
one_output errlocal/linearGalerkin.025.Taylor3.out  
one_output errlocal/linearFD.027.Tdestagger3.Tderiv3.out      
one_output errlocal/compressible.029.Tdestagger3.Tderiv3.out  
one_output errlocal/shallow.031.Tdestagger3.Tderiv3.out 
echo "
   end subroutine GetReferences
end module localReferences"


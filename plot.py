#!/usr/bin/env python3

#
#  Usage: 
#     ./figures.sh && ./plot.py
#  Create pdfs for the graphs in the article.
#
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Settings:
m         = 20        # Size of the grid 
plot_type = 'plot'    # One of 'plot' or 'surf'
tfirst    = 0.999455  # first time >0 for which output exists
tlast     = 0.248464
tlast     = 2.184195  # last time >0 for which output exists 
tlast     = 10
tlast     = 0.999919
var       = 'rho'     # one of 'p', 'rho', 'vx' or 'vy': variable to plot
do_initial= True     # plots of initial conditions
do_dinitial= False    # plots of initial derivative
#------------------------------------------------------------

def make_plots( m=20, plot_type='plot', time=10.000000, var='rho', dirnam=None):

   if dirnam is None:
      dirnam='.'

   do_surf = plot_type == 'surf'
   n = m
   gridname='%dx%d' % (m,n)
   
   grid = np.loadtxt(dirnam + '/grid_'+gridname+'.asc')
   
   x = grid[:,0].reshape((m,n))
   y = grid[:,1].reshape((m,n))
   
  
   fig,ax = plt.subplots()
   xx = np.concatenate(  ( np.concatenate( (x-1,x,x+1), axis=1), 
                           np.concatenate( (x-1,x,x+1), axis=1),
                           np.concatenate( (x-1,x,x+1), axis=1) ), axis=0)

   yy = np.concatenate(  ( np.concatenate( (y-1,y-1,y-1), axis=1), 
                           np.concatenate( (y  ,y  ,y  ), axis=1),
                           np.concatenate( (y+1,y+1,y+1), axis=1) ), axis=0)
   plt.plot(x,y,'k',linewidth=2)
   plt.plot(x.transpose(),y.transpose(),'k',linewidth=2)
   plt.plot(xx,yy,'k',linewidth=0.2)
   plt.plot(xx.transpose(),yy.transpose(),'k',linewidth=0.2)
   plt.title('')
   plt.ylim(-0.5,1.5)
   plt.xlim(-0.5,1.5)
   ax.set_frame_on(False)
   ax.set_aspect('equal')
   plt.savefig(dirnam + '/grid.pdf')

   do_initial = False
   if do_initial:
      fig,ax = plt.subplots()
      t = 0
      vx = np.loadtxt(var + '_exact_'+gridname+'_%0.6f.asc' % t).reshape((m,n))
      if do_surf:
         surf = plt.pcolor(x, y, vx)
         plt.colorbar()
         plt.xlabel('x [m]'); plt.ylabel('y [m]')
      else:
         surf = ax.plot(x-y, vx,'b')
         plt.xlabel('x-y [m]');
         if var == 'rho':
            plt.ylabel('density [kg/(m*m)]')
         elif var== 'p':
            plt.ylabel('pressure [Pa]')
         ax.legend( (surf[0],),('solution',))
         # plt.ylim(2.4,2.8)
      plt.title('initial solution '+var)
      plt.savefig('Initial_'+var+'.pdf', bbox_inches='tight')
      
      
      plt.figure()
      t = 0
      dvx = np.loadtxt(var+'_ddt_'+gridname+'_%0.6f.asc' % t).reshape((m,n))
      if do_surf:
         surf = plt.pcolor(x, y, dvx)
         plt.colorbar()
      else:
         surf = plt.plot(x-y, dvx,'b')
      plt.title('initial time derivative')
   
      if do_dinitial:
         if do_surf:
            plt.figure()
         t = tfirst
         vx1 = np.loadtxt(var+'_exact_'+gridname+'_%0.6f.asc' % t).reshape((m,n))
         if do_surf:
            surf = plt.pcolor(x, y, (vx1-vx)/t)
            plt.colorbar()
            plt.title('initial FD time derivative of exact solution')
         else:
            plt.plot(x-y, (vx1-vx)/t,'r')
   
   fig,ax = plt.subplots()
   vx_end = np.loadtxt(dirnam + '/' +var+ '_' + gridname+'_%0.6f.asc' % time).reshape((m,n))
   if 'scalar' in dirnam:
      vx_end = vx_end.transpose()

   if do_surf:
      surf = plt.pcolor(x, y, vx_end)
      plt.colorbar()
      # plt.title(var+(' at time=%0.6f' % time))
      plt.xlabel('x [m]'); plt.ylabel('y [m]')
      plt.savefig(dirnam+'/'+var+('_at_time_%0.6f.pdf'%time), bbox_inches='tight')
   
      plt.figure()
      vx_ex = np.loadtxt(dirnam + '/' + var+'_exact_'+gridname+'_%0.6f.asc' % time).reshape((m,n))
      if 'scalar' in dirnam:
         vx_ex = vx_ex.transpose()
      surf = plt.pcolor(x, y, vx_end-vx_ex)
      plt.colorbar()
      # plt.title('Error in '+var+(' at time=%0.6f'%time))
      plt.xlabel('x [m]'); plt.ylabel('y [m]')
      plt.savefig(dirnam+'/d'+var+('_at_time_%0.6f.pdf'%time), bbox_inches='tight')
   else:
      line1=ax.plot(x-y, vx_end,'r')
      plt.xlabel('x-y [m]'); 
      if var == 'rho':
         plt.ylabel('density [kg/(m*m)]')
      elif var== 'p':
         plt.ylabel('pressure [Pa]')
      vx_exact = np.loadtxt(dirnam + '/'+var+'_exact_'+gridname+'_%0.6f.asc' % time).reshape((m,n))
      if 'scalar' in dirnam:
         vx_exact = vx_exact.transpose()
      line2=ax.plot(x-y, vx_exact,'b')
      ax.legend((line1[0],line2[0]),('approximation','solution'))
      # plt.ylim(2.4,2.8)
      # plt.title(var+(' at time=%0.6f' % time))
      plt.savefig(dirnam+'/'+var+('_at_time_%0.6f.pdf'%time), bbox_inches='tight')

      if False:
          plt.figure()
          plt.plot(x-y, vx_end-vx_exact,'b')
          # plt.ylim(2.4,2.8)
          # plt.title('Error in '+var+(' at time=%0.6f' % time))
          plt.savefig(dirnam+'/d'+var+('_at_time_%0.6f.pdf'%time), bbox_inches='tight')
   
make_plots(m=20, plot_type='surf', time=10.000000, var='p', dirnam='fig_in_scalar_curv')
plt.close('all')
make_plots(m=20, plot_type='plot', time=10.000000, var='p', dirnam='fig_in_scalar_unif')
plt.close('all')
make_plots(m=80, plot_type='surf', time=0.248463, var='rho', dirnam='fig_in_shallow_curv')
plt.close('all')
make_plots(m=80, plot_type='plot', time=0.248463, var='rho', dirnam='fig_in_shallow_unif')
plt.close('all')
make_plots(m=40, plot_type='plot', time=0.000000, var='rho', dirnam='fig_in_compressible_unif')
plt.close('all')
make_plots(m=40, plot_type='plot', time=2.184181, var='rho', dirnam='fig_in_compressible_unif')
plt.close('all')


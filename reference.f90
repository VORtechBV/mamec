! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik

module localReferences
contains
   subroutine GetReferences(it, ref, found)
   use Settings, only: myprec
   use Inputs, only: m, nSpan, eqident, eqSCALAR, eqLINEARGALERKIN, eqLINEARFD, eqCOMPRESSIBLE, eqSHALLOW, &
                     Pint => P, ScaleCurv_val => ScaleCurv, rel_tol
   implicit none
      integer,      intent(in) :: it
      real(myprec), intent(out) :: ref(:)
      logical,      intent(out) :: found

      character(len=32)  :: wavelet, destagger, deriv, reltol, P, ScaleCurv, correction
      logical, parameter :: verbose=.false.

      found = .false.
      if (eqident == eqSCALAR .or. eqident == eqLINEARGALERKIN) then
         write(wavelet,'(a,i0)') 'Taylor',nSpan
         destagger  =  'not set'
         deriv      =  'not set'
      else
         wavelet    =  'not set'
         write(destagger,'(a,i0)') 'Tdestagger',nSpan
         write(deriv,'(a,i0)') 'Tderiv',nSpan
      end if
      correction =  'Corr'
      write(reltol,'(e12.3)') rel_tol
      reltol = adjustl(reltol)
      write(P,'(i0)') Pint
      write(ScaleCurv,'(f5.3)') ScaleCurv_val

      if (verbose) then
         print *,'m=',m
         print *,'wavelet=',wavelet
         print *,'destagger=',destagger
         print *,'deriv=',deriv
         print *,'reltol=',reltol
         print *,'P=',P
         print *,'ScaleCurv=',ScaleCurv
         print *,'corection=',correction
      end if
      if (eqident == eqSCALAR .and. &
          m          ==  23 .and. &
          wavelet    ==  "Taylor3" .and. &
          reltol     ==  "0.100E-07" .and. &
          P          ==  "5" .and. &
          ScaleCurv  ==  "1.000" .and. &
          correction ==  "Corr") then
         if (it == 0) then
            found = .true.
            ref(1) = .000
            ref(2) = 0.000E+00
            ref(3) = 2.988E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 9.624E+00
            ref(7) = 0.000E+00
         end if
         if (it == 1) then
            found = .true.
            ref(1) = 1.000
            ref(2) = 0.000E+00
            ref(3) = 3.344E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 1.191E+01
            ref(7) = 0.000E+00
         end if
         if (it == 2) then
            found = .true.
            ref(1) = 2.000
            ref(2) = 0.000E+00
            ref(3) = 4.133E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 1.305E+01
            ref(7) = 0.000E+00
         end if
         if (it == 3) then
            found = .true.
            ref(1) = 3.000
            ref(2) = 0.000E+00
            ref(3) = 2.287E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 7.498E+00
            ref(7) = 0.000E+00
         end if
         if (it == 4) then
            found = .true.
            ref(1) = 4.000
            ref(2) = 0.000E+00
            ref(3) = 4.482E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 1.401E+01
            ref(7) = 0.000E+00
         end if
         if (it == 5) then
            found = .true.
            ref(1) = 5.000
            ref(2) = 0.000E+00
            ref(3) = 2.506E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 7.492E+00
            ref(7) = 0.000E+00
         end if
         if (it == 6) then
            found = .true.
            ref(1) = 6.000
            ref(2) = 0.000E+00
            ref(3) = 3.833E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 1.300E+01
            ref(7) = 0.000E+00
         end if
         if (it == 7) then
            found = .true.
            ref(1) = 7.000
            ref(2) = 0.000E+00
            ref(3) = 3.720E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 1.126E+01
            ref(7) = 0.000E+00
         end if
         if (it == 8) then
            found = .true.
            ref(1) = 8.000
            ref(2) = 0.000E+00
            ref(3) = 2.615E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 1.013E+01
            ref(7) = 0.000E+00
         end if
         if (it == 9) then
            found = .true.
            ref(1) = 9.000
            ref(2) = 0.000E+00
            ref(3) = 4.463E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 1.423E+01
            ref(7) = 0.000E+00
         end if
         if (it == 10) then
            found = .true.
            ref(1) = 10.000
            ref(2) = 0.000E+00
            ref(3) = 2.216E-02
            ref(4) = 0.000E+00
            ref(5) = 0.000E+00
            ref(6) = 5.668E+00
            ref(7) = 0.000E+00
         end if
      end if
      if (eqident == eqLINEARGALERKIN .and. &
          m          ==  25 .and. &
          wavelet    ==  "Taylor3" .and. &
          reltol     ==  "0.100E-07" .and. &
          P          ==  "5" .and. &
          ScaleCurv  ==  "1.000" .and. &
          correction ==  "Corr") then
         if (it == 0) then
            found = .true.
            ref(1) = .000
            ref(2) = 1.001E-02
            ref(3) = 8.254E-03
            ref(4) = 1.176E-02
            ref(5) = 2.146E-01
            ref(6) = 1.095E-01
            ref(7) = 1.977E-01
         end if
         if (it == 1) then
            found = .true.
            ref(1) = 1.000
            ref(2) = 1.188E-02
            ref(3) = 1.091E-02
            ref(4) = 1.284E-02
            ref(5) = 2.874E-01
            ref(6) = 1.749E-01
            ref(7) = 2.403E-01
         end if
         if (it == 2) then
            found = .true.
            ref(1) = 2.000
            ref(2) = 1.521E-02
            ref(3) = 1.254E-02
            ref(4) = 1.793E-02
            ref(5) = 3.201E-01
            ref(6) = 1.755E-01
            ref(7) = 2.960E-01
         end if
         if (it == 3) then
            found = .true.
            ref(1) = 3.000
            ref(2) = 7.550E-03
            ref(3) = 7.109E-03
            ref(4) = 8.031E-03
            ref(5) = 1.787E-01
            ref(6) = 1.135E-01
            ref(7) = 1.435E-01
         end if
         if (it == 4) then
            found = .true.
            ref(1) = 4.000
            ref(2) = 1.695E-02
            ref(3) = 1.441E-02
            ref(4) = 1.954E-02
            ref(5) = 3.574E-01
            ref(6) = 2.066E-01
            ref(7) = 3.187E-01
         end if
         if (it == 5) then
            found = .true.
            ref(1) = 5.000
            ref(2) = 7.945E-03
            ref(3) = 6.741E-03
            ref(4) = 9.127E-03
            ref(5) = 1.639E-01
            ref(6) = 8.474E-02
            ref(7) = 1.455E-01
         end if
         if (it == 6) then
            found = .true.
            ref(1) = 6.000
            ref(2) = 1.382E-02
            ref(3) = 1.245E-02
            ref(4) = 1.516E-02
            ref(5) = 3.243E-01
            ref(6) = 1.967E-01
            ref(7) = 2.575E-01
         end if
         if (it == 7) then
            found = .true.
            ref(1) = 7.000
            ref(2) = 1.328E-02
            ref(3) = 1.088E-02
            ref(4) = 1.570E-02
            ref(5) = 2.872E-01
            ref(6) = 1.522E-01
            ref(7) = 2.498E-01
         end if
         if (it == 8) then
            found = .true.
            ref(1) = 8.000
            ref(2) = 9.239E-03
            ref(3) = 8.613E-03
            ref(4) = 9.893E-03
            ref(5) = 2.271E-01
            ref(6) = 1.445E-01
            ref(7) = 1.653E-01
         end if
         if (it == 9) then
            found = .true.
            ref(1) = 9.000
            ref(2) = 1.683E-02
            ref(3) = 1.409E-02
            ref(4) = 1.964E-02
            ref(5) = 3.639E-01
            ref(6) = 2.015E-01
            ref(7) = 3.128E-01
         end if
         if (it == 10) then
            found = .true.
            ref(1) = 10.000
            ref(2) = 6.802E-03
            ref(3) = 6.108E-03
            ref(4) = 7.495E-03
            ref(5) = 1.149E-01
            ref(6) = 7.634E-02
            ref(7) = 1.002E-01
         end if
      end if
      if (eqident == eqLINEARFD .and. &
          m          ==  27 .and. &
          destagger  ==  "Tdestagger3" .and. &
          deriv      ==  "Tderiv3" .and. &
          reltol     ==  "0.100E-07" .and. &
          ScaleCurv  ==  "1.000" .and. &
          correction ==  "Corr") then
         if (it == 0) then
            found = .true.
            ref(1) = .000
            ref(2) = 9.543E-03
            ref(3) = 2.079E-03
            ref(4) = 3.058E-03
            ref(5) = 3.870E-01
            ref(6) = 3.333E-02
            ref(7) = 5.925E-02
         end if
         if (it == 1) then
            found = .true.
            ref(1) = 1.000
            ref(2) = 1.063E-02
            ref(3) = 1.746E-03
            ref(4) = 1.596E-03
            ref(5) = 4.246E-01
            ref(6) = 3.944E-02
            ref(7) = 2.909E-02
         end if
         if (it == 2) then
            found = .true.
            ref(1) = 2.000
            ref(2) = 1.184E-02
            ref(3) = 2.524E-03
            ref(4) = 3.334E-03
            ref(5) = 3.921E-01
            ref(6) = 4.357E-02
            ref(7) = 6.553E-02
         end if
         if (it == 3) then
            found = .true.
            ref(1) = 3.000
            ref(2) = 7.952E-03
            ref(3) = 1.605E-03
            ref(4) = 1.580E-03
            ref(5) = 2.803E-01
            ref(6) = 6.023E-02
            ref(7) = 2.301E-02
         end if
         if (it == 4) then
            found = .true.
            ref(1) = 4.000
            ref(2) = 1.222E-02
            ref(3) = 2.463E-03
            ref(4) = 2.654E-03
            ref(5) = 4.183E-01
            ref(6) = 4.444E-02
            ref(7) = 4.731E-02
         end if
         if (it == 5) then
            found = .true.
            ref(1) = 5.000
            ref(2) = 7.763E-03
            ref(3) = 2.055E-03
            ref(4) = 2.641E-03
            ref(5) = 3.452E-01
            ref(6) = 5.024E-02
            ref(7) = 5.118E-02
         end if
         if (it == 6) then
            found = .true.
            ref(1) = 6.000
            ref(2) = 1.211E-02
            ref(3) = 1.820E-03
            ref(4) = 1.781E-03
            ref(5) = 5.397E-01
            ref(6) = 3.172E-02
            ref(7) = 3.533E-02
         end if
         if (it == 7) then
            found = .true.
            ref(1) = 7.000
            ref(2) = 1.122E-02
            ref(3) = 2.346E-03
            ref(4) = 3.370E-03
            ref(5) = 3.795E-01
            ref(6) = 3.725E-02
            ref(7) = 6.412E-02
         end if
         if (it == 8) then
            found = .true.
            ref(1) = 8.000
            ref(2) = 9.224E-03
            ref(3) = 1.852E-03
            ref(4) = 1.536E-03
            ref(5) = 4.293E-01
            ref(6) = 7.474E-02
            ref(7) = 2.579E-02
         end if
         if (it == 9) then
            found = .true.
            ref(1) = 9.000
            ref(2) = 1.177E-02
            ref(3) = 2.586E-03
            ref(4) = 3.008E-03
            ref(5) = 3.520E-01
            ref(6) = 4.299E-02
            ref(7) = 5.371E-02
         end if
         if (it == 10) then
            found = .true.
            ref(1) = 10.000
            ref(2) = 6.551E-03
            ref(3) = 1.832E-03
            ref(4) = 2.139E-03
            ref(5) = 1.856E-01
            ref(6) = 6.034E-02
            ref(7) = 4.194E-02
         end if
      end if
      if (eqident == eqCOMPRESSIBLE .and. &
          m          ==  29 .and. &
          destagger  ==  "Tdestagger3" .and. &
          deriv      ==  "Tderiv3" .and. &
          reltol     ==  "0.100E-07" .and. &
          ScaleCurv  ==  "1.000" .and. &
          correction ==  "Corr") then
         if (it == 0) then
            found = .true.
            ref(1) = .000
            ref(2) = 8.128E-03
            ref(3) = 3.080E-04
            ref(4) = 2.107E-04
            ref(5) = 1.672E-01
            ref(6) = 3.960E-03
            ref(7) = 2.898E-03
         end if
         if (it == 1) then
            found = .true.
            ref(1) = .218
            ref(2) = 7.929E-03
            ref(3) = 1.032E-03
            ref(4) = 4.846E-04
            ref(5) = 1.452E-01
            ref(6) = 1.860E-02
            ref(7) = 7.766E-03
         end if
         if (it == 2) then
            found = .true.
            ref(1) = .437
            ref(2) = 7.000E-03
            ref(3) = 5.422E-04
            ref(4) = 4.430E-04
            ref(5) = 1.286E-01
            ref(6) = 7.851E-03
            ref(7) = 4.756E-03
         end if
         if (it == 3) then
            found = .true.
            ref(1) = .655
            ref(2) = 8.091E-03
            ref(3) = 1.014E-03
            ref(4) = 1.070E-03
            ref(5) = 1.464E-01
            ref(6) = 1.167E-02
            ref(7) = 1.159E-02
         end if
         if (it == 4) then
            found = .true.
            ref(1) = .874
            ref(2) = 9.683E-03
            ref(3) = 1.927E-03
            ref(4) = 1.898E-03
            ref(5) = 1.844E-01
            ref(6) = 1.818E-02
            ref(7) = 2.104E-02
         end if
         if (it == 5) then
            found = .true.
            ref(1) = 1.092
            ref(2) = 9.274E-03
            ref(3) = 2.839E-03
            ref(4) = 2.855E-03
            ref(5) = 2.207E-01
            ref(6) = 2.757E-02
            ref(7) = 4.047E-02
         end if
         if (it == 6) then
            found = .true.
            ref(1) = 1.311
            ref(2) = 1.229E-02
            ref(3) = 1.301E-02
            ref(4) = 1.005E-02
            ref(5) = 2.071E-01
            ref(6) = 1.894E-01
            ref(7) = 1.370E-01
         end if
         if (it == 7) then
            found = .true.
            ref(1) = 1.529
            ref(2) = 5.894E-02
            ref(3) = 7.300E-02
            ref(4) = 4.061E-02
            ref(5) = 1.334E+00
            ref(6) = 1.266E+00
            ref(7) = 7.226E-01
         end if
         if (it == 8) then
            found = .true.
            ref(1) = 1.747
            ref(2) = 8.664E-02
            ref(3) = 1.040E-01
            ref(4) = 7.458E-02
            ref(5) = 2.156E+00
            ref(6) = 1.272E+00
            ref(7) = 1.370E+00
         end if
         if (it == 9) then
            found = .true.
            ref(1) = 1.966
            ref(2) = 2.403E-01
            ref(3) = 2.753E-01
            ref(4) = 3.038E-01
            ref(5) = 6.712E+00
            ref(6) = 7.292E+00
            ref(7) = 6.045E+00
         end if
         if (it == 10) then
            found = .true.
            ref(1) = 2.184
            ref(2) = 6.871E-01
            ref(3) = 6.550E-01
            ref(4) = 7.880E-01
            ref(5) = 1.448E+01
            ref(6) = 5.163E+01
            ref(7) = 9.370E+01
         end if
      end if
      if (eqident == eqSHALLOW .and. &
          m          ==  31 .and. &
          destagger  ==  "Tdestagger3" .and. &
          deriv      ==  "Tderiv3" .and. &
          reltol     ==  "0.100E-07" .and. &
          ScaleCurv  ==  "1.000" .and. &
          correction ==  "Corr") then
         if (it == 0) then
            found = .true.
            ref(1) = .000
            ref(2) = 1.732E-02
            ref(3) = 2.220E-02
            ref(4) = 9.530E-03
            ref(5) = 1.509E+00
            ref(6) = 1.216E+01
            ref(7) = 5.404E+00
         end if
         if (it == 1) then
            found = .true.
            ref(1) = .025
            ref(2) = 1.407E-02
            ref(3) = 1.675E-02
            ref(4) = 7.264E-03
            ref(5) = 1.323E+00
            ref(6) = 9.883E+00
            ref(7) = 4.651E+00
         end if
         if (it == 2) then
            found = .true.
            ref(1) = .050
            ref(2) = 1.381E-02
            ref(3) = 1.751E-02
            ref(4) = 7.359E-03
            ref(5) = 1.165E+00
            ref(6) = 1.099E+01
            ref(7) = 5.333E+00
         end if
         if (it == 3) then
            found = .true.
            ref(1) = .075
            ref(2) = 1.697E-02
            ref(3) = 2.120E-02
            ref(4) = 9.472E-03
            ref(5) = 1.550E+00
            ref(6) = 1.142E+01
            ref(7) = 4.817E+00
         end if
         if (it == 4) then
            found = .true.
            ref(1) = .099
            ref(2) = 1.430E-02
            ref(3) = 1.619E-02
            ref(4) = 8.900E-03
            ref(5) = 1.233E+00
            ref(6) = 9.932E+00
            ref(7) = 4.659E+00
         end if
         if (it == 5) then
            found = .true.
            ref(1) = .124
            ref(2) = 1.509E-02
            ref(3) = 2.074E-02
            ref(4) = 1.001E-02
            ref(5) = 1.283E+00
            ref(6) = 1.214E+01
            ref(7) = 6.093E+00
         end if
         if (it == 6) then
            found = .true.
            ref(1) = .149
            ref(2) = 2.488E-02
            ref(3) = 4.700E-02
            ref(4) = 3.351E-02
            ref(5) = 1.558E+00
            ref(6) = 1.956E+01
            ref(7) = 1.313E+01
         end if
         if (it == 7) then
            found = .true.
            ref(1) = .174
            ref(2) = 4.952E-02
            ref(3) = 8.077E-02
            ref(4) = 6.849E-02
            ref(5) = 3.341E+00
            ref(6) = 3.605E+01
            ref(7) = 3.261E+01
         end if
         if (it == 8) then
            found = .true.
            ref(1) = .199
            ref(2) = 9.104E-02
            ref(3) = 1.814E-01
            ref(4) = 1.029E-01
            ref(5) = 8.284E+00
            ref(6) = 1.240E+02
            ref(7) = 7.158E+01
         end if
         if (it == 9) then
            found = .true.
            ref(1) = .224
            ref(2) = 2.858E-01
            ref(3) = 4.463E-01
            ref(4) = 4.700E-01
            ref(5) = 3.846E+01
            ref(6) = 3.600E+02
            ref(7) = 3.794E+02
         end if
         if (it == 10) then
            found = .true.
            ref(1) = .248
            ref(2) = 8.104E-01
            ref(3) = 9.254E-01
            ref(4) = 9.096E-01
            ref(5) = 5.169E+02
            ref(6) = 1.750E+03
            ref(7) = 5.168E+03
         end if
      end if

   end subroutine GetReferences
end module localReferences

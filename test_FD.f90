! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
Program test_linearFD
! Test the finite-volume, zero-for-constant, and symmetry properties for 
! the FD operators.
use SwitchToEquation, only: Initialize
use Tests,            only: TestChainRule, TestSymmetryRho
use Tests,            only: TestAdvection, TestInterp, TestAdjointInterp
use Tests,            only: TestGradConst, TestSymmetry,    TestDivConst
use Util,             only: end_program
use FDApplyOperators, only: ApplyGradient, ApplyDivergence
implicit none


    call Initialize('test_linearFD')

    print *
    print *,'*******************************************************'
    print *,'test_linearFD'
    print *
    call TestInterp()
    call TestDivConst(ApplyDivergence)
    call TestAdjointInterp()
    call TestGradConst(ApplyGradient)
    call TestSymmetry(ApplyDivergence , ApplyGradient)

    call TestChainRule()
    call TestSymmetryRho()
    call TestAdvection()

    call end_program('test_linearFD')
    print *
    print *,'test_linearFD is finished'
    print *,'*******************************************************'
    print *

end Program test_linearFD

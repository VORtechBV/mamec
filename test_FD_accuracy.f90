! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
Program test_linearFD
! Test the accuracy of the FD operators.
use SwitchToEquation, only: Initialize
use Tests,            only: TestAccuracyRho
use Tests,            only: TestAccuracy2, TestAccuracy
use Util,             only: end_program
use FDApplyOperators, only: ApplyGradient, ApplyDivergence
implicit none


    call Initialize('test_linearFD')

    print *
    print *,'*******************************************************'
    print *,'test_linearFD-accuracy'
    print *

    call TestAccuracy(ApplyDivergence , ApplyGradient)
    call TestAccuracy2()
    call TestAccuracyRho()

    print *
    print *,'test_linearFD-accuracy is finished'
    call end_program('test_linearFD-accuracy')
    print *,'*******************************************************'
    print *

end Program test_linearFD

! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
Program test_linearGalerkin
! Test the finite-volume, zero-for-constant, and symmetry properties for 
! the linear Galerkin operators
use SwitchToEquation, only: Initialize
use Tests,            only: TestDivConst, TestGradConst, TestSymmetry, TestQuadratures
use Util,             only: end_program
use LinearWaveEqGalerkin, only: ApplyGradient, ApplyDivergence
implicit none

    call Initialize('test_linearGalerkin')
    print *
    print *,'*******************************************************'
    print *,'test_linearGalerkin'
    print *
    call TestQuadratures()
    call TestDivConst(ApplyDivergence)
    call TestGradConst(ApplyGradient)
    call TestSymmetry(ApplyDivergence , ApplyGradient)

    call end_program('test_linearGalerkin')
    print *
    print *,'*******************************************************'
    print *,'test_linearGalerkin is done'
    print *

end Program test_linearGalerkin

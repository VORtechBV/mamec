! This file is part of the MaMEC-project.
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
! Author(s): B. van 't Hof and M.J. Vuik
Program test_linearGalerkin
! Test the accuracy of the operators in the Galerkin-style operators for the linear wave equation
use Settings,         only: myprec
use SwitchToEquation, only: Initialize
use Tests,            only: TestAccuracy
use Util,             only: end_program
use LinearWaveEqGalerkin, only: ApplyGradient, ApplyDivergence
implicit none

    call Initialize('test_linearGalerkin')
    print *
    print *,'*******************************************************'
    print *,'test_linearGalerkin'
    print *

    call TestAccuracy(ApplyDivergence , ApplyGradient,tol=real(1d-3,myprec))

    print *
    print *,'*******************************************************'
    call end_program('test_linearGalerkin-accuracy')
    print *,'test_linearGalerkin is done'
    print *

end Program test_linearGalerkin
